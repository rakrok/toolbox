<?xml version="1.0" encoding="UTF-8"?>
<xri version="1.0">
   <description>
      <p>
        A collection of PixInsight scripts for image processing. The following scripts are
        currently contained: 
        	- CreateHubblePaletteFromOSC: Creates a Hubble palette image from a linear image captured using a One shot camera using dual narrowband filters.
        	- ImproveBrilliance: Improves the brilliance of color images.
        	- CombineImages: Combines images or masks with different methods.  
        	- ContinuumSubtraction: Continuum subtraction from Narrowband images to extract Ha.
        	- CombineHaToRGB: Combine Ha with RGB image.
        	- CreateHDRImage: Integrate HDR into your images for brighter regions of your target to show more details.
        	- AutoLinearFit: Linear Fit on all color channels automatically.  
        	- EnhanceNebula: Brightens the nebula. 
        	- SelectiveColorCorrection: Selectively change the colors of your image. 
        	- LabColorBoost: Improves the colors of your image using the L*a*b color space.
        	- CombineRGBAndNarrowband: Combines RGB image with duo narrowband image.
     </p>
   </description>
   <platform os="all" arch="noarch" version="1.8.9-1:1.9.10">
      <package fileName="20240111-PixInsightToolbox-Package.zip" sha1="daf3090bae35f7a2d66f6b34bfddd4739d2b2acb" type="script" releaseDate="20240111">
         <title>
            PixInsight Utility scripts
         </title>
         <remove>
            src/scripts/AutoLinearFit.js,
            src/scripts/AutoLinearFit.xsgn,
            src/scripts/CombineRGBAndDuoNarrowband.js,
            src/scripts/CombineRGBAndDuoNarrowband.xsgn,
            src/scripts/CombineHaToRGB.js,
            src/scripts/CombineHaToRGB.xsgn,
            src/scripts/CombineImages.js,
            src/scripts/CombineImages.xsgn,
            src/scripts/ContinuumSubtraction.js,
            src/scripts/ContinuumSubtraction.xsgn,
            src/scripts/CreateHDRImage.js,
            src/scripts/CreateHDRImage.xsgn,
            src/scripts/CreateHubblePaletteFromOSC.js,
            src/scripts/CreateHubblePaletteFromOSC.xsgn,
            src/scripts/EnhanceNebula.js,
            src/scripts/EnhanceNebula.xsgn,
            src/scripts/ImproveBrilliance.js,
            src/scripts/ImproveBrilliance.xsgn,
            src/scripts/LabColorBoost.js,
            src/scripts/LabColorBoost.xsgn,
            src/scripts/PixInsightToolsPreviewControl.jsh,
            src/scripts/PixInsightToolsPreviewControl.xsgn,
            src/scripts/SelectiveColorCorrection.js,
            src/scripts/SelectiveColorCorrection.xsgn,
            doc/scripts/CombineRGBAndDuoNarrowband
         </remove>
         <description>
            <p>GraXpert 1.2b: <br/>
            	- Now, always saves the temporary image into the temp folder to avoid permission issues when the image was opened from a readonly folder. <br/> 
            	- Saves your settings on execute and restores them on the next script run. <br/>
            	- Supports showing the background model generated by the GraXpert AI. <br/>
            	- You can now reset all your settings back to the defaults. <br/>
            	- Improved console output. <br/>
            	- Fixed: New Instance button did not work. <br/>
            	- <b>Improved: Windows users now no longer need to select the path to GraXpert manually when using the installer. Using the wrench icon the script will ask, if it can do this automatically!</b>  <br/>
            	<br/>            	
            	<b>You will need to install GraXpert release 2.2.0 or higher in order to use this script! The old script version will also not work with the new GraXpert version.</b>    
            </p>
            <p>All scripts with preview: Zooming per Shift+Left Mouse Button improved. Previously, sometimes zooming failed. </p>
            <p>EnhanceNebula: Added amount control to reduce the brightness enhancement. The stars have now more contrast on bright backgrounds. </p>
            <p>Copyright (c) 2024 Juergen Terpe, All Rights Reserved.</p>
         </description>
      </package>
   </platform>
</xri>
<Signature developerId="JuergenTerpe" timestamp="2024-01-11T19:34:07.163Z" encoding="Base64">pBWthZEfMe9BQI6TSAcMTOOJz9kxNVolDYsEm3M19k62ft4dcfY7G6fZ5KJEJGTxagv4BArMFXoYezj2e8WnCQ==</Signature>
