<?xml version="1.0" encoding="UTF-8"?>
<xri version="1.0">
   <description>
      <p>A collection of PixInsight scripts for image processing. The following scripts are currently contained: </p>
      <p>	- CreateHubblePaletteFromOSC: Creates a Hubble palette image from a linear image captured using a One shot camera using dual narrowband filters.</p>
      <p>	- ImproveBrilliance: Improves the brilliance of color images.</p>
      <p>	- CombineImages: Combines images or masks with different methods. </p>
      <p>	- ContinuumSubtraction: Continuum subtraction from Narrowband images to extract Ha.</p>
      <p>	- CombineHaToRGB: Combine Ha with RGB image.</p>
      <p>	- CreateHDRImage: Integrate HDR into your images for brighter regions of your target to show more details.</p>
      <p>	- AutoLinearFit: Linear Fit on all color channels automatically. </p>
      <p>	- EnhanceNebula: Brightens the nebula.</p>
      <p>	- SelectiveColorCorrection: Selectively change the colors of your image. </p>
      <p>	- LabColorBoost: Improves the colors of your image using the L*a*b color space.</p>
      <p>	- CombineRGBAndNarrowband: Combines RGB image with duo narrowband image.</p>
      <p>	- GraXpert script: Use GraXpert from within PixInsight to automatically remove gradients using AI.</p>
      <p>	- GraXpertDenoise script: Use GraXpert from within PixInsight to automatically reduce noise using AI.</p>
      <p>	- NarrowbandHueCombination: Simplifies the HSV colourspace-based generation of RGB images from narrowband sources.</p>
      <p>	- NBStarColorsFromRGB: Extracts the RGB stars and replaces them in the target narrowband image using their a and b components in CIELAB color space.</p>
      <p>	- BGVisualizer: Helper script to visualize in black/white the flatness of the background.</p>
   </description>
   <platform os="all" arch="noarch" version="1.8.9-1:1.9.10">
      <package fileName="20240422-PixInsightToolbox-Package.zip" sha1="db9091372fcec87bea724a6478ff593a85bcc06f" type="script" releaseDate="20240422">
         <title>
            PixInsight Utility scripts
         </title>
         <remove>
         	src/scripts/BGVisualizer.js,
         	src/scripts/BGVisualizer.xsgn,
         	src/scripts/NarrowbandHueCombination.js,
         	src/scripts/NarrowbandHueCombination.xsgn,
         	src/scripts/NBStarColorsFromRGB.js,
         	src/scripts/NBStarColorsFromRGB.xsgn         	
         </remove>
         <description>
            <p>New script: GraXpertDenoise uses GraXpert v3.0.0 or higher to denoise images via AI.</p>
            <p>CreateHDRImage: The script now processes grayscale images correctly. Previously, grayscale images were converted into an RGB image by mistake.</p>
            <p>
               <b>Please note: All scripts are code-signed again.</b> If you are still using an older version of PixInsight please consider updating or remove the *.xsgn files from src/scripts/Toolbox folder inside your PixInsight installation!
            At least PixInsight Build <b>1602</b> is required!</p>
            <p>Copyright (c) 2024 Jürgen Bätz and Jürgen Terpe, All Rights Reserved.</p>
         </description>
      </package>
   </platform>
</xri>
<Signature developerId="JuergenTerpe" timestamp="2024-04-22T14:37:45.230Z" encoding="Base64">fEnUSRaQn8ahXpan828rj3R+YNbjd0PHdWOhLfCayZMdW9qmar5ZwTzSiH15+JXRQK250FLChZ5kaDwcy5TvCg==</Signature>
