#include <pjsr/StdIcon.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/TextAlign.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/ColorSpace.jsh>
#include <pjsr/Interpolation.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/CheckState.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/SampleType.jsh>
#include "PixInsightToolsPreviewControl.jsh"

#include <pjsr/DataType.jsh>
#include <../src/scripts/AdP/WCSmetadata.jsh>


 /*
 * Default STF Parameters
 */
// Shadows clipping point in (normalized) MAD units from the median.
#define DEFAULT_AUTOSTRETCH_SCLIP  -2.80
// Target mean background in the [0,1] range.
#define DEFAULT_AUTOSTRETCH_TBGND   0.25


#feature-id CreateHubblePaletteFromOSC :  Toolbox > CreateHubblePaletteFromOSC

#feature-info  A script for creating color palette images from OSC dual narrowband images.<br/>\
   <br/>\
   A utility script to create different color palette images from OSC dual narrowband images. \
   <br/> \
   Copyright &copy; 2024 Juergen Terpe


#define VERSION "1.9"
#define TITLE   "Create color palette image from OSC Duo Narrowband Images"

#define LEFTWIDTH 400

var OSC2HSOParameters = {
   palette: "HOO",
   clipBlack: 0.0,
   processedView: undefined,
   previewView: undefined,
   previewImage: undefined,
   previewHa: undefined,
   previewOIII: undefined,
   previewSII: undefined,
   createMask: true
}

function scaleImage(image) {
   var maxSize = Math.max(image.width, image.height);

   if (createHDRParameters.previewMode == 0) {
      var scale = 800.0/maxSize;
      image.resample(scale);
   }
   else if (createHDRParameters.previewMode == 1) {
      var scale = Math.min(2048.0/maxSize, 1.0/3.0);
      image.resample(scale);
   }

   return image;
}

function blackClip(view, amount) {

   if (amount > 0.01) {

      var P = new PixelMath;
      P.expression = "k = "+amount+"*mean($T);\n $T-k";
      P.useSingleExpression = true;
      P.symbols = "k";
      P.clearImageCacheAndExit = false;
      P.cacheGeneratedImages = false;
      P.generateOutput = true;
      P.singleThreaded = false;
      P.optimization = true;
      P.use64BitWorkingImage = false;
      P.rescale = false;
      P.rescaleLower = 0;
      P.rescaleUpper = 1;
      P.truncate = true;
      P.truncateLower = 0;
      P.truncateUpper = 1;
      P.createNewImage = false;
      P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
      P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
      P.executeOn(view);
   }
}

function doAutoStretchImage(image, id) {
   Console.writeln("Autostretch image"+ id);

   var imageWindow = new ImageWindow(image.width, image.height, image.numberOfChannels, image.bitsPerSample,
                                 image.sampleType == SampleType_Real, image.isColor, id );
   var view = imageWindow.mainView;
   view.beginProcess( UndoFlag_NoSwapFile );
   view.image.assign( image );
   view.endProcess();

   doAutoStretch(view);
   image.assign(view.image);
   imageWindow.forceClose();

   return image;
}

function doAutoStretch(view) {
   let shadowsClipping = DEFAULT_AUTOSTRETCH_SCLIP;
   let targetBackground = DEFAULT_AUTOSTRETCH_TBGND;

   var P = new HistogramTransformation;

   var c0 = 0, m = 0;
   var n = view.image.isColor ? 3 : 1;

   var median = view.computeOrFetchProperty( "Median" );
   var mad = view.computeOrFetchProperty( "MAD" );
   mad.mul( 1.4826 ); // coherent with a normal distribution

   for ( var c = 0; c < n; ++c )
   {
      if ( 1 + mad.at( c ) != 1 )
         c0 += median.at( c ) + shadowsClipping * mad.at( c );
      m  += median.at( c );
   }
   c0 = Math.range( c0/n, 0.0, 1.0 );
   m = Math.mtf( targetBackground, m/n - c0 );

   P.H = [ // c0, m, c1, r0, r1
      [0.00000000, 0.5, 1.00000000, 0.00000000, 1.00000000],
      [0.00000000, 0.5, 1.00000000, 0.00000000, 1.00000000],
      [0.00000000, 0.5, 1.00000000, 0.00000000, 1.00000000],
      [c0,   m, 1.00000000, 0.00000000, 1.00000000],
      [0.00000000, 0.5, 1.00000000, 0.00000000, 1.00000000]
   ];
   P.executeOn(view);
}


function STFAutoStretch( view) {
   let shadowsClipping = DEFAULT_AUTOSTRETCH_SCLIP;
   let targetBackground = DEFAULT_AUTOSTRETCH_TBGND;
   let rgbLinked = true;

   var stf = new ScreenTransferFunction;

   var n = view.image.isColor ? 3 : 1;

   var median = view.computeOrFetchProperty( "Median" );

   var mad = view.computeOrFetchProperty( "MAD" );
   mad.mul( 1.4826 ); // coherent with a normal distribution

   if ( rgbLinked )
   {
      /*
       * Try to find how many channels look as channels of an inverted image.
       * We know a channel has been inverted because the main histogram peak is
       * located over the right-hand half of the histogram. Seems simplistic
       * but this is consistent with astronomical images.
       */
      var invertedChannels = 0;
      for ( var c = 0; c < n; ++c )
         if ( median.at( c ) > 0.5 )
            ++invertedChannels;

      if ( invertedChannels < n )
      {
         /*
          * Noninverted image
          */
         var c0 = 0, m = 0;
         for ( var c = 0; c < n; ++c )
         {
            if ( 1 + mad.at( c ) != 1 )
               c0 += median.at( c ) + shadowsClipping * mad.at( c );
            m  += median.at( c );
         }
         c0 = Math.range( c0/n, 0.0, 1.0 );
         m = Math.mtf( targetBackground, m/n - c0 );

         stf.STF = [ // c0, c1, m, r0, r1
                     [c0, 1, m, 0, 1],
                     [c0, 1, m, 0, 1],
                     [c0, 1, m, 0, 1],
                     [0, 1, 0.5, 0, 1] ];
      }
      else
      {
         /*
          * Inverted image
          */
         var c1 = 0, m = 0;
         for ( var c = 0; c < n; ++c )
         {
            m  += median.at( c );
            if ( 1 + mad.at( c ) != 1 )
               c1 += median.at( c ) - shadowsClipping * mad.at( c );
            else
               c1 += 1;
         }
         c1 = Math.range( c1/n, 0.0, 1.0 );
         m = Math.mtf( c1 - m/n, targetBackground );

         stf.STF = [ // c0, c1, m, r0, r1
                     [0, c1, m, 0, 1],
                     [0, c1, m, 0, 1],
                     [0, c1, m, 0, 1],
                     [0, 1, 0.5, 0, 1] ];
      }
   }
   else
   {
      /*
       * Unlinked RGB channnels: Compute automatic stretch functions for
       * individual RGB channels separately.
       */
      var A = [ // c0, c1, m, r0, r1
               [0, 1, 0.5, 0, 1],
               [0, 1, 0.5, 0, 1],
               [0, 1, 0.5, 0, 1],
               [0, 1, 0.5, 0, 1] ];

      for ( var c = 0; c < n; ++c )
      {
         if ( median.at( c ) < 0.5 )
         {
            /*
             * Noninverted channel
             */
            var c0 = (1 + mad.at( c ) != 1) ? Math.range( median.at( c ) + shadowsClipping * mad.at( c ), 0.0, 1.0 ) : 0.0;
            var m  = Math.mtf( targetBackground, median.at( c ) - c0 );
            A[c] = [c0, 1, m, 0, 1];
         }
         else
         {
            /*
             * Inverted channel
             */
            var c1 = (1 + mad.at( c ) != 1) ? Math.range( median.at( c ) - shadowsClipping * mad.at( c ), 0.0, 1.0 ) : 1.0;
            var m  = Math.mtf( c1 - median.at( c ), targetBackground );
            A[c] = [0, c1, m, 0, 1];
         }
      }

      stf.STF = A;
   }

   stf.executeOn( view );
}

function SetRGBWorkingSpace(view) {

   var P = new RGBWorkingSpace;
   P.channels = [ // Y, x, y
      [1.000000, 0.648431, 0.330856],
      [1.000000, 0.321152, 0.597871],
      [1.000000, 0.155886, 0.066044]
   ];
   P.gamma = 2.20;
   P.sRGBGamma = true;
   P.applyGlobalRGBWS = false;
   P.executeOn(view);
}

function clone(view) {
   var P = new PixelMath;
   P.expression = "$T";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.truncate = true;
   P.createNewImage = true;
   P.showNewImage = true;
   P.newImageId = view.id + "_cloned";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);
   return view.id + "_cloned";
}

function cloneHidden(view) {
   var P = new PixelMath;
   P.expression = "$T";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.truncate = true;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + "_cloned";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return View.viewById(P.newImageId);
}

function linearFit(view, referenceView) {
   var P = new LinearFit;
   P.referenceViewId = referenceView.id;
   P.rejectLow = 0.000000;
   P.rejectHigh = 0.920000;
   P.executeOn(view);
}

function linearFitChannels(viewHa, viewSII, viewOIII) {

   var meanHa = viewHa.image.mean();
   var meanSII = viewSII.image.mean();
   var meanOIII = viewOIII.image.mean();

   if (meanOIII < Math.min(meanSII, meanHa)) {
      linearFit(viewHa, viewOIII);
      linearFit(viewSII, viewOIII);

   } else if (meanSII < Math.min(meanOIII, meanHa)) {
      linearFit(viewOIII, viewSII);
      linearFit(viewHa, viewSII);

   } else {
      linearFit(viewSII, viewHa);
      linearFit(viewOIII, viewHa);
   }
}

function boostChannel(view, boostFactor) {
   var P = new HistogramTransformation;
   P.H = [ // c0, m, c1, r0, r1
      [0.00000000, 0.50000000, 1.00000000, 0.00000000, 1.00000000],
      [0.00000000, 0.50000000, 1.00000000, 0.00000000, 1.00000000],
      [0.00000000, 0.50000000, 1.00000000, 0.00000000, 1.00000000],
      [0.00000000, 0.50000000 - (boostFactor * 0.5)/10.0, 1.00000000, 0.00000000, 1.00000000],
      [0.00000000, 0.50000000, 1.00000000, 0.00000000, 1.00000000]
   ];
   P.executeOn(view);

}


function createHa(view) {
   Console.writeln("creating Ha channel");
   var P = new PixelMath;
   P.expression = "$T[0]";
   P.expression1 = "";
   P.expression2 = "";
   P.expression3 = "";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + "_Ha";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return View.viewById(P.newImageId);
}

function createOIII(view) {
   Console.writeln("creating OIII channel");
   var P2 = new PixelMath;
   P2.expression = "0.7*$T[1]+0.3*$T[2]";
   P2.useSingleExpression = true;
   P2.clearImageCacheAndExit = false;
   P2.cacheGeneratedImages = false;
   P2.generateOutput = true;
   P2.singleThreaded = false;
   P2.optimization = true;
   P2.use64BitWorkingImage = false;
   P2.rescale = false;
   P2.truncate = true;
   P2.createNewImage = true;
   P2.showNewImage = false;
   P2.newImageId = view.id + "_OIII";
   P2.newImageColorSpace = PixelMath.prototype.Gray;
   P2.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P2.executeOn(view);

   return View.viewById(P2.newImageId);
}

function createSII(view) {
   Console.writeln("creating SII channel");
   var P3 = new PixelMath;
   P3.expression = "CIEL($T)";
   P3.useSingleExpression = true;
   P3.clearImageCacheAndExit = false;
   P3.cacheGeneratedImages = false;
   P3.generateOutput = true;
   P3.singleThreaded = false;
   P3.optimization = true;
   P3.use64BitWorkingImage = false;
   P3.rescale = false;
   P3.truncate = true;
   P3.createNewImage = true;
   P3.showNewImage = false;
   P3.newImageId = view.id + "_SII";
   P3.newImageColorSpace = PixelMath.prototype.Gray;
   P3.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P3.executeOn(view);

   return View.viewById(P3.newImageId);
}



function combineHSO(view, ha_id, sii_id, oiii_id, newImage=true) {
   Console.writeln("creating HSO");

   var P = new PixelMath;
   P.expression = ha_id;
   P.expression1 = sii_id;
   P.expression2 = oiii_id;
   P.expression3 = "";
   P.symbols = "";
   P.useSingleExpression = false;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = newImage;
   P.showNewImage = true;
   P.newImageId = view.id + "_HSO";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.RGB;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return newImage ? View.viewById(P.newImageId) : view;
}

function combineOSH(view, ha_id, sii_id, oiii_id, newImage=true) {
   Console.writeln("creating OSH");

   var P = new PixelMath;
   P.expression = oiii_id;
   P.expression1 = sii_id;
   P.expression2 = ha_id;
   P.expression3 = "";
   P.useSingleExpression = false;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = newImage;
   P.showNewImage = true;
   P.newImageId = view.id + "_OSH";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.RGB;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return newImage ? View.viewById(P.newImageId) : view;
}

function combineOHS(view, ha_id, sii_id, oiii_id, newImage=true) {
   Console.writeln("creating OHS");

   var P = new PixelMath;
   P.expression = oiii_id;
   P.expression1 = ha_id;
   P.expression2 = sii_id;
   P.expression3 = "";
   P.useSingleExpression = false;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = newImage;
   P.showNewImage = true;
   P.newImageId = view.id + "_OHS";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.RGB;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return newImage ? View.viewById(P.newImageId) : view;
}

function combineSHO(view, ha_id, sii_id, oiii_id, newImage=true) {
   Console.writeln("creating SHO");

   var P = new PixelMath;
   P.expression = sii_id;
   P.expression1 = ha_id;
   P.expression2 = oiii_id;
   P.expression3 = "";
   P.useSingleExpression = false;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = newImage;
   P.showNewImage = true;
   P.newImageId = view.id + "_SHO";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.RGB;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return newImage ? View.viewById(P.newImageId) : view;
}

function combineHOS(view, ha_id, sii_id, oiii_id, newImage=true) {
   Console.writeln("creating HOS");

   var P = new PixelMath;
   P.expression = ha_id;
   P.expression1 = oiii_id;
   P.expression2 = sii_id;
   P.expression3 = "";
   P.useSingleExpression = false;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = newImage;
   P.showNewImage = true;
   P.newImageId = view.id + "_HOS";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.RGB;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return newImage ? View.viewById(P.newImageId) : view;
}

function combineHOO(view, ha_id, sii_id, oiii_id, newImage=true) {
   Console.writeln("creating HOO");

   var P = new PixelMath;
   P.expression = ha_id;
   P.expression1 = oiii_id;
   P.expression2 = oiii_id;
   P.expression3 = "";
   P.useSingleExpression = false;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = newImage;
   P.showNewImage = true;
   P.newImageId = view.id + "_HOO";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.RGB;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return newImage ? View.viewById(P.newImageId) : view;
}

function combineForaxSHO(view, ha_id, sii_id, oiii_id, newImage=true) {
   Console.writeln("creating ForaxSHO");

   var P = new PixelMath;
   P.expression = "("+oiii_id+"^~"+oiii_id+")*"+sii_id+"+ ~("+oiii_id+"^~"+oiii_id+")*"+ha_id;
   P.expression1 = "(("+oiii_id+"*"+ha_id+")^~("+oiii_id+"*"+ha_id+"))*"+ha_id+" + ~(("+oiii_id+"*"+ha_id+")^~("+oiii_id+"*"+ha_id+"))*"+oiii_id;
   P.expression2 = oiii_id;
   P.expression3 = "";
   P.useSingleExpression = false;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = newImage;
   P.showNewImage = true;
   P.newImageId = view.id + "_ForaxSHO";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.RGB;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return newImage ? View.viewById(P.newImageId) : view;
}

function combineForaxHSO(view, ha_id, sii_id, oiii_id, newImage=true) {
   Console.writeln("creating ForaxHSO");

   var P = new PixelMath;
   P.expression = "("+oiii_id+"^~"+oiii_id+")*"+sii_id+"+ ~("+oiii_id+"^~"+oiii_id+")*"+ha_id;
   P.expression1 = sii_id;
   P.expression2 = "(("+oiii_id+"*"+ha_id+")^~("+oiii_id+"*"+ha_id+"))*"+ha_id+" + ~(("+oiii_id+"*"+ha_id+")^~("+oiii_id+"*"+ha_id+"))*"+oiii_id;
   P.expression3 = "";
   P.useSingleExpression = false;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = newImage;
   P.showNewImage = true;
   P.newImageId = view.id + "_ForaxHSO";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.RGB;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return newImage ? View.viewById(P.newImageId) : view;
}

function combineForaxHOO(view, ha_id, sii_id, oiii_id, newImage=true) {
   Console.writeln("creating ForaxHOO");

   var P = new PixelMath;
   P.expression = ha_id;
   P.expression1 = "(("+oiii_id+"*"+ha_id+")^~("+oiii_id+"*"+ha_id+"))*"+ha_id+" + ~(("+oiii_id+"*"+ha_id+")^~("+oiii_id+"*"+ha_id+"))*"+oiii_id;
   P.expression2 = oiii_id;
   P.expression3 = "";
   P.useSingleExpression = false;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = newImage;
   P.showNewImage = true;
   P.newImageId = view.id + "_ForaxHOO";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.RGB;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return newImage ? View.viewById(P.newImageId) : view;
}

function combineDreamsplease(view, ha_id, sii_id, oiii_id, newImage=true) {
   Console.writeln("creating Dreamsplease");

   var P = new PixelMath;
   P.expression = "iif("+ha_id+" > 0.15, "+ha_id+", ("+ha_id+"*0.8)+("+oiii_id+"*0.2))";
   P.expression1 = "iif("+ha_id+" > 0.5, 1-(1-"+oiii_id+")*(1-("+ha_id+"-0.5)), "+oiii_id+"*("+ha_id+"+0.5))";
   P.expression2 = "iif("+oiii_id+" > 0.1, "+oiii_id+", ("+ha_id+"*0.3)+("+oiii_id+"*0.2))";
   P.expression3 = "";
   P.useSingleExpression = false;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = true;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = newImage;
   P.showNewImage = true;
   P.newImageId = view.id + "_Dreamsplease";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.RGB;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(view);

   return newImage ? View.viewById(P.newImageId) : view;
}


function iconizeView(view) {
   if (view !== undefined ) {
      view.window.iconize();
   }
}

function closeView(view) {
   if (view !== undefined && view != null) {
      try {
         view.window.forceClose();
      }
      catch(error) {
         Console.writeln("could not close view...");
      }
   }
}

function closeAllPreviewViews() {

   var preview = OSC2HSOParameters.previewView;
   OSC2HSOParameters.previewView = undefined;
   closeView(preview);

   preview = OSC2HSOParameters.previewHa;
   OSC2HSOParameters.previewHa = undefined;
   closeView(preview);

   preview = OSC2HSOParameters.previewOIII;
   OSC2HSOParameters.previewOIII = undefined;
   closeView(preview);

   preview = OSC2HSOParameters.previewSII;
   OSC2HSOParameters.previewSII = undefined;
   closeView(preview);
}

function process(currentView) {

   try {
      var viewId = currentView.id;
      if (viewId.length == 0) {
         Console.warning("No image selected for processing!");
         return;
      }

      Console.writeln("Procssing " + viewId);
   }
   catch(error) {
      return;
   }

   let metadata = new ImageMetadata("CreateHubblePaletteFromOSC");
	metadata.ExtractMetadata(currentView.window);

   try {

      var processView = cloneHidden(currentView);
      SetRGBWorkingSpace(processView);

      var oIIIView = createOIII(processView);
      var haView = createHa(processView);
      var sIIView = createSII(processView);

      linearFitChannels(haView, sIIView, oIIIView);
      SetRGBWorkingSpace(processView);

      if (OSC2HSOParameters.palette == "HSO") {
         var finalImage = combineHSO(processView, haView.id, sIIView.id, oIIIView.id);
         blackClip(finalImage, OSC2HSOParameters.clipBlack);
         STFAutoStretch(finalImage);
         finalImage.window.keywords = currentView.window.keywords;
      } else if (OSC2HSOParameters.palette == "SHO") {
         var finalImage = combineSHO(processView, haView.id, sIIView.id, oIIIView.id);
         blackClip(finalImage, OSC2HSOParameters.clipBlack);
         STFAutoStretch(finalImage);
         finalImage.window.keywords = currentView.window.keywords;
      } else if (OSC2HSOParameters.palette == "OSH") {
         var finalImage = combineOSH(processView, haView.id, sIIView.id, oIIIView.id);
         blackClip(finalImage, OSC2HSOParameters.clipBlack);
         STFAutoStretch(finalImage);
         finalImage.window.keywords = currentView.window.keywords;
      } else if (OSC2HSOParameters.palette == "OHS") {
         var finalImage = combineOHS(processView, haView.id, sIIView.id, oIIIView.id);
         blackClip(finalImage, OSC2HSOParameters.clipBlack);
         STFAutoStretch(finalImage);
         finalImage.window.keywords = currentView.window.keywords;
      } else if (OSC2HSOParameters.palette == "HOS") {
         var finalImage = combineHOS(processView, haView.id, sIIView.id, oIIIView.id);
         blackClip(finalImage, OSC2HSOParameters.clipBlack);
         STFAutoStretch(finalImage);
         finalImage.window.keywords = currentView.window.keywords;
      } else if (OSC2HSOParameters.palette == "HOO") {
         var finalImage = combineHOO(processView, haView.id, sIIView.id, oIIIView.id);
         blackClip(finalImage, OSC2HSOParameters.clipBlack);
         STFAutoStretch(finalImage);
         finalImage.window.keywords = currentView.window.keywords;
      } else if (OSC2HSOParameters.palette == "ForaxHOO") {
         var finalImage = combineForaxHOO(processView, haView.id, sIIView.id, oIIIView.id);
         STFAutoStretch(finalImage);
         finalImage.window.keywords = currentView.window.keywords;
      } else if (OSC2HSOParameters.palette == "ForaxSHO") {
         var finalImage = combineForaxSHO(processView, haView.id, sIIView.id, oIIIView.id);
         blackClip(finalImage, OSC2HSOParameters.clipBlack);
         STFAutoStretch(finalImage);
         finalImage.window.keywords = currentView.window.keywords;
      } else if (OSC2HSOParameters.palette == "ForaxHSO") {
         var finalImage = combineForaxHSO(processView, haView.id, sIIView.id, oIIIView.id);
         blackClip(finalImage, OSC2HSOParameters.clipBlack);
         STFAutoStretch(finalImage);
         finalImage.window.keywords = currentView.window.keywords;
      } else if (OSC2HSOParameters.palette == "Dreamsplease") {
         var finalImage = combineDreamsplease(processView, haView.id, sIIView.id, oIIIView.id);
         blackClip(finalImage, OSC2HSOParameters.clipBlack);
         STFAutoStretch(finalImage);
         finalImage.window.keywords = currentView.window.keywords;
      }
   }
   finally {

      processView.window.forceClose();

      if (!metadata.projection || !metadata.ref_I_G) {
         Console.writeln("The image " + currentView.id + " has no astrometric solution");
      }
      else {
         metadata.SaveKeywords( finalImage.window, false);
         metadata.SaveProperties( finalImage.window, TITLE + " " + VERSION);
      }

      if (!OSC2HSOParameters.createMasks) {

         haView.window.forceClose();
         oIIIView.window.forceClose();
         sIIView.window.forceClose();
      }
      else {
         haView.id = haView.id.replace("_cloned", "_mask");
         haView.window.iconize();
         oIIIView.id = oIIIView.id.replace("_cloned", "_mask");
         oIIIView.window.iconize();
         sIIView.id = sIIView.id.replace("_cloned", "_mask");
         sIIView.window.iconize();
      }


   }
}


function previewBG(view) {

   var bghigh = view.image.mean();

   var P = new BackgroundNeutralization;
   P.backgroundReferenceViewId = "";
   P.backgroundLow = 0.0000000;
   P.backgroundHigh = bghigh;
   P.useROI = false;
   P.roiX0 = 0;
   P.roiY0 = 0;
   P.roiX1 = 0;
   P.roiY1 = 0;
   P.mode = BackgroundNeutralization.prototype.RescaleAsNeeded;
   P.targetBackground = bghigh;
   P.executeOn(view);
}

function preparePreview() {
   if (OSC2HSOParameters.processedView) {
      var previewImage = new Image(OSC2HSOParameters.processedView.image);

      // create a closed image for processing in the background
      var previewWindow = new ImageWindow( previewImage.width, previewImage.height, previewImage.numberOfChannels, previewImage.bitsPerSample,
                                 previewImage.sampleType == SampleType_Real, previewImage.isColor, OSC2HSOParameters.processedView.id + "_preview" );
      var v = previewWindow.mainView;
      v.beginProcess( UndoFlag_NoSwapFile );
      v.image.assign( previewImage );
      v.endProcess();

      SetRGBWorkingSpace(v);

      OSC2HSOParameters.previewView = previewWindow.mainView;

      var processView = previewWindow.mainView;

      previewBG(processView);

      var oIIIView = createOIII(processView);
      var haView = createHa(processView);
      var sIIView = createSII(processView);

      linearFitChannels(haView, sIIView, oIIIView);

      OSC2HSOParameters.previewHa = haView;
      OSC2HSOParameters.previewOIII = oIIIView;
      OSC2HSOParameters.previewSII = sIIView;

      doAutoStretch(v);
      previewImage.assign(v.image);

      OSC2HSOParameters.previewImage = previewImage;
      return previewImage;
   }
   return undefined;
}

function processPreview() {
   if (OSC2HSOParameters.previewImage
       && OSC2HSOParameters.previewHa
       && OSC2HSOParameters.previewOIII
       && OSC2HSOParameters.previewSII) {

      var img = OSC2HSOParameters.previewImage;
      // create a closed image for processing in the background
      var wtmp = new ImageWindow( img.width, img.height, img.numberOfChannels, img.bitsPerSample,
                                 img.sampleType == SampleType_Real, img.isColor, "tmp_preview" );
      var previewTemp = wtmp.mainView;
      previewTemp.beginProcess( UndoFlag_NoSwapFile );
      previewTemp.image.assign( img );
      previewTemp.endProcess();

      SetRGBWorkingSpace(previewTemp);

      var haView = cloneHidden(OSC2HSOParameters.previewHa);
      var oIIIView = cloneHidden(OSC2HSOParameters.previewOIII);
      var sIIView = cloneHidden(OSC2HSOParameters.previewSII);

      try {

         var ha_Id = haView.id;
         var oIII_Id = oIIIView.id;
         var sII_Id = sIIView.id;

         if (OSC2HSOParameters.palette == "HSO") {
            combineHSO(previewTemp, ha_Id, sII_Id, oIII_Id, false);
         } else if (OSC2HSOParameters.palette == "SHO") {
            combineSHO(previewTemp, ha_Id, sII_Id, oIII_Id, false);
         } else if (OSC2HSOParameters.palette == "OSH") {
            combineOSH(previewTemp, ha_Id, sII_Id, oIII_Id, false);
         } else if (OSC2HSOParameters.palette == "OHS") {
            combineOHS(previewTemp, ha_Id, sII_Id, oIII_Id, false);
         } else if (OSC2HSOParameters.palette == "HOS") {
            combineHOS(previewTemp, ha_Id, sII_Id, oIII_Id, false);
         } else if (OSC2HSOParameters.palette == "HOO") {
            combineHOO(previewTemp, ha_Id, sII_Id, oIII_Id, false);
         } else if (OSC2HSOParameters.palette == "ForaxHOO") {
            combineForaxHOO(previewTemp, ha_Id, sII_Id, oIII_Id, false);
         } else if (OSC2HSOParameters.palette == "ForaxSHO") {
            combineForaxSHO(previewTemp, ha_Id, sII_Id, oIII_Id, false);
         } else if (OSC2HSOParameters.palette == "ForaxHSO") {
            combineForaxHSO(previewTemp, ha_Id, sII_Id, oIII_Id, false);
         } else if (OSC2HSOParameters.palette == "Dreamsplease") {
            combineDreamsplease(previewTemp, ha_Id, sII_Id, oIII_Id, false);
         }

         blackClip(previewTemp, OSC2HSOParameters.clipBlack);
         doAutoStretch(previewTemp);

         OSC2HSOParameters.previewImage.assign(previewTemp.image);

      }
      finally {
         closeView(haView);
         closeView(oIIIView);
         closeView(sIIView);
         wtmp.forceClose();

      }

   }
}

function getPreviewKey() {

   try {

      if (!OSC2HSOParameters.processedView)
         return ""
      else {
         let key = "PreviewId: "+ OSC2HSOParameters.processedView.id
            + ", Palette: "+ OSC2HSOParameters.palette
            + ", CLIPBLACK: " + OSC2HSOParameters.clipBlack;

         return key;
      }
   }
   catch(error) {
      Console.writeln(error);
      return "..."
   }
}


function OSC2HSODialog() {
   this.__base__ = Dialog
   this.__base__();
   this.scaledMinWidth = 1280;
   this.scaledMinHeight = 800;

   this.helpLabel = new Label( this );
   this.helpLabel.scaledMinWidth = LEFTWIDTH;
   this.helpLabel.scaledMaxWidth = LEFTWIDTH;
   this.helpLabel.frameStyle = FrameStyle_Box;
   this.helpLabel.margin = 4;
   this.helpLabel.wordWrapping = true;
   this.helpLabel.useRichText = true;
   this.helpLabel.text = "<b>" + TITLE + " v"
                         + VERSION + "</b> <p>Create different color palette images from linear OSC dual narrowband images. Remove the stars, do a background neutralization and reduce the noise with a process of your choice before using this script! The image should still be linear!"
                         + "The resulting image is still linear and the scripts can create masks for each channel for later use!</p>";

   // Select the image to be procressed
   this.targetViewSelectorFrame = new Frame(this);
   this.targetViewSelectorFrame.scaledMinWidth = LEFTWIDTH;
   this.targetViewSelectorFrame.scaledMaxWidth = LEFTWIDTH;
   this.targetViewSelectLabel = new Label(this);
   this.targetViewSelectLabel.text = "Narrowband View (Ha + OIII):";
   this.targetViewSelectLabel.toolTip = "<p>Select the linear image to be converted. This image must be captured using a Duo Narrowband filter and must contain Ha and OIII.</p>";
   this.targetViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.targetViewSelector = new ViewList(this);
   this.targetViewSelector.scaledMaxWidth = 350;
   this.targetViewSelector.getMainViews();

   with(this.targetViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;
         add(this.targetViewSelectLabel);
         addSpacing(8);
         add(this.targetViewSelector);
         adjustToContents();
      }
   }

   this.targetViewSelector.onViewSelected = function(view) {

      this.dialog.previewTimer.stop();
      closeAllPreviewViews();

      OSC2HSOParameters.processedView = view;
      if (view) {
         var previewImage = preparePreview();

         var metadata = {
            width: previewImage.width,
            height: previewImage.height
         }

         SetRGBWorkingSpace(view);

         this.dialog.previewControl.SetPreview(previewImage, OSC2HSOParameters.processedView, metadata);
         this.dialog.previewControl.zoomToFit();
         this.dialog.previewControl.forceRedraw();
      }

      this.dialog.previewTimer.previewKey = "..";
      this.dialog.previewTimer.start();

   };

   // Select the palette....
   this.paletteFrame = new Frame(this);
   this.paletteFrame.scaledMinWidth = LEFTWIDTH;
   this.paletteFrame.scaledMaxWidth = LEFTWIDTH;
   this.paletteLabel = new Label(this);
   this.paletteLabel.text = "Palette:";
   this.paletteLabel.tooltip = "<p>Choose the palette for the image to be created.</p>";
   this.paletteLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.paletteList = new ComboBox(this);
   this.paletteList.scaledMaxWidth = 350;

   this.paletteList.addItem("HOO");
   this.paletteList.addItem("HSO");
   this.paletteList.addItem("HOS");
   this.paletteList.addItem("SHO");
   this.paletteList.addItem("OSH");
   this.paletteList.addItem("OHS");
   this.paletteList.addItem("ForaxSHO");
   this.paletteList.addItem("ForaxHOO");
   //this.paletteList.addItem("Dreamsplease");

   with(this.paletteFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;

         add(this.paletteLabel);
         addSpacing(8);
         add(this.paletteList);
         adjustToContents();
      }
   }


   this.paletteList.onItemSelected = function(index) {
      this.dialog.previewTimer.stop();

      OSC2HSOParameters.palette = this.itemText(index);

      this.dialog.previewTimer.previewKey = "..";
      this.dialog.previewTimer.start();
   }

   this.clipFrame = new Frame(this);
   this.clipFrame.scaledMinWidth = LEFTWIDTH;
   this.clipFrame.scaledMaxWidth = LEFTWIDTH;
   this.clipSlider = new NumericControl(this);
   this.clipSlider.label.text = "Blackpoint:  ";
   this.clipSlider.label.width = 250;
   this.clipSlider.toolTip = "<p>Increase/Decrease blackpoint clipping relative to the mean value (0 = no clipping, 1 = mean).</p>";
   this.clipSlider.setRange(0.0, 1.0);
   this.clipSlider.slider.setRange(0.0, 100.0);
   this.clipSlider.setPrecision(2);
   this.clipSlider.setReal(true);
   this.clipSlider.setValue(OSC2HSOParameters.clipBlack);

   this.clipSlider.onValueUpdated = function(value) {
      this.dialog.previewTimer.stop();
      OSC2HSOParameters.clipBlack = value;
      this.dialog.previewTimer.start();
   }

   with(this.clipFrame) {
      sizer = new VerticalSizer();

      with(sizer) {
         margin = 6;

         addSpacing(8);
         add(this.clipSlider);

         addSpacing(16);
      }
   }

   this.checkboxFrame = new Frame;
   this.checkboxFrame.scaledMinWidth = LEFTWIDTH;
   this.checkboxFrame.scaledMaxWidth = LEFTWIDTH;
   this.checkboxFrame.sizer = new HorizontalSizer;
   this.checkboxFrame.sizer.margin = 6;
   this.checkboxFrame.sizer.spacing = 6;
   this.checkboxFrame.sizer.addStretch();

   this.createMasks = new CheckBox(this);
   this.createMasks.text = "Create Masks";
   this.createMasks.checked = true;

   this.checkboxFrame.sizer.add(this.createMasks);

   this.buttonFrame = new Frame;
   this.buttonFrame.scaledMinWidth = LEFTWIDTH;
   this.buttonFrame.scaledMaxWidth = LEFTWIDTH;
   this.buttonFrame.sizer = new HorizontalSizer;
   this.buttonFrame.sizer.margin = 6;
   this.buttonFrame.sizer.spacing = 6;

   this.ok_Button = new ToolButton( this );
   this.ok_Button.icon = this.scaledResource( ":/icons/ok.png" );
   this.ok_Button.toolTip = "<p>Create the image with the selected color palette.</p>";
   this.ok_Button.onClick = () => {

      this.dialog.previewTimer.stop();
      if (this.dialog.busy) return;
      this.ok();
      closeAllPreviewViews();

      if (this.dialog.targetViewSelector.currentView) {

         OSC2HSOParameters.createMasks = this.createMasks.checked;
         process(OSC2HSOParameters.processedView);
      }
   };

   this.cancel_Button = new ToolButton( this );
   this.cancel_Button.icon = this.scaledResource( ":/process-interface/cancel.png" );
   this.cancel_Button.toolTip = "<p>Close this dialog.</p>";
   this.cancel_Button.onClick = () => {
      this.dialog.previewTimer.stop();
      if (this.dialog.busy) return;
      this.cancel();

      closeAllPreviewViews();
   };

   this.help_Button = new ToolButton( this );
   this.help_Button.icon = this.scaledResource(":/process-interface/browse-documentation.png");
   this.help_Button.setScaledFixedSize( 24, 24 );
   this.help_Button.toolTip = "<p>Shows the script documentation.</p>";
   this.help_Button.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      Dialog.browseScriptDocumentation("CreateHubblePaletteFromOSC");
      if (timerWasRunning) this.dialog.previewTimer.start();
   };


   this.buttonFrame.sizer.addSpacing(16);
   this.buttonFrame.sizer.add( this.ok_Button );
   this.buttonFrame.sizer.addStretch();
   this.buttonFrame.sizer.add( this.help_Button );
   this.buttonFrame.sizer.addStretch();
   this.buttonFrame.sizer.add( this.cancel_Button );
   this.buttonFrame.sizer.addSpacing(16);

   this.previewControl = new PreviewControl(this);
   this.previewControl.scaledMinWidth = 800;
   this.previewControl.minHeight = 600;

   // Layout the dialog
   this.sizer = new HorizontalSizer;
   this.sizer.margin = 6;
   this.sizer.spacing = 6;

   this.leftSizer = new VerticalSizer;
   this.leftSizer.margin = 6;
   this.leftSizer.spacing = 6;

   this.leftSizer.add(this.helpLabel);
   this.leftSizer.add(this.targetViewSelectorFrame);
   this.leftSizer.add(this.paletteFrame);
   this.leftSizer.add(this.clipFrame);
   this.leftSizer.add(this.checkboxFrame);

   this.leftSizer.addStretch();
   this.leftSizer.add(this.buttonFrame);

   this.sizer.add(this.leftSizer);
   this.sizer.add(this.previewControl);

   this.previewTimer = new Timer();
   this.previewTimer.interval = 0.50;
   this.previewTimer.periodic = true;
   this.previewTimer.dialog = this;
   this.previewTimer.busy = false;
   this.previewTimer.previewKey = "";
   this.previewTimer.updateCount = 0;

   this.previewTimer.onTimeout = function()  {
      let currentPreviewKey = getPreviewKey();
      let needsUpdate = (this.previewKey != currentPreviewKey);

      if (needsUpdate)
      {
         if (this.busy) return;

         this.dialog.targetViewSelector.enabled = false;
         this.dialog.ok_Button.enabled = false;
         this.dialog.cancel_Button.enabled = false;
         this.stop();
         this.busy = true;

         try {
            processPreview();

            if (OSC2HSOParameters.previewImage) {
               let previewImage = OSC2HSOParameters.previewImage;
               var metadata = {
                  width: previewImage.width,
                  height: previewImage.height
               }

               this.dialog.previewControl.SetPreview(previewImage, OSC2HSOParameters.processedView, metadata);
               this.dialog.previewControl.zoomToFit();
               this.dialog.previewControl.forceRedraw();
            }
         }
         catch(error) {
            Console.critical(error);
         }

         this.previewKey = currentPreviewKey;

         this.busy = false;
         this.dialog.targetViewSelector.enabled = true;
         this.dialog.ok_Button.enabled = true;
         this.dialog.cancel_Button.enabled = true;

         this.start();
      }

   }

   if (OSC2HSOParameters.processedView !== undefined) {
      this.targetViewSelector.currentView = OSC2HSOParameters.processedView;
      var previewImage = preparePreview();
      var metadata = {
         width: previewImage.width,
         height: previewImage.height
      }

      this.dialog.previewControl.SetPreview(previewImage, OSC2HSOParameters.processedView, metadata);
      this.dialog.previewControl.forceRedraw();

      this.dialog.previewTimer.start();
   }

   this.onHide = function() {
      this.previewTimer.stop();
      closeAllPreviewViews();
   }

}





OSC2HSODialog.prototype = new Dialog


function main() {

   var window = ImageWindow.activeWindow;
   if (!window.isNull) {
      OSC2HSOParameters.processedView = window.mainView;
   }

   let dialog = new OSC2HSODialog();
   dialog.execute();

}


main();






