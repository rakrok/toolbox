#include <pjsr/StdIcon.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/TextAlign.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/ColorSpace.jsh>
#include <pjsr/Interpolation.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/CheckState.jsh>
#include <pjsr/SampleType.jsh>
#include <pjsr/SectionBar.jsh>
#include "PixInsightToolsPreviewControl.jsh"


#feature-id EnhanceNebula :  Toolbox > EnhanceNebula

#feature-info  A script to brighten the nebula.<br/>\
   <br/>\
   A script to brighten the nebula. \
   <br/> \
   Copyright &copy; 2024 Juergen Terpe


#define VERSION "1.10"
#define TITLE  "EnhanceNebula"
#define TEXT   "Brightens the nebula on non-linear images. Based on the script BrightenNebula by Juergen Ehners and Frank Sackenheim. Special thanks for the permission to reuse this idea. The script requires StarNetV2 or StarXTerminator! "

#define LEFTWIDTH 400
#define LABELWIDTH 150

var enhanceNebulaParameters = {
   imageView: undefined,
   previewImage: undefined,
   previewStars: undefined,
   previewStarless: undefined,
   previewMode: 0,
   isStarless: true,
   useLuminanceMask: true,
   blackClip: 0.25,
   scaleFactor: 1.0,
   whiteClip: 1.0,
   protectHighlights: 1.0,
   protectHighlightsFactor: 0.9,
   maskSmoothingIterations: 3,
   amount: 1.0,

   // stores the current parameters values into the script instance
   save: function() {
        Parameters.set("isStarless", enhanceNebulaParameters.isStarless);
        Parameters.set("useLuminanceMask", enhanceNebulaParameters.useLuminanceMask);
        Parameters.set("blackClip", enhanceNebulaParameters.blackClip);

        Parameters.set("scaleFactor", enhanceNebulaParameters.scaleFactor);
        Parameters.set("whiteClip", enhanceNebulaParameters.whiteClip);
        Parameters.set("previewMode", enhanceNebulaParameters.previewMode);
        Parameters.set("amount", enhanceNebulaParameters.amount);
        Parameters.set("protectHighlights", enhanceNebulaParameters.protectHighlights);
        Parameters.set("protectHighlightsFactor", enhanceNebulaParameters.protectHighlightsFactor);
        Parameters.set("maskSmoothingIterations", enhanceNebulaParameters.maskSmoothingIterations);
    },

    // loads the script instance parameters
    load: function() {

         Console.writeln("Loading previous parameters...");
         if (Parameters.has("isStarless"))
            enhanceNebulaParameters.isStarless = Parameters.getBoolean("isStarless");
         if (Parameters.has("useLuminanceMask"))
            enhanceNebulaParameters.useLuminanceMask = Parameters.getBoolean("useLuminanceMask");

         if (Parameters.has("blackClip"))
            enhanceNebulaParameters.blackClip = Parameters.getReal("blackClip");
         if (Parameters.has("scaleFactor"))
            enhanceNebulaParameters.scaleFactor = Parameters.getReal("scaleFactor");
         if (Parameters.has("whiteClip"))
            enhanceNebulaParameters.whiteClip = Parameters.getReal("whiteClip");

         if (Parameters.has("previewMode"))
            enhanceNebulaParameters.previewMode = Parameters.getInteger("previewMode");

         if (Parameters.has("amount"))
            enhanceNebulaParameters.amount = Parameters.getReal("amount");
         if (Parameters.has("protectHighlights"))
            enhanceNebulaParameters.protectHighlights = Parameters.getReal("protectHighlights");
         if (Parameters.has("protectHighlightsFactor"))
            enhanceNebulaParameters.protectHighlightsFactor = Parameters.getReal("protectHighlightsFactor");
         if (Parameters.has("maskSmoothingIterations"))
            enhanceNebulaParameters.maskSmoothingIterations = Parameters.getInteger("maskSmoothingIterations");

    }
}

function getPreviewKey() {

   try {

      if (enhanceNebulaParameters.imageView == undefined)
         return ""
      else {
         let key = "View: "+ enhanceNebulaParameters.imageView.id
            + ", Mode: "+ enhanceNebulaParameters.previewMode
            + ", Starless: "+ enhanceNebulaParameters.isStarless
            + ", Scale: "+ enhanceNebulaParameters.scaleFactor
            + ", BlackClip: "+ enhanceNebulaParameters.blackClip
            + ", WhiteClip: "+ enhanceNebulaParameters.whiteClip
            + ", ProtectHighlights: " + enhanceNebulaParameters.protectHighlights
            + ", ProtectHighligthsFactor: " + enhanceNebulaParameters.protectHighlightsFactor
            + ", MaskIterations: " + enhanceNebulaParameters.maskSmoothingIterations
            + ", Amount: "+ enhanceNebulaParameters.amount;

         return key;
      }
   }
   catch(error) {
      Console.writeln(error);
      return "..."
   }
}


function closeView(view) {
   if (view !== undefined && view.window != null && !view.window.isNull) {
      try {
         Console.writeln("closing view: "+view.id);
         view.window.forceClose();
      }
      catch(error) {
         Console.writeln("could not close view..." + error);
      }
   }
}

function closeAllTempViews() {

   var temp2 = enhanceNebulaParameters.previewStars;
   enhanceNebulaParameters.previewStars = undefined;
   closeView(temp2);

   var temp3 = enhanceNebulaParameters.previewStarless;
   enhanceNebulaParameters.previewStarless = undefined;
   closeView(temp3);
}


function scaleImage(image) {
   var maxSize = Math.max(image.width, image.height);

   if (enhanceNebulaParameters.previewMode == 0) {
      var scale = Math.min(1800.0/maxSize, 1.0/3.0);
      image.resample(scale);
   }
   else if (enhanceNebulaParameters.previewMode == 1) {
      var scale = Math.min(3600.0/maxSize, 1.0/2.0);
      image.resample(scale);
   }

   return image;
}


function clone(view, postfix) {
   var P = new PixelMath;
   P.expression = "$T";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id+postfix;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return View.viewById(P.newImageId);
}

function cloneHidden(view) {
   var P = new PixelMath;
   P.expression = "$T";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.truncate = true;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + "_cloned";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.RGB;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return View.viewById(P.newImageId);
}

function subtract(view1, view2, identifier) {
   var P = new PixelMath;
   P.expression = "~((~"+ view1.id +")/(~"+ view2.id +"))";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = true;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = identifier;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view1);

   return View.viewById(P.newImageId);
}

function createStarless(view) {

   var clonedView = clone(view, "_starless");

   try {
      var P = new StarXTerminator;
      P.stars = false;
      P.unscreen = false;
      P.overlap = 0.20;

      P.executeOn(clonedView);

      return clonedView;

   } catch(error) {
      Console.writeln("No StarXTerminator found, trying StarNetV2! ");

      try {

            var P = new StarNet2;
            P.stride = StarNet2.prototype.itemOne;
            P.mask = false;

            P.executeOn(clonedView);
            return clonedView;

      } catch(error) {
            Console.error("No StarNetV2 available, you will need to create a star mask manually before running this script!");
      }
   }
   return undefined;
}

function createLightMask(view, starMask) {
   var P = new PixelMath;
   P.expression = "LUM = $T[0]*0.33333 + $T[0]*0.33333 + $T[0]*0.33333; iswitch(LUM < MIN, LUM/MIN*0.9, LUM < MAX, max((1.0+S)*LUM, 0.9), 0.9)";
   P.useSingleExpression = true;
   P.symbols = "LUM , MIN="+enhanceNebulaParameters.blackClip+", MAX="+enhanceNebulaParameters.whiteClip+", S="+enhanceNebulaParameters.scaleFactor;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + "_LM";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);
   var result = View.viewById(P.newImageId);

   if (enhanceNebulaParameters.protectHighlights < 1.0) {
      var P2 = new PixelMath;
      P2.expression = "iif($T>H, $T - $T*D, $T)";
      P2.symbols = "H="+enhanceNebulaParameters.protectHighlights+", D="+enhanceNebulaParameters.protectHighlightsFactor;
      P2.clearImageCacheAndExit = false;
      P2.cacheGeneratedImages = false;
      P2.generateOutput = true;
      P2.singleThreaded = false;
      P2.optimization = true;
      P2.use64BitWorkingImage = false;
      P2.rescale = false;
      P2.truncate = true;
      P2.truncateLower = 0;
      P2.truncateUpper = 1;
      P2.createNewImage = false;
      P2.executeOn(result);
   }

   for (let it=0; it<enhanceNebulaParameters.maskSmoothingIterations; it++) {
      var C = new Convolution;
      C.mode = Convolution.prototype.Parametric;
      C.sigma = 6.00;
      C.shape = 2.00;
      C.aspectRatio = 1.00;
      C.rotationAngle = 0.00;
      C.filterSource = "";
      C.rescaleHighPass = false;
      C.executeOn(result);
   }

   if (starMask !== undefined) {
      var P2 = new PixelMath;
      P2.expression = "$T - 1.8 * " + starMask.id;
      P2.clearImageCacheAndExit = false;
      P2.cacheGeneratedImages = false;
      P2.generateOutput = true;
      P2.singleThreaded = false;
      P2.optimization = true;
      P2.use64BitWorkingImage = false;
      P2.rescale = false;
      P2.truncate = true;
      P2.truncateLower = 0;
      P2.truncateUpper = 1;
      P2.createNewImage = false;
      P2.executeOn(result);
   }

   return result
}

function decreaseAmount(view) {
   var P = new CurvesTransformation;
   P.K = [ // x, y
      [0.00000, 0.00000],
      [1.00000, 0.5 + enhanceNebulaParameters.amount * 0.5]
   ];
   P.Kt = CurvesTransformation.prototype.AkimaSubsplines;
   P.executeOn(view);
}

function enhance(view) {
   decreaseAmount(view);

   var P = new PixelMath;
   P.expression = "1-(1-$T)*(1-$T)";
   P.expression1 = "";
   P.expression2 = "";
   P.expression3 = "";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;

   P.executeOn(view);
}

function addStars(view1, view2) {
   var P = new PixelMath;
   P.expression = "combine("+view1.id+ ", " + view2.id +", op_screen())";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;

   P.executeOn(view1);
}

function process() {
   if (enhanceNebulaParameters.imageView && enhanceNebulaParameters.imageView.id) {
      var view = enhanceNebulaParameters.imageView;

      if (enhanceNebulaParameters.isStarless) {
         var starless = clone(view);

         try {

            if (enhanceNebulaParameters.useLuminanceMask) {
              var lightMask = createLightMask(starless, undefined);
              starless.window.mask = lightMask.window;
            }
            enhance(starless);

            if (enhanceNebulaParameters.useLuminanceMask) {
               starless.window.removeMask();
            }

            view.beginProcess();
            view.image.assign(starless.image);
            view.endProcess();
         } finally {
            starless.window.forceClose();
            if (enhanceNebulaParameters.useLuminanceMask) {
               lightMask.window.forceClose();
            }
         }
      } else {
         try {

            var starless = createStarless(view);
            var stars = subtract(view, starless, view.id+"_enhanced_stars");

            if (enhanceNebulaParameters.useLuminanceMask) {
               var lightMask = createLightMask(starless, stars);
               starless.window.mask = lightMask.window;
            }
            enhance(starless);
            starless.window.removeMask();
            addStars(starless, stars);

            view.beginProcess();
            view.image.assign(starless.image);
            view.endProcess();
         } finally {
            starless.window.forceClose();
            stars.window.forceClose();
            if (enhanceNebulaParameters.useLuminanceMask) {
               lightMask.window.forceClose();
            }
         }
      }
   }
}

function preparePreview() {
   if (!enhanceNebulaParameters.previewImage) {
      Console.writeln("No image view available...");

   } else if (!enhanceNebulaParameters.previewStarless
       && !enhanceNebulaParameters.previewStars) {

      var image = new Image(enhanceNebulaParameters.previewImage);
      var previewWindow = new ImageWindow(image.width, image.height, image.numberOfChannels, image.bitsPerSample,
                                 image.sampleType == SampleType_Real, image.isColor, enhanceNebulaParameters.imageView.id + "_preview_temp" );
      var view = previewWindow.mainView;
      view.beginProcess( UndoFlag_NoSwapFile );
      view.image.assign( image );
      view.endProcess();

      Console.noteln("Preparing preview...");

      if (enhanceNebulaParameters.isStarless) {
         enhanceNebulaParameters.previewStarless = view;

      } else {
         var starless = createStarless(view);
         var stars = subtract(view, starless, view.id+"_enhanced_stars");

         enhanceNebulaParameters.previewStarless = starless;
         enhanceNebulaParameters.previewStars = stars;

         previewWindow.forceClose();
      }
      image.free();

   } else {
      Console.writeln("No new preview required...");
   }

}

function processPreview() {
   if (enhanceNebulaParameters.previewStarless) {

      Console.writeln("Clone starless image...");
      var starlessClone = cloneHidden(enhanceNebulaParameters.previewStarless, "_tmp_clone");

      if (enhanceNebulaParameters.useLuminanceMask) {

         var stars = enhanceNebulaParameters.previewStars;

         Console.writeln("Creating binary light mask...");
         var lightMask = createLightMask(starlessClone, stars);

         try {

            starlessClone.window.mask = lightMask.window;
            enhance(starlessClone);
            starlessClone.window.removeMask();

            if (stars) {
               Console.writeln('adding stars back...');
               addStars(starlessClone, stars);
            }

            enhanceNebulaParameters.previewImage.assign(starlessClone.image);


         } finally {
            starlessClone.window.forceClose();
            lightMask.window.forceClose();
         }
      }
      else {
         try {

            var stars = enhanceNebulaParameters.previewStars;
            enhance(starlessClone);

            if (stars) {
               addStars(starlessClone, stars);
            }

            enhanceNebulaParameters.previewImage.assign(starlessClone.image);

         } finally {
            starlessClone.window.forceClose();
         }

      }
   }
}

function EnhanceNebulaDialog() {
   this.__base__ = Dialog
   this.__base__();
   this.scaledMinWidth = 1280;
   this.scaledMinHeight = 800;

   let labelMinWidth = Math.round(this.font.width("Highlights:M"));
   let sliderMinWidth = 250;

   this.helpLabel = new Label( this );
   this.helpLabel.scaledMinWidth = LEFTWIDTH;
   this.helpLabel.scaledMaxWidth = LEFTWIDTH;
   this.helpLabel.frameStyle = FrameStyle_Box;
   this.helpLabel.margin = 4;
   this.helpLabel.wordWrapping = true;
   this.helpLabel.useRichText = true;
   this.helpLabel.text = "<b>" + TITLE + " v"
                        + VERSION + "</b> &mdash; "
                        + TEXT;

   this.imageGroup = new GroupBox(this);
   this.imageGroup.title = "Image";
   this.imageGroup.scaledMinWidth = LEFTWIDTH;
   this.imageGroup.scaledMaxWidth = LEFTWIDTH;
   this.imageGroup.sizer = new VerticalSizer();
   this.imageGroup.sizer.spacing = 0;

   this.imageViewSelectorFrame = new Frame(this);
   this.imageViewSelectorFrame.scaledMinWidth = LEFTWIDTH;
   this.imageViewSelectorFrame.scaledMaxWidth = LEFTWIDTH;
   this.imageViewSelectLabel = new Label(this);
   this.imageViewSelectLabel.text = "Target View:";
   this.imageViewSelectLabel.toolTip = "<p>Select the view to be processed.</p>";
   this.imageViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.imageViewSelector = new ViewList(this);
   this.imageViewSelector.scaledMaxWidth = 250;
   this.imageViewSelector.getMainViews();

   with(this.imageViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 4;
         spacing = 8;
         add(this.imageViewSelectLabel);
         addSpacing(8);
         add(this.imageViewSelector);
         adjustToContents();
      }
   }

   this.imageViewSelector.onViewSelected = function(view) {
      this.dialog.previewTimer.stop();

      enhanceNebulaParameters.imageView = view;
      closeAllTempViews();

      if (view && view.id) {
         var id = view.id.toLowerCase();

         var previewImage = new Image(enhanceNebulaParameters.imageView.image);
         previewImage = scaleImage(previewImage);
         var metadata = {
            width: previewImage.width,
            height: previewImage.height
         }

         this.dialog.previewControl.SetPreview(previewImage, enhanceNebulaParameters.imageView, metadata);
         this.dialog.previewControl.zoomToFit();
         enhanceNebulaParameters.previewImage = previewImage;
      }
      else
          this.dialog.previewControl.NoImage = true;

      this.dialog.previewTimer.previewKey = "Image_changed";
      this.dialog.previewTimer.start();
   }

   this.checkBoxFrame = new Frame;
   this.checkBoxFrame.scaledMinWidth = LEFTWIDTH;
   this.checkBoxFrame.scaledMaxWidth = LEFTWIDTH;
   this.checkBoxFrame.sizer = new HorizontalSizer;
   this.checkBoxFrame.sizer.margin = 6;
   this.checkBoxFrame.sizer.spacing = 6;

   this.starlessCheckbox = new CheckBox(this);
   this.starlessCheckbox.text = "Protect stars";
   this.starlessCheckbox.checked = !enhanceNebulaParameters.isStarless;
   this.starlessCheckbox.toolTip = "<p>Stars should not be enhanced and need to be extracted, which can be done using this script, if you are using StarXTerminator or StartNet2. If you are using a starless image you should switch this off to save time.</p>"

   this.starlessCheckbox.onCheck = function(checked) {
      this.dialog.previewTimer.stop();
      enhanceNebulaParameters.isStarless = !checked;
      closeAllTempViews();

      if (enhanceNebulaParameters.previewImage) {
         enhanceNebulaParameters.previewImage.free();
      }

      Console.noteln("Create new previews...");
      var previewImage = new Image(enhanceNebulaParameters.imageView.image);
      previewImage = scaleImage(previewImage);
      var metadata = {
         width: previewImage.width,
         height: previewImage.height
      }

      this.dialog.previewControl.SetPreview(previewImage, enhanceNebulaParameters.imageView, metadata);
      enhanceNebulaParameters.previewImage = previewImage;
      this.dialog.previewTimer.previewKey = "Starless_changed";
      this.dialog.previewTimer.start();
   }

   this.imageGroup.sizer.add(this.imageViewSelectorFrame);
   this.imageGroup.sizer.add(this.checkBoxFrame);
   this.imageGroup.sizer.addSpacing(8);

   this.previewModeFrame = new Frame(this);
   this.previewModeFrame.scaledMinWidth = LEFTWIDTH;
   this.previewModeFrame.scaledMaxWidth = LEFTWIDTH;
   this.previewModeFrame.sizer = new HorizontalSizer;
   this.previewModeFrame.sizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;
   this.previewModeFrame.sizer.margin = 6;
   this.previewModeFrameLabel = new Label(this);
   this.previewModeFrameLabel.textAlignment = TextAlign_Left | TextAlign_VertCenter;
   this.previewModeFrameLabel.text = "Preview Mode:";
   this.previewModeFrameLabel.toolTip = "<p>Select the mode for the preview. A small preview will be processed fast while a large preview will show more details but takes more time to be processed.</p>";

   this.previewModeSelector = new ComboBox(this);
   this.previewModeSelector.toolTip = "<p>Select the mode for the preview. A small preview will be processed fast while a large preview will show more details but takes more time to be processed.</p>";

   this.previewModeSelector.addItem("Small-sized Preview (fast)");
   this.previewModeSelector.addItem("Medium-sized Preview");
   this.previewModeSelector.addItem("Large-sized Preview (slow)");
   this.previewModeSelector.currentItem = 0;

   this.previewModeSelector.onItemSelected = function(index) {
      this.dialog.previewTimer.stop();

      closeAllTempViews();
      if (enhanceNebulaParameters.previewImage) {
         enhanceNebulaParameters.previewImage.free();
      }

      enhanceNebulaParameters.previewMode = index;
      var previewImage = new Image(enhanceNebulaParameters.imageView.image);
      previewImage = scaleImage(previewImage);
      var metadata = {
         width: previewImage.width,
         height: previewImage.height
      }

      this.dialog.previewControl.SetPreview(previewImage, enhanceNebulaParameters.imageView, metadata);
      this.dialog.previewControl.zoomToFit();
      enhanceNebulaParameters.previewImage = previewImage;
      this.dialog.previewTimer.previewKey = "....";
      this.dialog.previewTimer.start();
   };

   this.previewModeFrame.sizer.add(this.previewModeFrameLabel);
   this.previewModeFrame.sizer.addStretch();
   this.previewModeFrame.sizer.add(this.previewModeSelector);

   this.luminanceMaskCheckbox = new CheckBox(this);
   this.luminanceMaskCheckbox.text = "Luminance Mask";
   this.luminanceMaskCheckbox.checked = enhanceNebulaParameters.useLuminanceMask;
   this.luminanceMaskCheckbox.toolTip = "<p>Disable the luminance mask if you prefer to manage the contrast of the image later using the process 'CurvesTransformation'.</p>"

   this.luminanceMaskCheckbox.onCheck = function(checked) {
      this.dialog.previewTimer.stop();
      enhanceNebulaParameters.useLuminanceMask = checked;
      this.dialog.blackSlider.enabled = checked;
      this.dialog.whiteSlider.enabled = checked;
      this.dialog.scaleSlider.enabled = checked;
      this.dialog.protectHighlightsLimitSlider.enabled = checked;
      this.dialog.resetHighlightsLimitButton.enabled = checked;
      this.dialog.protectHighlightsFactorSlider.enabled = checked;
      this.dialog.resetHighlightsFactorButton.enabled = checked;
      this.dialog.maskSmoothingBox.enabled = checked;

      closeAllTempViews();
      if (enhanceNebulaParameters.previewImage) {
         enhanceNebulaParameters.previewImage.free();
      }
      var previewImage = new Image(enhanceNebulaParameters.imageView.image);
      previewImage = scaleImage(previewImage);
      var metadata = {
         width: previewImage.width,
         height: previewImage.height
      }

      this.dialog.previewControl.SetPreview(previewImage, enhanceNebulaParameters.imageView, metadata);
      enhanceNebulaParameters.previewImage = previewImage;

      this.dialog.previewTimer.previewKey = "LUM_changed";
      this.dialog.previewTimer.start();
   }

   this.checkBoxFrame.sizer.add(this.luminanceMaskCheckbox);
   this.checkBoxFrame.sizer.addStretch();
   this.checkBoxFrame.sizer.add(this.starlessCheckbox);


   this.contrastSlidersBar = new SectionBar(this, "Enhancement parameters");
   this.contrastSlidersBar.scaledMinWidth = LEFTWIDTH;
   this.contrastSlidersBar.scaledMaxWidth = LEFTWIDTH;
   this.contrastSlidersFrame = new Control(this);
   this.contrastSlidersFrame.scaledMinWidth = LEFTWIDTH;
   this.contrastSlidersFrame.scaledMaxWidth = LEFTWIDTH;
   this.contrastSlidersFrame.sizer = new VerticalSizer;
   this.contrastSlidersFrame.sizer.addSpacing(8);
   this.contrastSlidersFrame.backgroundColor = 0xFFD8D7D3;

   this.contrastSlidersBar.onToggleSection =  function( section, toggleBegin ) {
      if ( !toggleBegin ) {
         section.dialog.adjustToContents();
         section.dialog.setVariableSize();
      }
   }

   this.contrastSlidersBar.setSection( this.contrastSlidersFrame );

   this.blackSlider = new NumericControl(this);
   this.blackSlider.setRange(0.0, 1.0);
   this.blackSlider.slider.setRange(0.0, 1000.0);
   this.blackSlider.setPrecision(3);
   this.blackSlider.setReal(true);
   this.blackSlider.enableFixedPrecision(true);
   this.blackSlider.setValue(enhanceNebulaParameters.blackClip);
   this.blackSlider.label.text =  "Shadows: ";
   this.blackSlider.label.minWidth = labelMinWidth;
   this.blackSlider.slider.scaledMinWidth = sliderMinWidth;
   this.blackSlider.toolTip = "<p>Increase this value to reduce the shadow brightness to darken the background.</p>";

   this.blackSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      enhanceNebulaParameters.blackClip = t;
      this.dialog.previewTimer.start();
   };

   this.resetblackSliderButton = new ToolButton( this );
   this.resetblackSliderButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetblackSliderButton.setScaledFixedSize( 24, 24 );
   this.resetblackSliderButton.toolTip = "<p>Reset the shadow brightness parameter.</p>";
   this.resetblackSliderButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      enhanceNebulaParameters.blackClip = 0.25;
      this.dialog.blackSlider.setValue(enhanceNebulaParameters.blackClip);

      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.blackHorizontal = new HorizontalSizer(this);
   this.blackHorizontal.add(this.blackSlider);
   this.blackHorizontal.add(this.resetblackSliderButton);

   this.contrastSlidersFrame.sizer.add(this.blackHorizontal);
   this.contrastSlidersFrame.sizer.addSpacing(8);


   this.scaleSlider = new NumericControl(this);
   this.scaleSlider.setRange(0.0, 2.0);
   this.scaleSlider.slider.setRange(0.0, 5000.0);
   this.scaleSlider.setPrecision(3);
   this.scaleSlider.setReal(true);
   this.scaleSlider.enableFixedPrecision(true);
   this.scaleSlider.setValue(enhanceNebulaParameters.scaleFactor);
   this.scaleSlider.label.text =  "Scale: ";
   this.scaleSlider.label.minWidth = labelMinWidth;
   this.scaleSlider.slider.scaledMinWidth = sliderMinWidth;
   this.scaleSlider.toolTip = "<p>Increase or decrease the scale to control the amount of brightness of darker regions. Increasing the scale whill bring brighter zones into saturation.</p>";


   this.scaleSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      enhanceNebulaParameters.scaleFactor = t;
      this.dialog.previewTimer.start();
   };

   this.resetScaleSliderButton = new ToolButton( this );
   this.resetScaleSliderButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetScaleSliderButton.setScaledFixedSize( 24, 24 );
   this.resetScaleSliderButton.toolTip = "<p>Reset the scale parameter.</p>";
   this.resetScaleSliderButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      enhanceNebulaParameters.scaleFactor = 1.0;
      this.dialog.scaleSlider.setValue(enhanceNebulaParameters.scaleFactor);

      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.scaleHorizontal = new HorizontalSizer(this);
   this.scaleHorizontal.add(this.scaleSlider);
   this.scaleHorizontal.add(this.resetScaleSliderButton);

   this.contrastSlidersFrame.sizer.add(this.scaleHorizontal);
   this.contrastSlidersFrame.sizer.addSpacing(8);

   this.whiteSlider = new NumericControl(this);

   this.whiteSlider.setRange(0.0, 1.0);
   this.whiteSlider.slider.setRange(0.0, 1000.0);
   this.whiteSlider.setPrecision(3);
   this.whiteSlider.setReal(true);
   this.whiteSlider.enableFixedPrecision(true);
   this.whiteSlider.setValue(enhanceNebulaParameters.whiteClip);
   this.whiteSlider.label.text = "Highlights: ";
   this.whiteSlider.label.minWidth = labelMinWidth;
   this.whiteSlider.slider.scaledMinWidth = sliderMinWidth;
   this.whiteSlider.toolTip = "<p>Increase or decrease the highlights to change the brightness of brighter regions to avoid loosing details in the brightest nebula areas.</p>";

   this.whiteSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      enhanceNebulaParameters.whiteClip = t;

      this.dialog.previewTimer.start();
   };

   this.resetWhiteSliderButton = new ToolButton( this );
   this.resetWhiteSliderButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetWhiteSliderButton.setScaledFixedSize( 24, 24 );
   this.resetWhiteSliderButton.toolTip = "<p>Reset the scale parameter.</p>";
   this.resetWhiteSliderButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      enhanceNebulaParameters.whiteClip = 1.0;
      this.dialog.whiteSlider.setValue(enhanceNebulaParameters.whiteClip);

      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.whiteHorizontal = new HorizontalSizer(this);
   this.whiteHorizontal.add(this.whiteSlider);
   this.whiteHorizontal.add(this.resetWhiteSliderButton);

   this.contrastSlidersFrame.sizer.add(this.whiteHorizontal);
   this.contrastSlidersFrame.sizer.addSpacing(8);

   this.amountSlider = new NumericControl(this);

   this.amountSlider.setRange(0.0, 1.0);
   this.amountSlider.slider.setRange(0.0, 1000.0);
   this.amountSlider.setPrecision(3);
   this.amountSlider.setReal(true);
   this.amountSlider.enableFixedPrecision(true);
   this.amountSlider.setValue(enhanceNebulaParameters.whiteClip);
   this.amountSlider.label.text = "Amount: ";
   this.amountSlider.label.minWidth = labelMinWidth;
   this.amountSlider.toolTip = "<p>Increasing this value will increase the amount of enhancement, decrease the amount to reduce the enhancement.</p>";

   this.amountSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      enhanceNebulaParameters.amount = t;

      this.dialog.previewTimer.start();
   };

   this.resetAmountSliderButton = new ToolButton( this );
   this.resetAmountSliderButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetAmountSliderButton.setScaledFixedSize( 24, 24 );
   this.resetAmountSliderButton.toolTip = "<p>Reset the amount parameter.</p>";
   this.resetAmountSliderButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      enhanceNebulaParameters.whiteClip = 1.0;
      this.dialog.amountSlider.setValue(enhanceNebulaParameters.whiteClip);

      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.amountHorizontal = new HorizontalSizer(this);
   this.amountHorizontal.add(this.amountSlider);
   this.amountHorizontal.add(this.resetAmountSliderButton);

   this.contrastSlidersFrame.sizer.add(this.amountHorizontal);
   this.contrastSlidersFrame.sizer.addSpacing(12);

   this.maskSmoothingLabel = new Label(this);
   this.maskSmoothingLabel.text = "Mask Smoothing Iterations:";

   this.maskSmoothingBox = new SpinBox(this);
   this.maskSmoothingBox.minValue = 1;
   this.maskSmoothingBox.maxValue = 20;
   this.maskSmoothingBox.value = enhanceNebulaParameters.maskSmoothingIterations;
   this.maskSmoothingBox.stepSize = 1;
   this.maskSmoothingBox.toolTip = "<p>Increase the number of iterations to smooth the light mask!</p>";

   this.maskSmoothingBox.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      enhanceNebulaParameters.maskSmoothingIterations = t;

      this.dialog.previewTimer.start();
   }

   this.maskSmoothingHorizontal = new HorizontalSizer();
   this.maskSmoothingHorizontal.scaledMinWidth = LEFTWIDTH;
   this.maskSmoothingHorizontal.scaledMaxWidth = LEFTWIDTH;
   this.maskSmoothingHorizontal.add(this.maskSmoothingLabel);
   this.maskSmoothingHorizontal.addSpacing(8);
   this.maskSmoothingHorizontal.add(this.maskSmoothingBox);
   this.contrastSlidersFrame.sizer.add(this.maskSmoothingHorizontal);


   this.protectHighlightsBar = new SectionBar(this, "Highlights protection");
   this.protectHighlightsBar.scaledMinWidth = LEFTWIDTH;
   this.protectHighlightsBar.scaledMaxWidth = LEFTWIDTH;
   this.protectHighlightsControl = new Control(this);
   this.protectHighlightsControl.scaledMinWidth = LEFTWIDTH;
   this.protectHighlightsControl.scaledMaxWidth = LEFTWIDTH;
   this.protectHighlightsControl.backgroundColor = 0xFFD8D7D3;

   this.protectHighlightsBar.onToggleSection =  function( section, toggleBegin ) {
      if ( !toggleBegin ) {
         section.dialog.adjustToContents();
         section.dialog.setVariableSize();
      }
   }

   this.protectHighlightsLimitSlider = new NumericControl(this);
   this.protectHighlightsLimitSlider.scaledMaxWidth = LEFTWIDTH-32;
   this.protectHighlightsLimitSlider.label.text = "Max. Luminance: ";
   this.protectHighlightsLimitSlider.label.minWidth = labelMinWidth;
   this.protectHighlightsLimitSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.protectHighlightsLimitSlider.toolTip = "<p>Increase/Decrease this value to protect more or less highlights.</p>";
   this.protectHighlightsLimitSlider.setRange(0.50, 1.0);
   this.protectHighlightsLimitSlider.slider.setRange(0.0, 100000.0);
   this.protectHighlightsLimitSlider.setPrecision(4);
   this.protectHighlightsLimitSlider.setReal(true);
   this.protectHighlightsLimitSlider.setValue(enhanceNebulaParameters.protectHighlights);

   this.protectHighlightsLimitSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      enhanceNebulaParameters.protectHighlights = t;

      this.dialog.previewTimer.start();
   };

   this.resetHighlightsLimitButton = new ToolButton( this );
   this.resetHighlightsLimitButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetHighlightsLimitButton.setScaledFixedSize( 24, 24 );
   this.resetHighlightsLimitButton.toolTip = "<p>Reset the Highlights protection to its default (no highlights protected).</p>";
   this.resetHighlightsLimitButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      enhanceNebulaParameters.protectHighlights = 1.0;
      this.dialog.protectHighlightsLimitSlider.setValue(enhanceNebulaParameters.protectHighlights);

      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.highlightsLimitHorizontal = new HorizontalSizer();
   this.highlightsLimitHorizontal.scaledMinWidth = LEFTWIDTH;
   this.highlightsLimitHorizontal.scaledMaxWidth = LEFTWIDTH;
   this.highlightsLimitHorizontal.add(this.protectHighlightsLimitSlider);
   this.highlightsLimitHorizontal.add(this.resetHighlightsLimitButton);


   // enhanceNebulaParameters.protectHighlightsFactor
   this.protectHighlightsFactorSlider = new NumericControl(this);
   this.protectHighlightsFactorSlider.scaledMaxWidth = LEFTWIDTH-32;
   this.protectHighlightsFactorSlider.label.text = "Factor: ";
   this.protectHighlightsFactorSlider.label.width = LABELWIDTH;
   this.protectHighlightsFactorSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.protectHighlightsFactorSlider.toolTip = "<p>Increase/Decrease this value to darken more or less the protected highlights in the luminance mask.</p>";
   this.protectHighlightsFactorSlider.setRange(0.00, 1.0);
   this.protectHighlightsFactorSlider.slider.setRange(0.0, 100000.0);
   this.protectHighlightsFactorSlider.setPrecision(4);
   this.protectHighlightsFactorSlider.setReal(true);
   this.protectHighlightsFactorSlider.setValue(enhanceNebulaParameters.protectHighlightsFactor);

   this.protectHighlightsFactorSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      enhanceNebulaParameters.protectHighlightsFactor = t;

      this.dialog.previewTimer.start();
   };

   this.resetHighlightsFactorButton = new ToolButton( this );
   this.resetHighlightsFactorButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetHighlightsFactorButton.setScaledFixedSize( 24, 24 );
   this.resetHighlightsFactorButton.toolTip = "<p>Reset the Highlights protection factor to its default.</p>";
   this.resetHighlightsFactorButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      enhanceNebulaParameters.protectHighlightsFactor = 0.9;
      this.dialog.protectHighlightsFactorSlider.setValue(enhanceNebulaParameters.protectHighlightsFactor);

      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.highlightsFactorHorizontal = new HorizontalSizer();
   this.highlightsFactorHorizontal.scaledMinWidth = LEFTWIDTH;
   this.highlightsFactorHorizontal.scaledMaxWidth = LEFTWIDTH;
   this.highlightsFactorHorizontal.add(this.protectHighlightsFactorSlider);
   this.highlightsFactorHorizontal.add(this.resetHighlightsFactorButton);

   with(this.protectHighlightsControl) {
      sizer = new VerticalSizer();

      with(sizer) {
         addSpacing(8);
         add(this.highlightsLimitHorizontal);
         addSpacing(8);
         add(this.highlightsFactorHorizontal);
         addSpacing(8);
      }
   }

   this.protectHighlightsBar.setSection( this.protectHighlightsControl );

   this.buttonFrame = new Frame;
   this.buttonFrame.scaledMinWidth = LEFTWIDTH;
   this.buttonFrame.scaledMaxWidth = LEFTWIDTH;
   this.buttonFrame.sizer = new HorizontalSizer;
   this.buttonFrame.sizer.addSpacing(8);

   this.newInstanceButton = new ToolButton( this );
   this.newInstanceButton.icon = this.scaledResource( ":/process-interface/new-instance.png" );
   this.newInstanceButton.setScaledFixedSize( 24, 24 );
   this.newInstanceButton.onMousePress = () => {
      enhanceNebulaParameters.save();
      this.newInstance();
   }

   this.ok_Button = new ToolButton( this );
   this.ok_Button.icon = this.scaledResource( ":/process-interface/execute.png" );
   this.ok_Button.setScaledFixedSize( 24, 24 );
   this.ok_Button.toolTip = "<p>Change the target view image using the specified parameters.</p>";
   this.ok_Button.onClick = () => {
      this.dialog.previewTimer.stop();
      if (this.dialog.busy) return;

      enhanceNebulaParameters.save();
      process();
      closeAllTempViews();
      this.ok();
   };

   this.cancel_Button = new ToolButton( this );
   this.cancel_Button.icon = this.scaledResource( ":/process-interface/cancel.png" );
   this.cancel_Button.setScaledFixedSize( 24, 24 );
   this.cancel_Button.toolTip = "<p>Close this dialog with no changes.</p>";
   this.cancel_Button.onClick = () => {
      this.dialog.previewTimer.stop();
      if (this.dialog.busy) return;
      closeAllTempViews();
      this.cancel();
   };

   this.help_Button = new ToolButton( this );
   this.help_Button.icon = this.scaledResource(":/process-interface/browse-documentation.png");
   this.help_Button.setScaledFixedSize( 24, 24 );
   this.help_Button.toolTip = "<p>Shows the script documentation.</p>";
   this.help_Button.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      Dialog.browseScriptDocumentation("EnhanceNebula");
      if (timerWasRunning) this.dialog.previewTimer.start();
   };

   this.buttonFrame.sizer.add( this.newInstanceButton );
   this.buttonFrame.sizer.addSpacing(32);
   this.buttonFrame.sizer.add( this.ok_Button );
   this.buttonFrame.sizer.addSpacing(32);
   this.buttonFrame.sizer.add( this.cancel_Button );
   this.buttonFrame.sizer.addStretch();
   this.buttonFrame.sizer.add( this.help_Button );
   this.buttonFrame.sizer.addSpacing(8);

   this.previewControl = new PreviewControl(this);
   this.previewControl.scaledMinWidth = 800;
   this.previewControl.minHeight = 600;

   this.hintLabel = new Label( this );
   this.hintLabel.textAlignment = TextAlign_Center | TextAlign_VertCenter;
   this.hintLabel.margin = 4;
   this.hintLabel.textColor = 0xFF0000;
   this.hintLabel.text = "";

   this.sizerLeft = new VerticalSizer;
   this.sizerLeft.scaledMinWidth = LEFTWIDTH;
   this.sizerLeft.scaledMaxWidth = LEFTWIDTH;
   this.sizerLeft.add(this.helpLabel);
   this.sizerLeft.addSpacing(16);
   this.sizerLeft.add(this.imageGroup);
   this.sizerLeft.addSpacing(8);

   this.sizerLeft.add(this.previewModeFrame);
   this.sizerLeft.addSpacing(16);
   this.sizerLeft.add(this.contrastSlidersBar);
   this.sizerLeft.add(this.contrastSlidersFrame);
   this.sizerLeft.addSpacing(32);

   this.sizerLeft.add(this.protectHighlightsBar);
   this.sizerLeft.add(this.protectHighlightsControl);
   this.sizerLeft.addSpacing(8);
   this.sizerLeft.addStretch();


   this.sizerLeft.add(this.hintLabel);
   this.sizerLeft.addStretch();

   this.sizerLeft.add(this.buttonFrame);
   this.sizerRight = new VerticalSizer;
   this.sizerRight.add(this.previewControl);

   this.sizer = new HorizontalSizer;
   this.sizer.margin = 8;
   this.sizer.spacing = 6;
   this.sizer.add(this.sizerLeft);
   this.sizer.add(this.sizerRight);

   this.previewTimer = new Timer();
   this.previewTimer.interval = 2.00;
   this.previewTimer.periodic = true;
   this.previewTimer.dialog = this;
   this.previewTimer.busy = false;
   this.previewTimer.previewKey = "";
   this.previewTimer.updateCount = 0;

   this.previewTimer.onTimeout = function()  {
      let currentPreviewKey = getPreviewKey();
      let needsUpdate = (this.previewKey != currentPreviewKey);

      if (needsUpdate)
      {
         if (this.busy) return;
         this.stop();

         this.dialog.imageViewSelector.enabled = false;
         this.dialog.ok_Button.enabled = false;
         this.dialog.cancel_Button.enabled = false;

         this.busy = true;

         try {

            if (!enhanceNebulaParameters.previewStarless
                && !enhanceNebulaParameters.previewStars) {
               this.dialog.hintLabel.text = "Preparing preview...";
               this.dialog.hintLabel.visible = true;
            }
            preparePreview();
            this.dialog.hintLabel.visible = false;

            processPreview();

            if (enhanceNebulaParameters.previewImage) {
               let previewImage = enhanceNebulaParameters.previewImage;
               var metadata = {
                  width: previewImage.width,
                  height: previewImage.height
               }

               this.dialog.previewControl.SetPreview(previewImage, enhanceNebulaParameters.imageView, metadata);
               this.dialog.previewControl.forceRedraw();
            }
         }
         catch(error) {
            Console.critical(error);
         }

         this.previewKey = currentPreviewKey;

         this.busy = false;
         this.dialog.imageViewSelector.enabled = true;
         this.dialog.ok_Button.enabled = true;
         this.dialog.cancel_Button.enabled = true;
         this.start();
      }

   }


   this.onHide = function() {
      this.dialog.previewTimer.stop();
      closeAllTempViews();

      if (enhanceNebulaParameters.previewImage) {
         enhanceNebulaParameters.previewImage.free();
      }
   }

   if (enhanceNebulaParameters.imageView) {
      this.imageViewSelector.currentView = enhanceNebulaParameters.imageView;
      closeAllTempViews();

      if (enhanceNebulaParameters.previewImage) {
         enhanceNebulaParameters.previewImage.free();
      }

      var previewImage = new Image(enhanceNebulaParameters.imageView.image);
      previewImage = scaleImage(previewImage);
      var metadata = {
         width: previewImage.width,
         height: previewImage.height
      }


      this.previewControl.SetPreview(previewImage, enhanceNebulaParameters.imageView, metadata);
      this.previewControl.zoomToFit();
      enhanceNebulaParameters.previewImage = previewImage;

      this.previewTimer.start();
   }
}

EnhanceNebulaDialog.prototype = new Dialog

function main() {

   // is script started from an instance in global or view target context?
   if (Parameters.isViewTarget) {
      enhanceNebulaParameters.load();
      enhanceNebulaParameters.imageView = Parameters.targetView;
      process();
      return;
   } else if (Parameters.isGlobalTarget) {
      // then load the parameters from the instance and continue

      enhanceNebulaParameters.load();
   }

   var window = ImageWindow.activeWindow;
   if (!window.isNull) {
      enhanceNebulaParameters.imageView = window.mainView;
   }

   let dialog = new EnhanceNebulaDialog();
   dialog.execute();

}

main();
