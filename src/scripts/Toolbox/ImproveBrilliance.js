#include <pjsr/StdIcon.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/TextAlign.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/ColorSpace.jsh>
#include <pjsr/Interpolation.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/CheckState.jsh>
#include <pjsr/SampleType.jsh>
#include <pjsr/SectionBar.jsh>
#include "PixInsightToolsPreviewControl.jsh"


#feature-id ImproveBrilliance :  Toolbox > ImproveBrilliance

#feature-info  A script to improve the brilliance of a colored imaged.<br/>\
   <br/>\
   A script to make a color image to look more brilliant. \
   <br/> \
   Copyright &copy; 2024 Juergen Terpe


#define VERSION "2.3"
#define TITLE  "Improve Brilliance"
#define TEXT   "Improve the brilliance of the image using contrast, constrast limited adaptive histogram equalization and sharpening the luminance of the image."

#define LEFTWIDTH 400

var BrillianceParameters = {
   contrastDelta : 0.0,
   amount: 0.5,
   colorEnhance: 0.5,
   sharpenFactor: 0.0,
   increaseEdges: false,
   luminance: 0.0,
   autoPreview: false,
   rgbView: undefined,
   previewImage: undefined,
   previewMode: 0,
   manualUpdate: false,

    // stores the current parameters values into the script instance
    save: function() {

        Parameters.set("contrastDelta", BrillianceParameters.contrastDelta);
        Parameters.set("amount", BrillianceParameters.amount);
        Parameters.set("colorEnhance", BrillianceParameters.colorEnhance);
        Parameters.set("sharpenFactor", BrillianceParameters.sharpenFactor);
        Parameters.set("increaseEdges", BrillianceParameters.increaseEdges);
        Parameters.set("luminance", BrillianceParameters.luminance);
        Parameters.set("previewMode", BrillianceParameters.previewMode);
        Parameters.set("autoPreview", BrillianceParameters.autoPreview);
    },

    // loads the script instance parameters
    load: function() {

         Console.writeln("Loading previous parameters...");

         if (Parameters.has("contrastDelta"))
            BrillianceParameters.contrastDelta = Parameters.getReal("contrastDelta");
         if (Parameters.has("colorEnhance"))
            BrillianceParameters.colorEnhance = Parameters.getReal("colorEnhance");
         if (Parameters.has("sharpenFactor"))
            BrillianceParameters.sharpenFactor = Parameters.getReal("sharpenFactor");
         if (Parameters.has("increaseEdges"))
            BrillianceParameters.increaseEdges = Parameters.getBoolean("increaseEdges");
         if (Parameters.has("luminance"))
            BrillianceParameters.luminance = Parameters.getReal("luminance");
         if (Parameters.has("previewMode"))
            BrillianceParameters.previewMode = Parameters.getInteger("previewMode");
         if (Parameters.has("autoPreview"))
            BrillianceParameters.autoPreview = Parameters.getBoolean("autoPreview");

    }
}


function getPreviewKey() {

   try {

      if (!BrillianceParameters.previewImage)
         return ""
      else {
         let key = "Delta: "+ BrillianceParameters.contrastDelta
            + ", Amount: "+ BrillianceParameters.amount
            + ", Color: "+ BrillianceParameters.colorEnhance
            + ", Luminance: "+ BrillianceParameters.luminance
            + ", Sharpen: "+BrillianceParameters.sharpenFactor
            + ", Edges: "+BrillianceParameters.increaseEdges
            + ", Mode: "+BrillianceParameters.previewMode
            + ", View: "+ BrillianceParameters.rgbView.id;

         return key;
      }
   }
   catch(error) {
      Console.writeln(error);
      return "..."
   }
}

function closeView(view) {
   if (view !== undefined && view.window != null && !view.window.isNull) {
      try {
         Console.writeln("closing view: "+view.id);
         view.window.forceClose();
      }
      catch(error) {
         Console.writeln("could not close view...");
      }
   }
}

function createLuminance(view, swapfile = true) {
   var P = new PixelMath;
   P.expression = "$T[0]*0.333333 + $T[1]*0.333333 + $T[2]*0.333333";
   P.useSingleExpression = true;
   P.symbols = "";
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = true;
   P.rescale = true;
   P.truncate = false;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id+"_luminance";
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.executeOn(view, swapfile);

   return View.viewById(P.newImageId);
}

function contrastCurve(view, delta, swapfile = true) {

   var d = delta;
   if (d<0.0) d = 0.0; else if (d>1.0) d = 1.0;
   var dy = 0.2 * d;

   var P = new CurvesTransformation;
   P.R = [ // x, y
      [0.00000, 0.00000],
      [1.00000, 1.00000]
   ];
   P.Rt = CurvesTransformation.prototype.AkimaSubsplines;
   P.G = [ // x, y
      [0.00000, 0.00000],
      [1.00000, 1.00000]
   ];
   P.Gt = CurvesTransformation.prototype.AkimaSubsplines;
   P.B = [ // x, y
      [0.00000, 0.00000],
      [1.00000, 1.00000]
   ];
   P.Bt = CurvesTransformation.prototype.AkimaSubsplines;
   P.K = [ // x, y
      [0.00000, 0.00000],
      [0.25000, 0.25000-dy],
      [0.75000, 0.75000+dy],
      [1.00000, 1.00000]
   ];
   P.Kt = CurvesTransformation.prototype.AkimaSubsplines;

   P.executeOn(view, swapfile);
}

function improveLuminance(view, swapfile=true) {
   var P = new PixelMath;
   P.expression = "$T - ($T - min($T))*0.05";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.truncate = true;
   P.createNewImage = false;
   P.executeOn(view, swapfile);
}

function luminanceCurve(view, lum, swapfile=true) {

   var P = new CurvesTransformation;

   P.K = [ // x, y
      [0.00000, 0.00000],
      [0.5, 0.5 + 0.25*lum],
      [1.00000, 1.00000]
   ];
   P.Kt = CurvesTransformation.prototype.AkimaSubsplines;

   P.executeOn(view, swapfile);
}

function clahe(view, swapfile=true) {

   var P1 = new LocalHistogramEqualization;
   P1.radius = 32;
   P1.histogramBins = LocalHistogramEqualization.prototype.Bit10;
   P1.slopeLimit = 1.75;
   P1.amount = 0.165;
   P1.circularKernel = true;
   P1.executeOn(view, swapfile);

   var P2 = new LocalHistogramEqualization;
   P2.radius = 64;
   P2.histogramBins = LocalHistogramEqualization.prototype.Bit8;
   P2.slopeLimit = 1.75;
   P2.amount = 0.165;
   P2.circularKernel = true;
   P2.executeOn(view, swapfile);
}

function cloneHidden(view, swapfile=true) {
   var P = new PixelMath;
   P.expression = "$T";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.truncate = true;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + "_cloned";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.RGB;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view, swapfile);

   return View.viewById(P.newImageId);
}

function contrastEdges(view, swapfile=true) {
   var P = new CurvesTransformation;

   P.K = [ // x, y
      [0.00000, 0.00000],
      [0.25, 0.05],
      [0.75, 0.85],
      [1.00000, 1.00000]
   ];
   P.Kt = CurvesTransformation.prototype.AkimaSubsplines;

   P.executeOn(view, swapfile);
}

function highlightEdges(view, swapfile = true) {

   var edgeView = cloneHidden(view);

   var P = new PixelMath;
   P.expression = "0.65*erosion($T, 3)";
   P.expression1 = "";
   P.expression2 = "";
   P.expression3 = "";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;
   P.showNewImage = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(edgeView, swapfile);

   contrastEdges(edgeView, swapfile);

   var P2 = new PixelMath;
   P2.expression = "combine($T, "+edgeView.id+", op_screen())";
   P2.useSingleExpression = true;
   P2.symbols = "";
   P2.clearImageCacheAndExit = false;
   P2.cacheGeneratedImages = false;
   P2.generateOutput = true;
   P2.singleThreaded = false;
   P2.optimization = true;
   P2.use64BitWorkingImage = false;
   P2.rescale = false;
   P2.rescaleLower = 0;
   P2.rescaleUpper = 1;
   P2.truncate = true;
   P2.truncateLower = 0;
   P2.truncateUpper = 1;
   P2.createNewImage = false;
   P2.showNewImage = false;
   P2.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P2.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P2.executeOn(view);

   closeView(edgeView, swapfile);
}

function sharpening(view, swapfile=true) {

   if (BrillianceParameters.sharpenFactor>0.0) {

      var P = new MultiscaleLinearTransform;
      P.layers = [ // enabled, biasEnabled, bias, noiseReductionEnabled, noiseReductionThreshold, noiseReductionAmount, noiseReductionIterations
         [true, true, 0.000, false, 3.000, 1.00, 1],
         [true, true, 0.100 * BrillianceParameters.sharpenFactor*0.25, false, 3.000, 1.00, 1],
         [true, true, 0.200 * BrillianceParameters.sharpenFactor*0.25, false, 3.000, 1.00, 1],
         [true, true, 0.150 * BrillianceParameters.sharpenFactor*0.25, false, 3.000, 1.00, 1],
         [true, true, 0.000, false, 3.000, 1.00, 1]
      ];
      P.transform = MultiscaleLinearTransform.prototype.MultiscaleLinearTransform;
      P.scaleDelta = 0;
      P.scalingFunctionData = [
         0.25,0.5,0.25,
         0.5,1,0.5,
         0.25,0.5,0.25
      ];
      P.scalingFunctionRowFilter = [
         0.5,
         1,
         0.5
      ];
      P.scalingFunctionColFilter = [
         0.5,
         1,
         0.5
      ];
      P.scalingFunctionNoiseSigma = [
         0.8003,0.2729,0.1198,
         0.0578,0.0287,0.0143,
         0.0072,0.0036,0.0019,
         0.001
      ];
      P.scalingFunctionName = "Linear Interpolation (3)";
      P.linearMask = true;
      P.linearMaskAmpFactor = 100;
      P.linearMaskSmoothness = 1.00;
      P.linearMaskInverted = true;
      P.linearMaskPreview = false;
      P.largeScaleFunction = MultiscaleLinearTransform.prototype.NoFunction;
      P.curveBreakPoint = 0.75;
      P.noiseThresholding = false;
      P.noiseThresholdingAmount = 1.00;
      P.noiseThreshold = 3.00;
      P.softThresholding = true;
      P.useMultiresolutionSupport = false;
      P.deringing = false;
      P.deringingDark = 0.1000;
      P.deringingBright = 0.0000;
      P.outputDeringingMaps = false;
      P.lowRange = 0.0000;
      P.highRange = 0.0000;
      P.previewMode = MultiscaleLinearTransform.prototype.Disabled;
      P.previewLayer = 0;
      P.toLuminance = true;
      P.toChrominance = true;
      P.linear = false;

      P.executeOn(view, swapfile);
   }
}

function addLuminance(lum_id, view, amount, colorEnhance, swapfile=true) {
   var P = new LRGBCombination;
   P.channels = [ // enabled, id, k
      [false, "", 1.00000],
      [false, "", 1.00000],
      [false, "", 1.00000],
      [true, lum_id, 0.5000]
   ];
   P.mL = 0.500 + (amount - 0.5)/10.0;
   P.mc = 0.500 - colorEnhance/10.0;  // 0.5 default: smaller for more color, higher for less color: 0.35 ... 0.5
   P.clipHighlights = true;
   P.noiseReduction = false;
   P.layersRemoved = 4;
   P.layersProtected = 2;

   P.executeOn(view, swapfile);
}

function addLuminanceOnImage(lum_id, view, amount, colorEnhance, swapfile=true) {
   var P = new LRGBCombination;
   P.channels = [ // enabled, id, k
      [false, "", 1.00000],
      [false, "", 1.00000],
      [false, "", 1.00000],
      [true, lum_id, 0.5000]
   ];
   P.mL = 0.500 + (amount - 0.5)/10.0;
   P.mc = 0.500 - colorEnhance/10.0;  // 0.5 default: smaller for more color, higher for less color: 0.35 ... 0.5
   P.clipHighlights = true;
   P.noiseReduction = false;
   P.layersRemoved = 4;
   P.layersProtected = 2;
   P.inheritAstrometricSolution = true;

   P.executeOn(view, swapfile);
}

function colorBoost(view, amount, swapfile=true) {
   let value = amount - 0.5;
   if (Math.abs(value) > 0.0) {
      var P = new CurvesTransformation;

      P.c = [ // x, y
         [0.00000, 0.00000],
         [0.5, 0.5 + 0.125 * value],
         [1.00000, 1.00000]
      ];
      P.ct = CurvesTransformation.prototype.AkimaSubsplines;

      P.executeOn(view, swapfile);
   }
}


function processLuminance(view, swapfile=true) {
   luminanceCurve(view, BrillianceParameters.luminance, swapfile);
   contrastCurve(view, BrillianceParameters.contrastDelta, swapfile);
   improveLuminance(view);
   clahe(view, swapfile);
   sharpening(view, swapfile);

   if (BrillianceParameters.increaseEdges) {
      highlightEdges(view, swapfile);
   }
}

function process(view) {

   if (view && view.id) {

      try {
         var viewId = view.id;
         if (viewId.length == 0) {
            Console.warning("No image selected for processing!");
            return;
         }

         Console.writeln("Procssing " + viewId);
      }
      catch(error) {
         return;
      }

      Console.show();

      var v = createLuminance(view);
      processLuminance(v);

      addLuminance(v.id, view, BrillianceParameters.amount, BrillianceParameters.colorEnhance);

      view.window.mask = v.window;
      colorBoost(v, BrillianceParameters.colorEnhance);
      view.window.removeMask();

      v.window.forceClose();


   }
}

function scaleImage(image) {
   var maxSize = Math.max(image.width, image.height);

   if (BrillianceParameters.previewMode == 0) {
      var scale = Math.min(1400.0/maxSize, 1.0/3.0);
      image.resample(scale);
   }
   else if (BrillianceParameters.previewMode == 1) {
      var scale = Math.min(3600.0/maxSize, 1.0/2.0);
      image.resample(scale);
   }

   return image;
}


function processPreview() {
   if (BrillianceParameters.rgbView && BrillianceParameters.rgbView.id) {

      var img = BrillianceParameters.rgbView.image;
      var previewImage = new Image(BrillianceParameters.rgbView.image);
      previewImage = scaleImage(previewImage);

      // create a closed image for processing in the background
      var wtmp = new ImageWindow( previewImage.width, previewImage.height, img.numberOfChannels, img.bitsPerSample,
                                 img.sampleType == SampleType_Real, img.isColor, "tmp_preview" );

      var v = wtmp.mainView;
      v.beginProcess( UndoFlag_NoSwapFile );
      v.image.assign( previewImage );
      v.endProcess();

      let width = previewImage.width;
      let height = previewImage.height;

      var lv = createLuminance(v, false);

      try {


         processLuminance(lv, false);

         addLuminance(lv.id, wtmp.mainView, BrillianceParameters.amount, BrillianceParameters.colorEnhance, false);

         v.window.mask = lv.window;
         colorBoost(v, BrillianceParameters.colorEnhance, false);

         if (BrillianceParameters.previewImage) {
            BrillianceParameters.previewImage.free();
         }
         BrillianceParameters.previewImage = new Image(v.image);

      }
      catch(error) {
         Console.critical(error);
      }
      finally {
         lv.window.forceClose();
         previewImage.free();
         wtmp.forceClose();
      }
   }
}

function createPreviewImage(view) {
   let previewImage = new Image(view.image);
   return previewImage;
}

function cleanup() {
   if (BrillianceParameters.previewImage) {
      BrillianceParameters.previewImage.free();
      BrillianceParameters.previewImage = undefined;
   }
}


function BrillianceDialog() {
   this.__base__ = Dialog
   this.__base__();

   this.userResizable = true;
   this.scaledMinWidth = 1280;
   this.scaledMinHeight = 800;

   let labelMinWidth = Math.round(this.font.width("Highlights:M"));

   this.helpLabel = new Label( this );

   this.helpLabel.frameStyle = FrameStyle_Box;
   this.helpLabel.margin = 4;
   this.helpLabel.scaledMinWidth = LEFTWIDTH;
   this.helpLabel.scaledMaxWidth = LEFTWIDTH;
   this.helpLabel.wordWrapping = true;
   this.helpLabel.useRichText = true;
   this.helpLabel.text = "<b>" + TITLE + " v"
                        + VERSION + "</b> &mdash;"
                        + TEXT +"<p>Protect stars with a mask if you don't want to enhance them or just use a starless image. Using a starless image is the preferred option!</p>";


   // Select the image to be procressed
   this.targetViewSelectorFrame = new Frame(this);
   this.targetViewSelectorFrame.scaledMinWidth = LEFTWIDTH;
   this.targetViewSelectorFrame.scaledMaxWidth = LEFTWIDTH;
   this.targetViewSelectorFrame.backgroundColor = 0xFFD8D7D3;
   this.targetViewSelectLabel = new Label(this);
   this.targetViewSelectLabel.text = "Target View:";
   this.targetViewSelectLabel.toolTip = "<p>Select the image to be improved.</p>";

   this.targetViewSelector = new ViewList(this);
   this.targetViewSelector.scaledMaxWidth = 350;
   this.targetViewSelector.getMainViews();

   with(this.targetViewSelectorFrame) {
      sizer = new HorizontalSizer();
      sizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;

      with(sizer) {
         margin = 6;
         add(this.targetViewSelectLabel);
         addSpacing(8);
         add(this.targetViewSelector);
         adjustToContents();
      }
   }

   this.targetViewSelector.onViewSelected = function(view) {

      this.dialog.previewTimer.stop();
      BrillianceParameters.rgbView = view;


      this.dialog.previewTimer.previewKey = ".";
      this.dialog.previewTimer.start();
      if (BrillianceParameters.rgbView && BrillianceParameters.rgbView.id) {
         var previewImage = createPreviewImage(BrillianceParameters.rgbView);
         previewImage = scaleImage(previewImage);
         var metadata = {
            width: previewImage.width,
            height: previewImage.height
         }

         this.dialog.previewControl.SetPreview(previewImage, BrillianceParameters.rgbView, metadata);
         BrillianceParameters.previewImage = previewImage;

      }
      this.dialog.previewTimer.start();
   };

   this.previewModeFrame = new Frame(this);
   this.previewModeFrame.sizer = new HorizontalSizer;
   this.previewModeFrame.sizer.textAlignment = TextAlign_Left | TextAlign_VertCenter;
   this.previewModeFrame.sizer.margin = 6;
   this.previewModeFrame.scaledMinWidth = LEFTWIDTH;
   this.previewModeFrame.scaledMaxWidth = LEFTWIDTH;
   this.previewModeFrame.backgroundColor = 0xFFD8D7D3;
   this.previewModeFrameLabel = new Label(this);
   this.previewModeFrameLabel.text = "Preview Mode:";
   this.previewModeFrameLabel.toolTip = "<p>Select the mode for the preview. A small preview will be processed fast while a large preview will show more details but takes more time to be processed.</p><p><b>Hint: Increase the resolution of the preview while sharpening!</b></p>";

   this.previewModeSelector = new ComboBox(this);
   this.previewModeSelector.toolTip = "<p>Select the mode for the preview. A small preview will be processed fast while a large preview will show more details but takes more time to be processed.</p><p><b>Hint: Increase the resolution of the preview while sharpening!</b></p>";

   this.previewModeSelector.addItem("Small-sized Preview (fast)");
   this.previewModeSelector.addItem("Medium-sized Preview");
   this.previewModeSelector.addItem("Large-sized Preview (slow)");
   this.previewModeSelector.currentItem = BrillianceParameters.previewMode;

   this.previewModeSelector.onItemSelected = function(index) {
      this.dialog.previewTimer.stop();
      BrillianceParameters.previewMode = index;

      var previewImage = scaleImage(BrillianceParameters.previewImage);

      var metadata = {
         width: previewImage.width,
         height: previewImage.height
      }

      this.dialog.previewControl.SetPreview(previewImage, BrillianceParameters.rgbView, metadata);
      this.dialog.previewControl.forceRedraw();

      this.dialog.previewTimer.start();
   };

   this.previewModeFrame.sizer.add(this.previewModeFrameLabel);
   this.previewModeFrame.sizer.addStretch();
   this.previewModeFrame.sizer.add(this.previewModeSelector);

   this.autoPreviewFrame = new Frame;
   this.autoPreviewFrame.scaledMinWidth = LEFTWIDTH;
   this.autoPreviewFrame.scaledMaxWidth = LEFTWIDTH;
   this.autoPreviewFrame.backgroundColor = 0xFFD8D7D3;
   this.autoPreviewFrame.sizer = new HorizontalSizer;
   this.autoPreviewFrame.sizer.scaledMinWidth = LEFTWIDTH;
   this.autoPreviewFrame.sizer.scaledMaxWidth = LEFTWIDTH;
   this.autoPreviewFrame.sizer.margin = 6;
   this.autoPreviewFrame.sizer.spacing = 6;
   this.autoPreviewFrame.sizer.addStretch();

   this.autoPreviewCheckbox = new CheckBox(this);
   this.autoPreviewCheckbox.text = "Automatically update the preview";
   this.autoPreviewCheckbox.checked = BrillianceParameters.autoPreview;

   this.autoPreviewCheckbox.onCheck = function(checked) {
      BrillianceParameters.autoPreview = checked;
   }

   this.autoPreviewFrame.sizer.add(this.autoPreviewCheckbox);


   this.parametersBar = new SectionBar(this, "Parameters");
   this.parametersBar.scaledMinWidth = LEFTWIDTH;
   this.parametersBar.scaledMaxWidth = LEFTWIDTH;
   this.parametersControl = new Control(this);
   this.parametersControl.scaledMinWidth = LEFTWIDTH;
   this.parametersControl.scaledMaxWidth = LEFTWIDTH;

   this.parametersBar.onToggleSection =  function( section, toggleBegin ) {
      if ( !toggleBegin ) {
         section.dialog.adjustToContents();
         section.dialog.setVariableSize();
      }
   }

   this.parametersBar.setSection( this.parametersControl );


   this.contrastToolFrame = new Frame(this);
   this.contrastToolFrame.scaledMinWidth = LEFTWIDTH;
   this.contrastToolFrame.scaledMaxWidth = LEFTWIDTH;

   this.contrastSlider = new NumericControl(this);
   this.contrastSlider.setRange(0.0, 1.0);
   this.contrastSlider.slider.setRange(0.0, 1000.0);
   this.contrastSlider.setPrecision(3);
   this.contrastSlider.setReal(true);
   this.contrastSlider.enableFixedPrecision(true);
   this.contrastSlider.setValue(BrillianceParameters.contrastDelta);
   this.contrastSlider.label.text = "Contrast: ";
   this.contrastSlider.toolTip = "<p>Increase or decrease the contrast of the luminance. Increasing the contrast will reduce the amount of sharpening for darker image regions, but consider to decrease the amount in this case.</p>";
   this.contrastSlider.label.minWidth = labelMinWidth;

   this.resetContrastButton = new ToolButton( this );
   this.resetContrastButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetContrastButton.setScaledFixedSize( 24, 24 );
   this.resetContrastButton.toolTip = "<p>Reset the contrast to its default.</p>";
   this.resetContrastButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.contrastSlider.setValue(0.0);
      BrillianceParameters.contrastDelta = 0.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   with(this.contrastToolFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         add(this.contrastSlider);
         add(this.resetContrastButton);
         adjustToContents();
      }
   }

   this.contrastSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      BrillianceParameters.contrastDelta = t;
      this.dialog.previewTimer.start();
   };


   this.brillianceToolFrame = new Frame(this);
   this.brillianceToolFrame.scaledMinWidth = LEFTWIDTH;
   this.brillianceToolFrame.scaledMaxWidth = LEFTWIDTH;

   this.brillianceSlider = new NumericControl(this);
   this.brillianceSlider.label.text = "Brilliance: ";
   this.brillianceSlider.toolTip = "<p>Increase or decrease the amount of the luminance when it will be combined with your image. This controls the amount of the brilliance to be added. If you increased the contrast you should try to decrease the amount to avoid to dim the darker regions the image.</p>";
   this.brillianceSlider.setRange(0.0, 1.0);
   this.brillianceSlider.slider.setRange(0.0, 1000.0);
   this.brillianceSlider.setPrecision(3);
   this.brillianceSlider.setReal(true);
   this.brillianceSlider.setValue(BrillianceParameters.amount);
   this.brillianceSlider.label.minWidth = labelMinWidth;

   this.resetBrilliancButton = new ToolButton( this );
   this.resetBrilliancButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetBrilliancButton.setScaledFixedSize( 24, 24 );
   this.resetBrilliancButton.toolTip = "<p>Reset the brilliance to its default.</p>";
   this.resetBrilliancButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.brillianceSlider.setValue(0.5);
      BrillianceParameters.amount = 0.5;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   with(this.brillianceToolFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         add(this.brillianceSlider);
         add(this.resetBrilliancButton);
         adjustToContents();
      }
   }

   this.brillianceSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      BrillianceParameters.amount = t;
      this.dialog.previewTimer.start();
   };

   this.colorToolFrame = new Frame(this);
   this.colorToolFrame.scaledMinWidth = LEFTWIDTH;
   this.colorToolFrame.scaledMaxWidth = LEFTWIDTH;

   this.colorSlider = new NumericControl(this);
   this.colorSlider.label.text = "Color: ";

   this.colorSlider.toolTip = "<p>Increase or decrease the color brilliance.</p>";
   this.colorSlider.setRange(0.0, 1.0);
   this.colorSlider.slider.setRange(0.0, 1000.0);
   this.colorSlider.setPrecision(3);
   this.colorSlider.setReal(true);
   this.colorSlider.setValue(BrillianceParameters.colorEnhance);
   this.colorSlider.label.minWidth = labelMinWidth;

   this.resetColorButton = new ToolButton( this );
   this.resetColorButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetColorButton.setScaledFixedSize( 24, 24 );
   this.resetColorButton.toolTip = "<p>Reset the color to its default.</p>";
   this.resetColorButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.colorSlider.setValue(0.5);
      BrillianceParameters.colorEnhance = 0.5;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   with(this.colorToolFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         add(this.colorSlider);
         add(this.resetColorButton);
         adjustToContents();
      }
   }

   this.colorSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      BrillianceParameters.colorEnhance = t;
      this.dialog.previewTimer.start();
   };


   this.luminanceToolFrame = new Frame(this);
   this.luminanceToolFrame.scaledMinWidth = LEFTWIDTH;
   this.luminanceToolFrame.scaledMaxWidth = LEFTWIDTH;

   this.luminanceSlider = new NumericControl(this);
   this.luminanceSlider.label.text = "Luminance: ";

   this.luminanceSlider.toolTip = "<p>Increase/Decrease the Luminance.</p>";
   this.luminanceSlider.setRange(-1.0, 1.0);
   this.luminanceSlider.slider.setRange(-1000.0, 1000.0);
   this.luminanceSlider.setPrecision(3);
   this.luminanceSlider.setReal(true);
   this.luminanceSlider.setValue(BrillianceParameters.luminance);
   this.luminanceSlider.label.minWidth = labelMinWidth;

   this.luminanceSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      BrillianceParameters.luminance = t;
      this.dialog.previewTimer.start();
   };

   this.resetLuminanceButton = new ToolButton( this );
   this.resetLuminanceButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetLuminanceButton.setScaledFixedSize( 24, 24 );
   this.resetLuminanceButton.toolTip = "<p>Reset the luminance to its default.</p>";
   this.resetLuminanceButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.luminanceSlider.setValue(0.0);
      BrillianceParameters.luminance = 0.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   with(this.luminanceToolFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         add(this.luminanceSlider);
         add(this.resetLuminanceButton);
         adjustToContents();
      }
   }

   this.sharpenFrame = new Frame(this);
   this.sharpenFrame.scaledMinWidth = LEFTWIDTH;
   this.sharpenFrame.scaledMaxWidth = LEFTWIDTH;

   this.sharpenSlider = new NumericControl(this);
   this.sharpenSlider.label.text = "Sharpening:";

   this.sharpenSlider.toolTip = "<p>Increase or decrease the sharpening.</p><p><b>Hint: Increase the resolution of the preview while sharpening!</b></p>";
   this.sharpenSlider.setRange(0.0, 2.0);
   this.sharpenSlider.slider.setRange(0.0, 200.0);
   this.sharpenSlider.setPrecision(2);
   this.sharpenSlider.setReal(true);
   this.sharpenSlider.setValue(BrillianceParameters.sharpenFactor);
   this.sharpenSlider.label.minWidth = labelMinWidth;

   this.resetLuminanceButton = new ToolButton( this );
   this.resetLuminanceButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetLuminanceButton.setScaledFixedSize( 24, 24 );
   this.resetLuminanceButton.toolTip = "<p>Reset the sharpening to its default.</p>";
   this.resetLuminanceButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.sharpenSlider.setValue(0.0);
      BrillianceParameters.sharpenFactor = 0.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   with(this.sharpenFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         add(this.sharpenSlider);
         add(this.resetLuminanceButton);
         adjustToContents();
      }
   }

   this.sharpenSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      BrillianceParameters.sharpenFactor = t;
      this.dialog.previewTimer.start();
   };

   this.edgesFrame = new Frame;
   this.edgesFrame.scaledMinWidth = LEFTWIDTH;
   this.edgesFrame.scaledMaxWidth = LEFTWIDTH;
   this.edgesFrame.sizer = new HorizontalSizer;
   this.edgesFrame.sizer.scaledMinWidth = LEFTWIDTH;
   this.edgesFrame.sizer.scaledMaxWidth = LEFTWIDTH;
   this.edgesFrame.sizer.margin = 6;
   this.edgesFrame.sizer.spacing = 6;
   this.edgesFrame.sizer.addStretch();

   this.increaseEdgesCheckbox = new CheckBox(this);
   this.increaseEdgesCheckbox.text = "Improve contrast edges";
   this.increaseEdgesCheckbox.checked = BrillianceParameters.increaseEdges;

   this.increaseEdgesCheckbox.onCheck = function(checked) {
      BrillianceParameters.increaseEdges = checked;
   }

   this.edgesFrame.sizer.add(this.increaseEdgesCheckbox);

   with(this.parametersControl) {
      sizer = new VerticalSizer();
      sizer.textAlignment = TextAlign_Left|TextAlign_VertCenter;
      backgroundColor = 0xFFD8D7D3;
      with(sizer) {
         addSpacing(8);
         add(this.contrastToolFrame);
         addSpacing(4);
         add(this.luminanceToolFrame);
         addSpacing(4);
         add(this.colorToolFrame);
         addSpacing(4);
         add(this.brillianceToolFrame);
         addSpacing(4);
         add(this.sharpenFrame);
         addSpacing(8);
         add(this.edgesFrame);

         adjustToContents();
      }
  }

   this.previewControl = new PreviewControl(this);
   this.previewControl.scaledMinWidth = 800;
   this.previewControl.minheight = 600;

   this.buttonsFrame = new Frame;
   this.buttonsFrame.scaledMinWidth = LEFTWIDTH;
   this.buttonsFrame.scaledMaxWidth = LEFTWIDTH;
   this.buttonsFrame.sizer = new HorizontalSizer;
   this.buttonsFrame.sizer.margin = 6;
   this.buttonsFrame.sizer.spacing = 6;
   this.buttonsFrame.sizer.scaledMinWidth = LEFTWIDTH;
   this.buttonsFrame.sizer.scaledMaxWidth = LEFTWIDTH;
   this.buttonsFrame.sizer.addSpacing(8);

   this.newInstanceButton = new ToolButton( this );
   this.newInstanceButton.icon = this.scaledResource( ":/process-interface/new-instance.png" );
   this.newInstanceButton.setScaledFixedSize( 24, 24 );
   this.newInstanceButton.onMousePress = () => {
      BrillianceParameters.save();
      this.newInstance();
   }

   this.ok_Button = new ToolButton( this );
   this.ok_Button.icon = this.scaledResource( ":/process-interface/execute.png" );
   this.ok_Button.setScaledFixedSize( 24, 24 );
   this.ok_Button.toolTip = "<p>Change the target view image using the specified parameters.</p>";
   this.ok_Button.onClick = () => {
      this.dialog.previewTimer.stop();
      if (this.dialog.busy) return;


      if (this.targetViewSelector.currentView !== null && this.targetViewSelector.currentView !== undefined) {

         BrillianceParameters.contrastDelta = this.contrastSlider.value;
         BrillianceParameters.colorEnhance = this.colorSlider.value;
         BrillianceParameters.amount = this.brillianceSlider.value;
         BrillianceParameters.increaseEdges = this.increaseEdgesCheckbox.checked;

         Console.writeln("Saving script parameters...");
         BrillianceParameters.save();

         this.ok();
         process(this.targetViewSelector.currentView);
      }

      cleanup();
   };

   this.cancel_Button = new ToolButton( this );
   this.cancel_Button.icon = this.scaledResource( ":/process-interface/cancel.png" );
   this.cancel_Button.setScaledFixedSize( 24, 24 );
   this.cancel_Button.toolTip = "<p>Close this dialog with no changes.</p>";
   this.cancel_Button.onClick = () => {

      this.dialog.previewTimer.stop();
      if (this.dialog.busy) return;
      this.cancel();
      cleanup();
   };

   this.updateRefresh_Button = new ToolButton( this );
   this.updateRefresh_Button.icon = this.scaledResource( ":/icons/refresh.png" );
   this.updateRefresh_Button.setScaledFixedSize( 24, 24 );
   this.updateRefresh_Button.toolTip = "<p>Update the preview.</p>";
   this.updateRefresh_Button.onClick = () => {

     BrillianceParameters.manualUpdate = true;

      this.dialog.previewTimer.start();
      if (this.dialog.busy) return;

   };

   this.help_Button = new ToolButton( this );
   this.help_Button.icon = this.scaledResource(":/process-interface/browse-documentation.png");
   this.help_Button.setScaledFixedSize( 24, 24 );
   this.help_Button.toolTip = "<p>Shows the script documentation.</p>";
   this.help_Button.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      Dialog.browseScriptDocumentation("ImproveBrilliance");
      if (timerWasRunning) this.dialog.previewTimer.start();
   };

   this.buttonsFrame.sizer.add( this.newInstanceButton );
   this.buttonsFrame.sizer.addSpacing(16);
   this.buttonsFrame.sizer.add( this.ok_Button );
   this.buttonsFrame.sizer.addSpacing(32);
   this.buttonsFrame.sizer.add( this.cancel_Button );
   this.buttonsFrame.sizer.addStretch();
   this.buttonsFrame.sizer.add( this.updateRefresh_Button );
   this.buttonsFrame.sizer.addSpacing(8);
   this.buttonsFrame.sizer.add( this.help_Button );
   this.buttonsFrame.sizer.addSpacing(8);

   this.previewTimer = new Timer();
   this.previewTimer.interval = 0.50;
   this.previewTimer.periodic = true;
   this.previewTimer.dialog = this;
   this.previewTimer.busy = false;
   this.previewTimer.previewKey = "";
   this.previewTimer.updateCount = 0;

   this.previewTimer.onTimeout = function()  {

      if (!BrillianceParameters.autoPreview) {
         if (!BrillianceParameters.manualUpdate)
            return;
         BrillianceParameters.manualUpdate = false;
      }

      let currentPreviewKey = getPreviewKey();
      let needsUpdate = (this.previewKey != currentPreviewKey);

      if (needsUpdate)
      {
         if (this.busy) return;

         this.dialog.targetViewSelector.enabled = false;
         this.dialog.ok_Button.enabled = false;
         this.dialog.cancel_Button.enabled = false;
         this.stop();
         this.busy = true;


         try {
            processPreview();

            if (BrillianceParameters.previewImage != undefined) {
               let previewImage = BrillianceParameters.previewImage;
               var metadata = {
                  width: previewImage.width,
                  height: previewImage.height
               }

               this.dialog.previewControl.SetPreview(previewImage, BrillianceParameters.rgbView, metadata);
               this.dialog.previewControl.forceRedraw();
            }
         }
         catch(error) {
            Console.writeln(error);
         }

         this.previewKey = currentPreviewKey;

         this.busy = false;
         this.dialog.targetViewSelector.enabled = true;
         this.dialog.ok_Button.enabled = true;
         this.dialog.cancel_Button.enabled = true;

         this.start();
      }

   }



   this.onHide = function() {
      this.dialog.previewTimer.stop();

      cleanup();

      if (BrillianceParameters.previewImage !== undefined) {
         BrillianceParameters.previewImage.free();
      }
   }

   if (BrillianceParameters.rgbView !== undefined) {
      this.targetViewSelector.currentView = BrillianceParameters.rgbView;
      if (BrillianceParameters.previewImage !== undefined) {
         BrillianceParameters.previewImage.free();
      }
      var previewImage = createPreviewImage(BrillianceParameters.rgbView);

      var metadata = {
         width: previewImage.width,
         height: previewImage.height
      }

      this.dialog.previewControl.SetPreview(previewImage, BrillianceParameters.rgbView, metadata);
      this.dialog.previewControl.zoomToFit();
      BrillianceParameters.previewImage = previewImage;

      this.previewTimer.start();
   }


   this.sizerLeft = new VerticalSizer;

   this.sizerLeft.add(this.helpLabel);
   this.sizerLeft.addSpacing(16);
   this.sizerLeft.add(this.targetViewSelectorFrame);
   this.sizerLeft.add(this.previewModeFrame);
   this.sizerLeft.add(this.autoPreviewFrame);
   this.sizerLeft.addSpacing(16);
   this.sizerLeft.add(this.parametersBar);
   this.sizerLeft.add(this.parametersControl);
   this.sizerLeft.addStretch();
   this.sizerLeft.add(this.buttonsFrame);


   // layout the dialog
   this.sizer = new HorizontalSizer;
   this.sizer.margin = 8;
   this.sizer.spacing = 6;
   this.sizer.add(this.sizerLeft);
   this.sizer.add(this.previewControl);


}

BrillianceDialog.prototype = new Dialog


function main() {


   // is script started from an instance in global or view target context?
   if (Parameters.isViewTarget) {
      BrillianceParameters.load();
      process(Parameters.targetView);

      return;
   } else if (Parameters.isGlobalTarget) {
      BrillianceParameters.load();
   }

   var window = ImageWindow.activeWindow;

   if (!window.isNull) {
      BrillianceParameters.rgbView = window.mainView;
   }

   let dialog = new BrillianceDialog();
   dialog.execute();

}

main();


