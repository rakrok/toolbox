#include <pjsr/StdIcon.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/TextAlign.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/ColorSpace.jsh>
#include <pjsr/Interpolation.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/SectionBar.jsh>
#include <pjsr/CheckState.jsh>
#include <pjsr/SampleType.jsh>
#include "PixInsightToolsPreviewControl.jsh"

#feature-id CombineRGBAndNarrowband :  Toolbox > CombineRGBAndNarrowband

#feature-info  A script to combine RGB and Narrowband data.<br/>\
   <br/>\
   A script to combine RGB and Narrowband data. \
   <br/> \
   Copyright &copy; 2024 Juergen Terpe


#define VERSION "1.4"
#define TITLE  "Combine RGB and Narrowband data"
#define TEXT   "Combine RGB and Narrowband image data."

#define LEFTWIDTH 400

#define Q_DEFAULT 1.0
#define C_DEFAULT 1.0
#define StarBalance_DEFAULT  0.2
#define Vibrance_DEFAULT 0.0


var combineRGBNBParameters = {
   imageRGB: undefined,
   imageNB_R: undefined,
   imageNB_G: undefined,
   imageNB_B: undefined,
   starMask: undefined,
   previewImage: undefined,
   C_R: C_DEFAULT,
   C_G: C_DEFAULT,
   C_B: C_DEFAULT,
   amount: 1.0,
   vibrance: Vibrance_DEFAULT,
   highlightsValue: 1.0,
   rescaleValue: 1.0,
   starBalance: StarBalance_DEFAULT,

   // stores the current parameters values into the script instance
   save: function() {
      if (combineRGBNBParameters.imageRGB)
         Parameters.set("imageRGB", combineRGBNBParameters.imageRGB.id);
      if (combineRGBNBParameters.imageNB_R)
         Parameters.set("imageNB_R", combineRGBNBParameters.imageNB_R.id);
      else
         Parameters.set("imageNB_R", undefined);

      if (combineRGBNBParameters.imageNB_G)
         Parameters.set("imageNB_G", combineRGBNBParameters.imageNB_G.id);
      else
         Parameters.set("imageNB_G", undefined);

      if (combineRGBNBParameters.imageNB_B)
         Parameters.set("imageNB_B", combineRGBNBParameters.imageNB_B.id);
      else
         Parameters.set("imageNB_B", undefined);
      if (combineRGBNBParameters.starMask)
         Parameters.set("starMask", combineRGBNBParameters.starMask.id);
      else
         Parameters.set("starMask", undefined);

      Parameters.set("C_R", combineRGBNBParameters.C_R);
      Parameters.set("C_G", combineRGBNBParameters.C_G);
      Parameters.set("C_B", combineRGBNBParameters.C_B);
      Parameters.set("amount", combineRGBNBParameters.amount);
      Parameters.set("vibrance", combineRGBNBParameters.vibrance);
      Parameters.set("highlightsValue", combineRGBNBParameters.highlightsValue);
      Parameters.set("rescaleValue", combineRGBNBParameters.rescaleValue);
      Parameters.set("starBalance", combineRGBNBParameters.starBalance);
   },

   // loads the script instance parameters
   load: function() {

      if (Parameters.has("imageRGB"))
         combineRGBNBParameters.imageRGB = View.viewById(Parameters.get("imageRGB"));
      if (Parameters.has("imageNB_R"))
         combineRGBNBParameters.imageNB_R = View.viewById(Parameters.get("imageNB_R"));
      if (Parameters.has("imageNB_G"))
         combineRGBNBParameters.imageNB_G = View.viewById(Parameters.get("imageNB_G"));
      if (Parameters.has("imageNB_B"))
         combineRGBNBParameters.imageNB_B = View.viewById(Parameters.get("imageNB_B"));
      if (Parameters.has("starMask"))
         combineRGBNBParameters.starMask = View.viewById(Parameters.get("starMask"));

      if (Parameters.has("C_R"))
         combineRGBNBParameters.C_R = Parameters.getBoolean("C_R");
      if (Parameters.has("C_G"))
         combineRGBNBParameters.C_G = Parameters.getBoolean("C_G");
      if (Parameters.has("C_B"))
         combineRGBNBParameters.C_B = Parameters.getBoolean("C_B");
      if (Parameters.has("amount"))
         combineRGBNBParameters.amount = Parameters.getBoolean("amount");
      if (Parameters.has("vibrance"))
         combineRGBNBParameters.vibrance = Parameters.getBoolean("vibrance");
      if (Parameters.has("highlightsValue"))
         combineRGBNBParameters.highlightsValue = Parameters.getBoolean("highlightsValue");
      if (Parameters.has("rescaleValue"))
         combineRGBNBParameters.rescaleValue = Parameters.getBoolean("rescaleValue");
      if (Parameters.has("starBalance"))
         combineRGBNBParameters.starBalance = Parameters.getBoolean("starBalance");
   }
}

function closeView(view) {
   if (view && view.window) {
      try {
         Console.writeln("closing view: "+view.id);
         view.window.forceClose();
      }
      catch(error) {
         Console.writeln("could not close view..." + error);
      }
   }
}

function closeAllTempViews() {
   if (combineRGBNBParameters.previewImage != undefined) {
      closeView(combineRGBNBParameters.previewImage);
      combineRGBNBParameters.previewImage = undefined;
   }
}

function cloneHidden(view, postfix, swapfile=true) {
   var P = new PixelMath;
   P.expression = "$T";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.truncate = true;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + postfix;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.RGB;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view, swapfile);

   return View.viewById(P.newImageId);
}

function clone(view, postfix, swapfile=true) {
   var P = new PixelMath;
   P.expression = "$T";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.truncate = true;
   P.createNewImage = true;
   P.showNewImage = true;
   P.newImageId = view.id + postfix;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.RGB;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view, swapfile);

   return View.viewById(P.newImageId);
}

function extractHa(view) {
   var P = new PixelMath;
   P.expression = "$T[0]";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = true;
   P.newImageId = view.id+"_Ha";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view, false);

   return View.viewById(view.id+"_Ha");
}

function extractOIII(view) {
   var P = new PixelMath;
   P.expression = "0.7*$T[1]+0.3*$T[2]";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = true;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = true;
   P.newImageId = view.id+"_OIII";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view, false);

   return View.viewById(view.id+"_OIII");
}

function extractOIIIG(view) {
   var P = new PixelMath;
   P.expression = "0.5*$T[1]";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = true;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = true;
   P.newImageId = view.id+"_G";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view, false);

   return View.viewById(view.id+"_G");
}


function buildRed() {
   if (combineRGBNBParameters.imageNB_R != undefined) {
      var P = new PixelMath;
      P.expression = "Q*("+combineRGBNBParameters.imageNB_R.id+" - C*med("+ combineRGBNBParameters.imageNB_R.id +"))";
      P.useSingleExpression = true;
      P.symbols = "Q = "+combineRGBNBParameters.amount+", C = "+combineRGBNBParameters.C_R;
      P.clearImageCacheAndExit = false;
      P.cacheGeneratedImages = false;
      P.generateOutput = true;
      P.singleThreaded = false;
      P.optimization = true;
      P.use64BitWorkingImage = false;
      P.rescale = false;
      P.rescaleLower = 0;
      P.rescaleUpper = 1;
      P.truncate = true;
      P.truncateLower = 0;
      P.truncateUpper = 1;
      P.createNewImage = true;
      P.showNewImage = false;
      P.newImageId = combineRGBNBParameters.imageNB_R.id+"_Red_temp";
      P.newImageWidth = 0;
      P.newImageHeight = 0;
      P.newImageAlpha = false;
      P.newImageColorSpace = PixelMath.prototype.Gray;
      P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
      P.executeOn(combineRGBNBParameters.imageNB_R, false);

      return View.viewById(combineRGBNBParameters.imageNB_R.id+"_Red_temp");
   }

   if (combineRGBNBParameters.imageNB_G != undefined)
      return buildBlack(combineRGBNBParameters.imageNB_G, "_Red_temp");
   if (combineRGBNBParameters.imageNB_B != undefined)
      return buildBlack(combineRGBNBParameters.imageNB_B, "_Red_temp");
   return null;
}

function buildGreen() {
   if (combineRGBNBParameters.imageNB_G != undefined) {
      var P = new PixelMath;
      P.expression = "Q*("+combineRGBNBParameters.imageNB_G.id+" - C*med("+ combineRGBNBParameters.imageNB_G.id +"))";
      P.useSingleExpression = true;
      P.symbols = "Q = "+combineRGBNBParameters.amount+", C = "+combineRGBNBParameters.C_G;
      P.clearImageCacheAndExit = false;
      P.cacheGeneratedImages = false;
      P.generateOutput = true;
      P.singleThreaded = false;
      P.optimization = true;
      P.use64BitWorkingImage = false;
      P.rescale = false;
      P.rescaleLower = 0;
      P.rescaleUpper = 1;
      P.truncate = true;
      P.truncateLower = 0;
      P.truncateUpper = 1;
      P.createNewImage = true;
      P.showNewImage = false;
      P.newImageId = combineRGBNBParameters.imageNB_G.id+"_Green_temp";
      P.newImageWidth = 0;
      P.newImageHeight = 0;
      P.newImageAlpha = false;
      P.newImageColorSpace = PixelMath.prototype.Gray;
      P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
      P.executeOn(combineRGBNBParameters.imageNB_G, false);

      return View.viewById(combineRGBNBParameters.imageNB_G.id+"_Green_temp");
   }

   if (combineRGBNBParameters.imageNB_R != undefined)
      return buildBlack(combineRGBNBParameters.imageNB_R, "_Green_temp");
   if (combineRGBNBParameters.imageNB_B != undefined)
      return buildBlack(combineRGBNBParameters.imageNB_B, "_Green_temp");
   return null;
}

function buildBlue() {
   if (combineRGBNBParameters.imageNB_B != undefined) {
      var P = new PixelMath;
      P.expression = "Q*("+combineRGBNBParameters.imageNB_B.id+" - C*med("+ combineRGBNBParameters.imageNB_B.id +"))";
      P.useSingleExpression = true;
      P.symbols = "Q = "+combineRGBNBParameters.amount+", C = "+combineRGBNBParameters.C_B;
      P.clearImageCacheAndExit = false;
      P.cacheGeneratedImages = false;
      P.generateOutput = true;
      P.singleThreaded = false;
      P.optimization = true;
      P.use64BitWorkingImage = false;
      P.rescale = false;
      P.rescaleLower = 0;
      P.rescaleUpper = 1;
      P.truncate = true;
      P.truncateLower = 0;
      P.truncateUpper = 1;
      P.createNewImage = true;
      P.showNewImage = false;
      P.newImageId = combineRGBNBParameters.imageNB_B.id+"_Blue_temp";
      P.newImageWidth = 0;
      P.newImageHeight = 0;
      P.newImageAlpha = false;
      P.newImageColorSpace = PixelMath.prototype.Gray;
      P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
      P.executeOn(combineRGBNBParameters.imageNB_B, false);

      return View.viewById(combineRGBNBParameters.imageNB_B.id+"_Blue_temp");
   }

   if (combineRGBNBParameters.imageNB_R != undefined)
      return buildBlack(combineRGBNBParameters.imageNB_R, "_Blue_temp");
   if (combineRGBNBParameters.imageNB_G != undefined)
      return buildBlack(combineRGBNBParameters.imageNB_G, "_Blue_temp");

   return null;
}

function buildBlack(view, postfix) {
   var P = new PixelMath;
   P.expression = "0";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id+postfix;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view, false);

   return View.viewById(view.id+postfix);
}


function subtractStarNask(view, mask, starBalance) {
   var P = new PixelMath;
   P.expression = view.id + " - " + starBalance +" * " + mask.id;
   P.useSingleExpression = false;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.truncate = true;
   P.createNewImage = false;

   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view, false);
}

function combineRGBNB(viewRGB, R, G, B, rescaleValue, swapfile) {
   let rescale = rescaleValue < 0.995;
   Console.writeln("rescale="+rescaleValue);
   var P = new PixelMath;
   P.expression = "~(~$T[0] * ~" + R.id+")";
   P.expression1 = "~(~$T[1] * ~" + G.id+")";
   P.expression2 = "~(~$T[2] * ~" + B.id+")";
   P.expression3 = "";
   P.useSingleExpression = false;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;

   P.rescale = rescale;
   P.rescaleLower = 0.0;
   P.rescaleUpper = rescaleValue;
   P.truncate = !rescale;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;

   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(viewRGB, swapfile);
}


function improveVibranceAndLuminance(viewRGB, vibrance, luminance, swapfile) {
   var P = new CurvesTransformation;

   P.c = [ // x, y
      [0.00000, 0.00000],
      [0.50000, 0.50000 + 0.25*vibrance],
      [1.00000, 1.00000]
   ];
   P.ct = CurvesTransformation.prototype.AkimaSubsplines;

   P.L = [ // x, y
      [0.00000, 0.00000],
      [0.75000, 0.25000 + 0.5*luminance],
      [1.00000, 1.00000]
   ];
   P.lt = CurvesTransformation.prototype.AkimaSubsplines;

   P.executeOn(viewRGB, swapfile);
}


function process() {

   if (!combineRGBNBParameters.imageRGB)
      return;

   var imageRGB = combineRGBNBParameters.imageRGB;
   var red = buildRed();
   var green = buildGreen();
   var blue = buildBlue();

   if (red && green && blue) {

      if (combineRGBNBParameters.starMask) {
         subtractStarNask(red, combineRGBNBParameters.starMask, combineRGBNBParameters.starBalance);
         subtractStarNask(green, combineRGBNBParameters.starMask, combineRGBNBParameters.starBalance);
         subtractStarNask(blue, combineRGBNBParameters.starMask, combineRGBNBParameters.starBalance);
      }

      combineRGBNB(imageRGB, red, green, blue, combineRGBNBParameters.rescaleValue, true);
      improveVibranceAndLuminance(imageRGB, combineRGBNBParameters.vibrance, combineRGBNBParameters.highlightsValue, true);
   }
   closeView(red);
   closeView(green);
   closeView(blue);


}

function processPreview() {

   if (!combineRGBNBParameters.imageRGB)
      return;

   if (!combineRGBNBParameters.previewImage) {
      combineRGBNBParameters.previewImage = cloneHidden(combineRGBNBParameters.imageRGB, "_preview", false);
   }

   var clonedRGB = cloneHidden(combineRGBNBParameters.imageRGB, "_preview_temp", false);
   var red = buildRed();
   var green = buildGreen();
   var blue = buildBlue();

   if (red && green && blue) {

      if (combineRGBNBParameters.starMask) {
         subtractStarNask(red, combineRGBNBParameters.starMask, combineRGBNBParameters.starBalance);
         subtractStarNask(green, combineRGBNBParameters.starMask, combineRGBNBParameters.starBalance);
         subtractStarNask(blue, combineRGBNBParameters.starMask, combineRGBNBParameters.starBalance);
      }

      combineRGBNB(clonedRGB, red, green, blue, combineRGBNBParameters.rescaleValue, false);
      improveVibranceAndLuminance(clonedRGB, combineRGBNBParameters.vibrance, combineRGBNBParameters.highlightsValue, false);

      combineRGBNBParameters.previewImage.beginProcess( UndoFlag_NoSwapFile );
      combineRGBNBParameters.previewImage.image.assign( clonedRGB.image );
      combineRGBNBParameters.previewImage.endProcess();
   }
   closeView(clonedRGB);

   closeView(red);
   closeView(green);
   closeView(blue);
}

function CombineRGBAndNBDialog() {
   this.__base__ = Dialog
   this.__base__();
   this.scaledMinWidth = 1280;
   this.scaledMinHeight = 800;

   let labelMinWidth = Math.round(this.font.width("Highlights:M"));
   let sliderMinWidth = 250;

   this.helpLabel = new Label( this );
   this.helpLabel.scaledMinWidth = LEFTWIDTH;
   this.helpLabel.scaledMaxWidth = LEFTWIDTH;
   this.helpLabel.frameStyle = FrameStyle_Box;
   this.helpLabel.margin = 4;
   this.helpLabel.wordWrapping = true;
   this.helpLabel.useRichText = true;
   this.helpLabel.text = "<b>" + TITLE + " v"
                        + VERSION + "</b> &mdash; "
                        + TEXT;

   // layout the dialog
   this.sizerLeft = new VerticalSizer;
   this.sizerLeft.add(this.helpLabel);
   this.sizerLeft.addSpacing(16);

   this.imageRGBViewSelectorFrame = new Frame(this);
   this.imageRGBViewSelectorFrame.scaledMinWidth = LEFTWIDTH;
   this.imageRGBViewSelectorFrame.scaledMaxWidth = LEFTWIDTH;
   this.imageRGBViewSelectLabel = new Label(this);
   this.imageRGBViewSelectLabel.text = "RGB Image:";
   this.imageRGBViewSelectLabel.toolTip = "<p>Select the RGB image to be combined (starless or with stars, in this case you should also assign a star mask).</p>";
   this.imageRGBViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.imageRGBViewSelector = new ViewList(this);
   this.imageRGBViewSelector.scaledMinWidth = LEFTWIDTH-80;
   this.imageRGBViewSelector.getMainViews();

   with(this.imageRGBViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;
         add(this.imageRGBViewSelectLabel);
         addSpacing(8);
         add(this.imageRGBViewSelector);
         adjustToContents();
      }
   }

   this.imageRGBViewSelector.onViewSelected = function(view) {
      combineRGBNBParameters.imageRGB = view;
      if (combineRGBNBParameters.previewImage != undefined) {
         closeView(combineRGBNBParameters.previewImage);
      }
      combineRGBNBParameters.previewImage = cloneHidden(combineRGBNBParameters.imageRGB, "_preview", false);
      let previewImage = combineRGBNBParameters.previewImage;
      if (previewImage) {
         let metadata = {
                     width: previewImage.image.width,
                     height: previewImage.image.height
         };

         this.dialog.previewControl.SetPreview(previewImage.image, previewImage, metadata);
         this.dialog.previewControl.zoomToFit();
         this.dialog.previewControl.forceRedraw();
      }

   }


   this.sizerLeft.add(this.imageRGBViewSelectorFrame);
   this.sizerLeft.addSpacing(8);

   //
   // Narrowband R
   //
   this.imageNBRViewSelectorFrame = new Frame(this);
   this.imageNBRViewSelectorFrame.scaledMinWidth = LEFTWIDTH;
   this.imageNBRViewSelectorFrame.scaledMaxWidth = LEFTWIDTH;
   this.imageNBRViewSelectLabel = new Label(this);
   this.imageNBRViewSelectLabel.text = "Narrowband Red\nor NB Color:";
   this.imageNBRViewSelectLabel.toolTip = "<p>Select the <b>starless</b> Narrowband red image to be combined. Selecting the RGB Narrowband image here will split this into the red, green and blue channel</p>";
   this.imageNBRViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.imageNBRViewSelector = new ViewList(this);
   //this.imageNBRViewSelector.scaledMinWidth = LEFTWIDTH-80;
   this.imageNBRViewSelector.getMainViews();

   with(this.imageNBRViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;
         add(this.imageNBRViewSelectLabel);
         addSpacing(8);
         add(this.imageNBRViewSelector);

      }
   }

   this.imageNBRViewSelector.onViewSelected = function(view) {

      if (view.image.isGrayscale) {
         combineRGBNBParameters.imageNB_R = view;

         processPreview();
         let previewImage = combineRGBNBParameters.previewImage;
         if (previewImage) {
            let metadata = {
                        width: previewImage.image.width,
                        height: previewImage.image.height
            };

            this.dialog.previewControl.SetPreview(previewImage.image, previewImage, metadata);
            this.dialog.previewControl.forceRedraw();
         }
      }
      else {
         // extract channels
         let viewHa = extractHa(view);
         viewHa.window.iconize();

         let viewOIII = extractOIII(view);
         viewOIII.window.iconize();

         let viewG = extractOIIIG(view);
         viewG.window.iconize();

         this.dialog.imageNBRViewSelector.currentView = viewHa;
         this.dialog.imageNBGViewSelector.currentView = viewG;
         this.dialog.imageNBBViewSelector.currentView = viewOIII;

         combineRGBNBParameters.imageNB_R = viewHa;
         combineRGBNBParameters.imageNB_G = viewG;
         combineRGBNBParameters.imageNB_B = viewOIII;
         processPreview();
         let previewImage = combineRGBNBParameters.previewImage;
         if (previewImage) {
            let metadata = {
                        width: previewImage.image.width,
                        height: previewImage.image.height
            };

            this.dialog.previewControl.SetPreview(previewImage.image, previewImage, metadata);
            this.dialog.previewControl.forceRedraw();
         }

      }
   }

   this.sizerLeft.add(this.imageNBRViewSelectorFrame);
   this.sizerLeft.addSpacing(4);

   //
   // Narrowband G
   //
   this.imageNBGViewSelectorFrame = new Frame(this);
   this.imageNBGViewSelectorFrame.scaledMinWidth = LEFTWIDTH;
   this.imageNBGViewSelectorFrame.scaledMaxWidth = LEFTWIDTH;
   this.imageNBGViewSelectLabel = new Label(this);
   this.imageNBGViewSelectLabel.text = "Narrowband Green:";
   this.imageNBGViewSelectLabel.toolTip = "<p>Select the <b>starless</b> Narrowband green image to be combined.</p>";
   this.imageNBGViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.imageNBGViewSelector = new ViewList(this);
   this.imageNBGViewSelector.scaledMaxWidth = LEFTWIDTH-80;
   this.imageNBGViewSelector.getMainViews();

   with(this.imageNBGViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;
         add(this.imageNBGViewSelectLabel);
         addSpacing(8);
         add(this.imageNBGViewSelector);
         adjustToContents();
      }
   }

   this.imageNBGViewSelector.onViewSelected = function(view) {

      if (view.image.isGrayscale) {
         combineRGBNBParameters.imageNB_G = view;

         processPreview();
         let previewImage = combineRGBNBParameters.previewImage;
         if (previewImage) {
            let metadata = {
                        width: previewImage.image.width,
                        height: previewImage.image.height
            };

            this.dialog.previewControl.SetPreview(previewImage.image, previewImage, metadata);
            this.dialog.previewControl.forceRedraw();
         }
      }
      else this.imageNBGViewSelector.currentView = null;
   }

   this.sizerLeft.add(this.imageNBGViewSelectorFrame);
   this.sizerLeft.addSpacing(4);

   //
   // Narrowband B
   //
   this.imageNBBViewSelectorFrame = new Frame(this);
   this.imageNBBViewSelectorFrame.scaledMinWidth = LEFTWIDTH;
   this.imageNBBViewSelectorFrame.scaledMaxWidth = LEFTWIDTH;
   this.imageNBBViewSelectLabel = new Label(this);
   this.imageNBBViewSelectLabel.text = "Narrowband Blue:";
   this.imageNBBViewSelectLabel.toolTip = "<p>Select the <b>starless</b> Narrowband blue image to be combined.</p>";
   this.imageNBBViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.imageNBBViewSelector = new ViewList(this);
   this.imageNBBViewSelector.scaledMaxWidth = LEFTWIDTH-80;
   this.imageNBBViewSelector.getMainViews();

   with(this.imageNBBViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;
         add(this.imageNBBViewSelectLabel);
         addSpacing(8);
         add(this.imageNBBViewSelector);
         adjustToContents();
      }
   }

   this.imageNBBViewSelector.onViewSelected = function(view) {

      if (view.image.isGrayscale) {

         combineRGBNBParameters.imageNB_B = view;

         processPreview();
         let previewImage = combineRGBNBParameters.previewImage;
         if (previewImage) {
            let metadata = {
                        width: previewImage.image.width,
                        height: previewImage.image.height
            };

            this.dialog.previewControl.SetPreview(previewImage.image, previewImage, metadata);
            this.dialog.previewControl.forceRedraw();
         }
      }
      else this.imageNBBViewSelector.currentView = null;
   }

   this.sizerLeft.add(this.imageNBBViewSelectorFrame);
   this.sizerLeft.addSpacing(8);


   this.imageStarMaskViewSelectorFrame = new Frame(this);
   this.imageStarMaskViewSelectorFrame.scaledMinWidth = LEFTWIDTH;
   this.imageStarMaskViewSelectorFrame.scaledMaxWidth = LEFTWIDTH;
   this.imageStarMaskViewSelectLabel = new Label(this);
   this.imageStarMaskViewSelectLabel.text = "Star Mask (optional):";
   this.imageStarMaskViewSelectLabel.toolTip = "<p>Select the star mask to be used. Create this mask using StarXTerminator or StarNet++ and create a grayscale image from the star image without any blur for best results.</p>";
   this.imageStarMaskViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.imageStarMaskViewSelector = new ViewList(this);
   this.imageStarMaskViewSelector.scaledMaxWidth = LEFTWIDTH-80;
   this.imageStarMaskViewSelector.getMainViews();

   with(this.imageStarMaskViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;
         add(this.imageStarMaskViewSelectLabel);
         addSpacing(8);
         add(this.imageStarMaskViewSelector);
         adjustToContents();
      }
   }

   this.imageStarMaskViewSelector.onViewSelected = function(view) {
      combineRGBNBParameters.starMask = view;

      if (view) {
         this.dialog.starBalanceSlider.enabled = true;
         this.dialog.resetStarBalanceButton.enabled = true;
      } else {
         this.dialog.starBalanceSlider.enabled = false;
         this.dialog.resetStarBalanceButton.enabled = false;
      }

      processPreview();
      let previewImage = combineRGBNBParameters.previewImage;
      if (previewImage) {
         let metadata = {
                     width: previewImage.image.width,
                     height: previewImage.image.height
         };

         this.dialog.previewControl.SetPreview(previewImage.image, previewImage, metadata);
         this.dialog.previewControl.forceRedraw();
      }
   }

   this.sizerLeft.add(this.imageStarMaskViewSelectorFrame);
   this.sizerLeft.addSpacing(16);

   this.blackpointsFrame = new SectionBar(this, "Blackpoints");
   this.blackpointsFrame.scaledMinWidth = LEFTWIDTH;
   this.blackpointsFrame.scaledMaxWidth = LEFTWIDTH;

   this.blackpointsFrame.onToggleSection =  function( section, toggleBegin ) {
      if ( !toggleBegin ) {
         section.dialog.setVariableHeight();
         section.dialog.adjustToContents();
         if ( section.dialog.blackpointsFrame.isCollapsed() )
            section.dialog.setFixedHeight();
         else
            section.dialog.setMinHeight();
      }
   }


   this.blackpointsControl = new Control( this );
   this.blackpointsControl.scaledMinWidth = LEFTWIDTH;
   this.blackpointsControl.scaledMaxWidth = LEFTWIDTH;

   // Red sliders
   this.redCSlider = new NumericControl(this);
   this.redCSlider.label.text = "Red: ";
   this.redCSlider.toolTip = "<p>Decrease the blackpoint to see more of the fainter narrowband details.</p>";
   this.redCSlider.setRange(0.0, 1.0);
   this.redCSlider.slider.setRange(0.0, 1000.0);
   this.redCSlider.setPrecision(3);
   this.redCSlider.setReal(true);
   this.redCSlider.label.minWidth = labelMinWidth;
   this.redCSlider.setValue(combineRGBNBParameters.C_R);
   this.redCSlider.onValueUpdated = function(t) {
      combineRGBNBParameters.C_R = t;
   };

   this.resetRedCButton = new ToolButton( this );
   this.resetRedCButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetRedCButton.setScaledFixedSize( 24, 24 );
   this.resetRedCButton.toolTip = "<p>Reset the saturation.</p>";
   this.resetRedCButton.onClick = () => {
      combineRGBNBParameters.C_R = C_DEFAULT;
      this.redCSlider.setValue(C_DEFAULT);
   }

   this.redSlider = new HorizontalSizer();
   this.redSlider.add(this.redCSlider);
   this.redSlider.add(this.resetRedCButton);

   // green sliders
   this.greenCSlider = new NumericControl(this);
   this.greenCSlider.label.text = "Green: ";
   this.greenCSlider.toolTip = "<p>Decrease the blackpoint to see more of the fainter narrowband details.</p>";
   this.greenCSlider.setRange(0.0, 1.0);
   this.greenCSlider.slider.setRange(0.0, 1000.0);
   this.greenCSlider.setPrecision(3);
   this.greenCSlider.setReal(true);
   this.greenCSlider.setValue(combineRGBNBParameters.C_G);
   this.greenCSlider.label.minWidth = labelMinWidth;
   this.greenCSlider.onValueUpdated = function(t) {
      combineRGBNBParameters.C_G = t;
   };

   this.resetGreenCButton = new ToolButton( this );
   this.resetGreenCButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetGreenCButton.setScaledFixedSize( 24, 24 );
   this.resetGreenCButton.toolTip = "<p>Reset the saturation.</p>";
   this.resetGreenCButton.onClick = () => {
      combineRGBNBParameters.C_G = C_DEFAULT;
      this.greenCSlider.setValue(C_DEFAULT);
   }

   this.greenSlider = new HorizontalSizer();
   this.greenSlider.add(this.greenCSlider);
   this.greenSlider.add(this.resetGreenCButton);

   // blue sliders
   this.blueCSlider = new NumericControl(this);
   this.blueCSlider.label.text = "Blue: ";
   this.blueCSlider.toolTip = "<p>Decrease the blackpoint to see more of the fainter narrowband details.</p>";
   this.blueCSlider.setRange(0.0, 1.0);
   this.blueCSlider.slider.setRange(0.0, 1000.0);
   this.blueCSlider.setPrecision(3);
   this.blueCSlider.setReal(true);
   this.blueCSlider.setValue(combineRGBNBParameters.C_B);
   this.blueCSlider.label.minWidth = labelMinWidth;
   this.blueCSlider.onValueUpdated = function(t) {
      combineRGBNBParameters.C_B = t;
   };

   this.resetBlueCButton = new ToolButton( this );
   this.resetBlueCButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetBlueCButton.setScaledFixedSize( 24, 24 );
   this.resetBlueCButton.toolTip = "<p>Reset the saturation.</p>";
   this.resetBlueCButton.onClick = () => {
      combineRGBNBParameters.C_B = C_DEFAULT;
      this.blueCSlider.setValue(C_DEFAULT);
   }

   this.blueSlider = new HorizontalSizer();
   this.blueSlider.add(this.blueCSlider);
   this.blueSlider.add(this.resetBlueCButton);

   this.balanceFrame = new SectionBar(this, "Balance");
   this.balanceFrame.scaledMaxWidth = LEFTWIDTH;

   this.balanceFrame.onToggleSection =  function( section, toggleBegin ) {
      if ( !toggleBegin ) {
         section.dialog.setVariableHeight();
         section.dialog.adjustToContents();
         if ( section.dialog.balanceFrame.isCollapsed() )
            section.dialog.setFixedHeight();
         else
            section.dialog.setMinHeight();
      }
   }

   this.balanceControl = new Control(this);
   this.balanceControl.scaledMinWidth = LEFTWIDTH;
   this.balanceControl.scaledMaxWidth = LEFTWIDTH;
   this.balanceControl.backgroundColor = 0xFFD8D7D3;

   this.amountSlider = new NumericControl(this);
   this.amountSlider.label.text = "Amount: ";
   this.amountSlider.toolTip = "<p>Increase or decrease the amount of the narrowband data to be added to the final image.</p>";
   this.amountSlider.setRange(0.0, 3.0);
   this.amountSlider.slider.setRange(0.0, 3000.0);
   this.amountSlider.setPrecision(3);
   this.amountSlider.setReal(true);
   this.amountSlider.setValue(combineRGBNBParameters.amount);
   this.amountSlider.label.minWidth = labelMinWidth;
   this.amountSlider.onValueUpdated = function(t) {
      combineRGBNBParameters.amount = t;
   };

   this.resetAmountButton = new ToolButton( this );
   this.resetAmountButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetAmountButton.setScaledFixedSize( 24, 24 );
   this.resetAmountButton.toolTip = "<p>Reset the highlight anmount.</p>";
   this.resetAmountButton.onClick = () => {
      combineRGBNBParameters.amount = 1.0;
      this.amountSlider.setValue(1.0);
   }

   this.amountControl = new HorizontalSizer();
   this.amountControl.add(this.amountSlider);
   this.amountControl.add(this.resetAmountButton);

   this.vibranceSlider = new NumericControl(this);
   this.vibranceSlider.label.text = "Vibrance: ";
   this.vibranceSlider.toolTip = "<p>Increase or decrease the color vibrance of the final image.</p>";
   this.vibranceSlider.setRange(-1.0, 1.0);
   this.vibranceSlider.slider.setRange(-1000.0, 1000.0);
   this.vibranceSlider.setPrecision(3);
   this.vibranceSlider.setReal(true);
   this.vibranceSlider.setValue(combineRGBNBParameters.vibrance);
   this.vibranceSlider.label.minWidth = labelMinWidth;
   this.vibranceSlider.onValueUpdated = function(t) {
      combineRGBNBParameters.vibrance = t;
   };

   this.resetVibranceButton = new ToolButton( this );
   this.resetVibranceButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetVibranceButton.setScaledFixedSize( 24, 24 );
   this.resetVibranceButton.toolTip = "<p>Reset the vibrance anmount.</p>";
   this.resetVibranceButton.onClick = () => {
      combineRGBNBParameters.vibrance = Vibrance_DEFAULT;
      this.vibranceSlider.setValue(Vibrance_DEFAULT);
   }

   this.vibranceControl = new HorizontalSizer();
   this.vibranceControl.add(this.vibranceSlider);
   this.vibranceControl.add(this.resetVibranceButton);

   this.starBalanceSlider = new NumericControl(this);
   this.starBalanceSlider.label.text = "Stars: ";
   this.starBalanceSlider.toolTip = "<p>Increase or decrease the effect of the star mask.</p>";
   this.starBalanceSlider.setRange(0.0, 1.0);
   this.starBalanceSlider.slider.setRange(0.0, 1000.0);
   this.starBalanceSlider.setPrecision(3);
   this.starBalanceSlider.setReal(true);
   this.starBalanceSlider.setValue(combineRGBNBParameters.starBalance);
   this.starBalanceSlider.enabled = false;
   this.starBalanceSlider.label.minWidth = labelMinWidth;
   this.starBalanceSlider.onValueUpdated = function(t) {
      combineRGBNBParameters.starBalance = t;
   };

   this.resetStarBalanceButton = new ToolButton( this );
   this.resetStarBalanceButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetStarBalanceButton.setScaledFixedSize( 24, 24 );
   this.resetStarBalanceButton.toolTip = "<p>Reset the star balance.</p>";
   this.resetStarBalanceButton.enabled = true;
   this.resetStarBalanceButton.onClick = () => {
      combineRGBNBParameters.starBalance = StarBalance_DEFAULT;
      this.starBalanceSlider.setValue(StarBalance_DEFAULT);
   }

   this.starBalanceControl = new HorizontalSizer();
   this.starBalanceControl.add(this.starBalanceSlider);
   this.starBalanceControl.add(this.resetStarBalanceButton);

   this.rescaleSlider = new NumericControl(this);
   this.rescaleSlider.label.text = "Rescale:  ";
   this.rescaleSlider.toolTip = "<p>Decrease the rescale value to avoid clipping highlights.</p>";
   this.rescaleSlider.setRange(0.75, 1.0);
   this.rescaleSlider.slider.setRange(750.0, 1000.0);
   this.rescaleSlider.setPrecision(3);
   this.rescaleSlider.setReal(true);
   this.rescaleSlider.setValue(combineRGBNBParameters.rescaleValue);
   this.rescaleSlider.label.minWidth = labelMinWidth;

   this.rescaleSlider.onValueUpdated = function(t) {
      combineRGBNBParameters.rescaleValue = t;
   };

   this.resetRescaleButton = new ToolButton( this );
   this.resetRescaleButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetRescaleButton.setScaledFixedSize( 24, 24 );
   this.resetRescaleButton.toolTip = "<p>Reset rescaling.</p>";
   this.resetRescaleButton.onClick = () => {
      combineRGBNBParameters.rescaleValue = 1.0;
      this.rescaleSlider.setValue(1.0);
   }

   this.rescaleControl = new HorizontalSizer();
   this.rescaleControl.add(this.rescaleSlider);
   this.rescaleControl.add(this.resetRescaleButton);

   this.highlightSlider = new NumericControl(this);
   this.highlightSlider.label.text = "Highlights: ";
   this.highlightSlider.toolTip = "<p>Decrease the highlights.</p>";
   this.highlightSlider.setRange(0.5, 1.0);
   this.highlightSlider.slider.setRange(500.0, 1000.0);
   this.highlightSlider.setPrecision(3);
   this.highlightSlider.setReal(true);
   this.highlightSlider.setValue(combineRGBNBParameters.highlightsValue);
   this.highlightSlider.label.minWidth = labelMinWidth;
   this.highlightSlider.onValueUpdated = function(t) {
      combineRGBNBParameters.highlightsValue = t;
   };

   this.resetHighlightButton = new ToolButton( this );
   this.resetHighlightButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetHighlightButton.setScaledFixedSize( 24, 24 );
   this.resetHighlightButton.toolTip = "<p>Reset Highlights.</p>";
   this.resetHighlightButton.onClick = () => {
      combineRGBNBParameters.highlightsValue = 1.0;
      this.highlightSlider.setValue(1.0);
   }

   this.highlightControl = new HorizontalSizer();
   this.highlightControl.add(this.highlightSlider);
   this.highlightControl.add(this.resetHighlightButton);


   with(this.blackpointsControl) {
      sizer = new VerticalSizer();
      backgroundColor = 0xFFD8D7D3;
      with(sizer) {

         addSpacing(8);
         add(this.redSlider);
         addSpacing(4);
         add(this.greenSlider);
         addSpacing(4);
         add(this.blueSlider);
         addSpacing(8);
         adjustToContents();
         setMinHeight();
      }
   }
   this.blackpointsFrame.setSection( this.blackpointsControl );

   with(this.balanceControl) {
      sizer = new VerticalSizer();
      backgroundColor = 0xFFD8D7D3;
      with(sizer) {

         addSpacing(8);
         add(this.amountControl);
         addSpacing(4);
         add(this.vibranceControl);
         addSpacing(4);
         add(this.highlightControl);

         addSpacing(16);
         add(this.starBalanceControl);

         addSpacing(4);
         add(this.rescaleControl);
         addSpacing(8);

         adjustToContents();
         setMinHeight();
      }
   }
   this.balanceFrame.setSection( this.balanceControl );

   this.sizerLeft.add(this.blackpointsFrame);
   this.sizerLeft.add(this.blackpointsControl);
   this.sizerLeft.addSpacing(8);
   this.sizerLeft.add(this.balanceFrame);
   this.sizerLeft.add(this.balanceControl);
   this.sizerLeft.addSpacing(128);
   this.sizerLeft.addStretch();

   this.buttonFrame = new Frame;
   this.buttonFrame.scaledMinWidth = LEFTWIDTH;
   this.buttonFrame.scaledMaxWidth = LEFTWIDTH;
   this.buttonFrame.sizer = new HorizontalSizer;
   this.buttonFrame.sizer.margin = 6;
   this.buttonFrame.sizer.spacing = 6;
   this.buttonFrame.sizer.addSpacing(8);

   this.newInstanceButton = new ToolButton( this );
   this.newInstanceButton.icon = this.scaledResource( ":/process-interface/new-instance.png" );
   this.newInstanceButton.setScaledFixedSize( 24, 24 );
   this.newInstanceButton.onMousePress = () => {

      if (combineRGBNBParameters.hAlphaView) {
         combineRGBNBParameters.save();
         this.newInstance();
      }
   }

   this.ok_Button = new ToolButton( this );
   this.ok_Button.icon = this.scaledResource( ":/process-interface/execute.png" );
   this.ok_Button.setScaledFixedSize( 24, 24 );
   this.ok_Button.toolTip = "<p>Change the target view image using the specified parameters.</p>";
   this.ok_Button.onClick = () => {
      process();
      this.ok();

   };

   this.cancel_Button = new ToolButton( this );
   this.cancel_Button.icon = this.scaledResource( ":/process-interface/cancel.png" );
   this.cancel_Button.setScaledFixedSize( 24, 24 );
   this.cancel_Button.toolTip = "<p>Close this dialog with no changes.</p>";
   this.cancel_Button.onClick = () => {
      this.cancel();
   };

   this.help_Button = new ToolButton( this );
   this.help_Button.icon = this.scaledResource(":/process-interface/browse-documentation.png");
   this.help_Button.setScaledFixedSize( 24, 24 );
   this.help_Button.toolTip = "<p>Shows the script documentation.</p>";
   this.help_Button.onClick = () => {
      Dialog.browseScriptDocumentation("CombineRGBAndNarrowband", "Combine RGB & Narrowband Images");
   };

   this.updateRefresh_Button = new ToolButton( this );
   this.updateRefresh_Button.icon = this.scaledResource( ":/icons/refresh.png" );
   this.updateRefresh_Button.setScaledFixedSize( 24, 24 );
   this.updateRefresh_Button.toolTip = "<p>Update the preview.</p>";
   this.updateRefresh_Button.onClick = () => {
      processPreview();
      let previewImage = combineRGBNBParameters.previewImage;
      if (previewImage) {
         let metadata = {
                     width: previewImage.image.width,
                     height: previewImage.image.height
         };

         this.dialog.previewControl.SetPreview(previewImage.image, combineRGBNBParameters.previewImage, metadata);
         this.dialog.previewControl.forceRedraw();
      }
   };

   this.buttonFrame.sizer.add( this.newInstanceButton );
   this.buttonFrame.sizer.addSpacing(16);

   this.buttonFrame.sizer.add( this.ok_Button );
   this.buttonFrame.sizer.addSpacing(16);
   this.buttonFrame.sizer.add( this.cancel_Button );
   this.buttonFrame.sizer.addStretch();
   this.buttonFrame.sizer.add(this.updateRefresh_Button);
   this.buttonFrame.sizer.addSpacing(16);
   this.buttonFrame.sizer.add( this.help_Button );
   this.buttonFrame.sizer.addSpacing(8);

   this.sizerLeft.add(this.buttonFrame);

   this.previewControl = new PreviewControl(this);
   this.previewControl.scaledMinWidth = 800;
   this.previewControl.minHeight = 600;

   this.sizer = new HorizontalSizer;
   this.sizer.margin = 8;
   this.sizer.spacing = 6;
   this.sizer.add(this.sizerLeft);
   this.sizer.add(this.previewControl);

   this.onHide = function() {

      closeAllTempViews();
   }
}

CombineRGBAndNBDialog.prototype = new Dialog


function main() {

   if (Parameters.isViewTarget || Parameters.isGlobalTarget) {
      continuumParameters.load();
   }

   let dialog = new CombineRGBAndNBDialog();
   dialog.execute();

}

main();
