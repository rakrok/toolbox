#include <pjsr/StdIcon.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/TextAlign.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/ColorSpace.jsh>
#include <pjsr/Interpolation.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/CheckState.jsh>
#include <pjsr/SampleType.jsh>
#include <pjsr/SectionBar.jsh>
#include "PixInsightToolsPreviewControl.jsh"


#feature-id CombineHaWithRGB :  Toolbox > CombineHaWithRGB

#feature-info  A script to combine H Alpha to an RGB image.<br/>\
   <br/>\
   A script to combine H Alpha to an RGB image. \
   <br/> \
   Copyright &copy; 2024 Juergen Terpe


#define VERSION "1.7"
#define TITLE  "Combine H Alpha"
#define TEXT   "Combine H Alpha to an RGB image."

 /*
 * Default STF Parameters
 */
// Shadows clipping point in (normalized) MAD units from the median.
#define SHADOWS_CLIP -2.80
// Target mean background in the [0,1] range.
#define TARGET_BKG    0.25

#define LEFTWIDTH 400
#define LEFTWIDTH2 LEFTWIDTH-12
#define MINBGNOISE 0.0
#define MAXBGNOISE 2.0


var combineHaParameters = {
   amount: 2.0,
   beta: 0.0,
   bg: 0.015,
   sigma: 0.0,
   linearMode: true,
   rgbLinked: true,
   rgbView: undefined,
   alphaView: undefined,
   previewImage: undefined,
   mask: undefined,
   invertMask: true,


   // stores the current parameters values into the script instance
   save: function() {
      if (combineHaParameters.alphaView)
         Parameters.set("alphaView", combineHaParameters.alphaView.id);
      Parameters.set("amount", combineHaParameters.amount);
      Parameters.set("beta", combineHaParameters.beta);
      Parameters.set("linear", combineHaParameters.linearMode);
      Parameters.set("rgbLinked", combineHaParameters.rgbLinked);
      Parameters.set("bg", combineHaParameters.bg);
      if (combineHaParameters.rgbView)
         Parameters.set("rgbView", combineHaParameters.rgbView);
      if (combineHaParameters.mask)
         Parameters.set("mask", combineHaParameters.mask);
      Parameters.set("invertMask", combineHaParameters.invertMask);
      Parameters.set("sigma", combineHaParameters.sigma);
   },

   // loads the script instance parameters
   load: function() {

      Console.writeln("Loading previous parameters...");

      if (Parameters.has("linear"))
         combineHaParameters.linearMode = Parameters.getBoolean("linear");
      if (Parameters.has("rgbLinked"))
         combineHaParameters.rgbLinked = Parameters.getBoolean("rgbLinked");
      if (Parameters.has("amount"))
         combineHaParameters.amount = Parameters.getReal("amount");
      if (Parameters.has("beta"))
         combineHaParameters.beta = Parameters.getReal("beta");
      if (Parameters.has("bg"))
         combineHaParameters.bg = Parameters.getReal("bg");
      if (Parameters.has("sigma"))
         combineHaParameters.sigma = Parameters.getReal("sigma");

      if (Parameters.has("rgbView"))
         combineHaParameters.rgbView = View.viewById(Parameters.get("alphaView"));

      if (Parameters.has("mask"))
         combineHaParameters.mask = View.viewById(Parameters.get("mask"));

      if (Parameters.has("invertMask"))
         combineHaParameters.invertMask = Parameters.getBoolean("invertMask");

      if (Parameters.has("alphaView")) {
         Console.writeln("H-Alpha view: "+ Parameters.get("alphaView"));

         combineHaParameters.alphaView = View.viewById(Parameters.get("alphaView"));

         Console.writeln("Amount: "+combineHaParameters.amount
         + ", Beta: "+ combineHaParameters.beta
         + ", Linear: " + combineHaParameters.linearMode
         + ", Linked: "+ combineHaParameters.rgbLinked
         + ", BG: " + combineHaParameters.bg
         + ", Alpha-View: "+ combineHaParameters.alphaView.id);
      }


   }
}

function getPreviewKey() {
   if (!combineHaParameters.rgbView || !combineHaParameters.alphaView)
      return ""
   else {
      var maskId = "";
      if (combineHaParameters.mask)
         maskId = combineHaParameters.mask.id

      return "Amount: "+combineHaParameters.amount
         + ", Beta: "+ combineHaParameters.beta
         + ", BG: " + combineHaParameters.bg
         + ", Sigma: " + combineHaParameters.sigma
         + ", Linear: " + combineHaParameters.linearMode
         + ", Linked: "+ combineHaParameters.rgbLinked
         + ", InvertMask: "+ combineHaParameters.invertMask
         + ", Mask: " + maskId
         + ", Preview: "+combineHaParameters.rgbView;
   }
}

function closeView(view) {
   if (view !== undefined ) {
      Console.writeln("closing view: "+view.id);

      var imageView = View.viewById(view.id);
      imageView.window.forceClose();
   }
}

function hideView(view) {
   if (view !== undefined ) {
      var imageView = View.viewById(view.id);
      imageView.window.hide();
   }
}

function clone(view, appendix, showView=true) {
   var P = new PixelMath;
   P.expression = "$T";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = true;
   P.rescale = false;
   P.truncate = true;
   P.createNewImage = true;
   P.showNewImage = showView;
   P.newImageId = view.id + appendix;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return View.viewById(P.newImageId);
}

function cloneBlack(view, appendix, showView=true) {
   var P = new PixelMath;
   P.expression = "0";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = true;
   P.rescale = false;
   P.truncate = true;
   P.createNewImage = true;
   P.showNewImage = showView;
   P.newImageId = view.id + appendix;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return View.viewById(P.newImageId);
}


function doSTFStretch(view) {
   var stf = new ScreenTransferFunction;

   var n = view.image.isColor ? 3 : 1;

   var median = view.computeOrFetchProperty( "Median" );

   var mad = view.computeOrFetchProperty( "MAD" );
   mad.mul( 1.4826 ); // coherent with a normal distribution

   if ( combineHaParameters.rgbLinked ) {
      var c0 = 0, m = 0;

      for ( var c = 0; c < n; ++c )
      {
         if ( 1 + mad.at( c ) != 1 )
            c0 += median.at( c ) + SHADOWS_CLIP * mad.at( c );
         m  += median.at( c );
      }
      c0 = Math.range( c0/n, 0.0, 1.0 );
      m = Math.mtf( TARGET_BKG, m/n - c0 );


      stf.STF = [ // c0, c1, m, r0, r1
                  [c0, 1, m, 0, 1],
                  [c0, 1, m, 0, 1],
                  [c0, 1, m, 0, 1],
                  [0, 1, 0.5, 0, 1] ];

   }
   else {
      /*
       * Unlinked RGB channnels: Compute automatic stretch functions for
       * individual RGB channels separately.
       */
      var A = [ // c0, c1, m, r0, r1
               [0, 1, 0.5, 0, 1],
               [0, 1, 0.5, 0, 1],
               [0, 1, 0.5, 0, 1],
               [0, 1, 0.5, 0, 1] ];

      for ( var c = 0; c < n; ++c ) {
         var c0 = (1 + mad.at( c ) != 1) ? Math.range( median.at( c ) + SHADOWS_CLIP * mad.at( c ), 0.0, 1.0 ) : 0.0;
         var m  = Math.mtf( TARGET_BKG, median.at( c ) - c0 );
         A[c] = [c0, 1, m, 0, 1];
      }

      stf.STF = A;
   }

   stf.executeOn( view );
}

function doAutoStretch( view )
{
   var P = new HistogramTransformation;
   var n = view.image.isColor ? 3 : 1;

   if ( combineHaParameters.rgbLinked ) {
      var c0 = 0, m = 0;

      var median = view.computeOrFetchProperty( "Median" );
      var mad = view.computeOrFetchProperty( "MAD" );
      mad.mul( 1.4826 ); // coherent with a normal distribution

      for ( var c = 0; c < n; ++c )
      {
         if ( 1 + mad.at( c ) != 1 )
            c0 += median.at( c ) + SHADOWS_CLIP * mad.at( c );
         m  += median.at( c );
      }
      c0 = Math.range( c0/n, 0.0, 1.0 );
      m = Math.mtf( TARGET_BKG, m/n - c0 );

      P.H = [ // c0, m, c1, r0, r1
         [0.00000000, 0.5, 1.00000000, 0.00000000, 1.00000000],
         [0.00000000, 0.5, 1.00000000, 0.00000000, 1.00000000],
         [0.00000000, 0.5, 1.00000000, 0.00000000, 1.00000000],
         [c0,   m, 1.00000000, 0.00000000, 1.00000000],
         [0.00000000, 0.5, 1.00000000, 0.00000000, 1.00000000]
      ];
   } else {
      // unlinked stretch

      var median = view.computeOrFetchProperty( "Median" );
      var mad = view.computeOrFetchProperty( "MAD" );
      mad.mul( 1.4826 ); // coherent with a normal distribution

      var A = [ // c0, m, c1, r0, r1
               [0, 0.5, 1, 0, 1],
               [0, 0.5, 1, 0, 1],
               [0, 0.5, 1, 0, 1],
               [0, 0.5, 1, 0, 1] ];

      for ( var c = 0; c < n; ++c )
      {
         if ( median.at( c ) < 0.5 )
         {
            /*
             * Noninverted channel
             */
            var c0 = (1 + mad.at( c ) != 1) ? Math.range( median.at( c ) + SHADOWS_CLIP * mad.at( c ), 0.0, 1.0 ) : 0.0;
            var m  = Math.mtf( TARGET_BKG, median.at( c ) - c0 );
            A[c] = [c0, m, 1, 0, 1];
         }
         else
         {
            /*
             * Inverted channel
             */
            var c1 = (1 + mad.at( c ) != 1) ? Math.range( median.at( c ) - SHADOWS_CLIP * mad.at( c ), 0.0, 1.0 ) : 1.0;
            var m  = Math.mtf( c1 - median.at( c ), TARGET_BKG );
            A[c] = [0, m, c1, 0, 1];
         }
      }

      P.H = A;
   }

   P.executeOn(view);
}

function doAutoStretchImage(image, id) {

   var imageWindow = new ImageWindow(image.width, image.height, image.numberOfChannels, image.bitsPerSample,
                                 image.sampleType == SampleType_Real, image.isColor, id );
   var view = imageWindow.mainView;
   view.beginProcess( UndoFlag_NoSwapFile );
   view.image.assign( image );
   view.endProcess();

   doAutoStretch(view);
   image.assign(view.image);
   imageWindow.forceClose();

   return image;
}

function getHalpha(hAlphaView, median) {
   var P = new PixelMath;
   P.expression = "iif($T > MED, Q*($T - MED), 0)";
   P.useSingleExpression = true;
   P.symbols = "Q = "+ combineHaParameters.amount + ", "
                + "MED = " + median;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = true;
   P.truncate = true;
   P.rescale = false;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = hAlphaView.id + "_ha_temp";

   P.executeOn(hAlphaView);

   return View.viewById(P.newImageId);
}


function combineHa(targetView, rgbView, hAlphaView, median) {

   let ha = getHalpha(hAlphaView, median);

   var P = new PixelMath;
   P.expression = "combine("+rgbView.id +"[0], "+ha.id +" , op_screen())";
   P.expression1 = rgbView.id+"[1]";
   P.expression2 = rgbView.id +"[2] + iif("+hAlphaView.id+" > MED, Q*("+hAlphaView.id+" - MED)*BETA, 0)";
   P.expression3 = "";
   P.useSingleExpression = false;
   P.symbols = "Q = "+ combineHaParameters.amount + ", "
                + "BETA = " + combineHaParameters.beta + ", "
                + "MED = " + median;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = true;
   P.truncate = true;
   P.rescale = false;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;

   P.executeOn(targetView);

   ha.window.forceClose();
}

function assignWithMask(targetView, view) {
   var P = new PixelMath;
   P.expression = view.id;
   P.useSingleExpression = false;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;
   P.executeOn(targetView);
}

function convolution(view, sigma) {
   var P = new PixelMath;
   P.expression = "gconv($T, SIGMA)";
   P.useSingleExpression = true;
   P.symbols = "SIGMA="+sigma;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = true;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;

   P.executeOn(view);
}


function dropNoise(view, median, k) {

   var mx = Math.min(median*MAXBGNOISE, 0.5);
   var my = Math.min(median*MAXBGNOISE, 0.5);

   var P = new CurvesTransformation;
   P.K = [ // x, y
      [0.00000, 0.00000],
      [median*k, 0.0],
      [mx, my],
      [0.75,    0.75000],
      [1.00000, 1.00000]
   ];
   P.Kt = CurvesTransformation.prototype.AkimaSubsplines;
   P.executeOn(view);
}

function processPreview() {
   if (combineHaParameters.rgbView && combineHaParameters.alphaView) {

      var alphaView = combineHaParameters.alphaView;
      var view = combineHaParameters.rgbView;
      // create a hidden image for processing in the background
      var v = cloneBlack(view, "_tmp_preview", false);

      try {

         v.window.rgbWorkingSpace = view.window.rgbWorkingSpace;

         let median = combineHaParameters.alphaView.image.median();
         var alphaView = clone(combineHaParameters.alphaView, "_tmp_preview_drop", false);

         if (combineHaParameters.sigma > 0.0)
            convolution(alphaView, combineHaParameters.sigma);

         dropNoise(alphaView, median, combineHaParameters.bg);

         combineHa(v, combineHaParameters.rgbView, alphaView, median);

         alphaView.window.forceClose();

         if (combineHaParameters.mask != undefined) {
            var maskedResult = clone(view, "_tmp_preview_mask", false);

            maskedResult.window.setMask(combineHaParameters.mask.window, combineHaParameters.invertMask);

            assignWithMask(maskedResult, v);
            maskedResult.window.removeMask();
            v.window.forceClose();
            v = maskedResult;
         }

         if (combineHaParameters.linearMode)
            doAutoStretch(v);
         combineHaParameters.previewImage.assign(v.image);

      }
      finally {
         v.window.forceClose();
      }
   }
}

function process() {
   if (combineHaParameters.rgbView !== undefined
       && combineHaParameters.alphaView !== undefined) {

      var finalView = combineHaParameters.rgbView;
      // create a hidden image for processing in the background
      var v = cloneBlack(finalView, "_tmp", false);

      let median = combineHaParameters.alphaView.image.median();
      var alphaView = clone(combineHaParameters.alphaView, "_tmp_preview_drop", false);

      if (combineHaParameters.sigma > 0.0)
            convolution(alphaView, combineHaParameters.sigma);

      dropNoise(alphaView, median, combineHaParameters.bg);

      combineHa(v, combineHaParameters.rgbView, alphaView, median);

      alphaView.window.forceClose();

      if (combineHaParameters.mask != undefined) {
         finalView.window.setMask(combineHaParameters.mask.window, combineHaParameters.invertMask);

         assignWithMask(finalView, v);
         finalView.window.removeMask();
      }
      else {
         finalView.beginProcess();
         finalView.image.assign(v.image);
         finalView.endProcess();
      }

      if (combineHaParameters.mask != undefined) {
         finalView.window.removeMask();
      }

      v.window.forceClose();
      finalView.window.show();
   }
}

function CombineHAlphaDialog() {
   this.__base__ = Dialog
   this.__base__();

   this.scaledMinWidth = 1280;
   this.scaledMinHeight = 800;

   let labelMinWidth = Math.round(this.font.width("Background:M"));

   this.cursor = new Cursor( StdCursor_Arrow );

   this.helpLabel = new Label( this );
   this.helpLabel.scaledMinWidth = LEFTWIDTH;
   this.helpLabel.scaledMaxWidth = LEFTWIDTH;
   this.helpLabel.frameStyle = FrameStyle_Box;
   this.helpLabel.margin = 4;
   this.helpLabel.wordWrapping = true;
   this.helpLabel.useRichText = true;
   this.helpLabel.text = "<b>" + TITLE + " v" + VERSION + "</b> &mdash;" + TEXT +"<p>Hint: Both images must be aligned using StarAlignment process!</p>";

   this.viewsFrame = new Frame(this);
   this.viewsFrame.scaledMinWidth = LEFTWIDTH;
   this.viewsFrame.scaledMaxWidth = LEFTWIDTH;
   this.viewsFrame.backgroundColor = 0xFFD8D7D3;

   // Select the RGB image to be procressed
   this.rgbViewSelectorFrame = new Frame(this);
   this.rgbViewSelectorFrame.scaledMinWidth = LEFTWIDTH2;
   this.rgbViewSelectorFrame.scaledMaxWidth = LEFTWIDTH2;
   this.rgbViewSelectorFrame.backgroundColor = 0xFFD8D7D3;
   this.rgbViewSelectLabel = new Label(this);
   this.rgbViewSelectLabel.text = "RGB View:";
   this.rgbViewSelectLabel.toolTip = "<p>Select the RGB image. H Alpha will be combined into this image!</p>";
   this.rgbViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.rgbViewSelector = new ViewList(this);
   this.rgbViewSelector.scaledMaxWidth = 250;
   this.rgbViewSelector.getMainViews();

   with(this.rgbViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         add(this.rgbViewSelectLabel);
         addSpacing(8);
         add(this.rgbViewSelector);
         adjustToContents();
      }
   }

   this.rgbViewSelector.onViewSelected = function(view) {

      this.dialog.previewTimer.stop();

      if (combineHaParameters.previewImage) {
         combineHaParameters.previewImage.free();
         combineHaParameters.previewImage = undefined;
      }

      combineHaParameters.rgbView = view;
      if (combineHaParameters.rgbView && combineHaParameters.rgbView.id) {
         var previewImage = new Image(combineHaParameters.rgbView.image);

         combineHaParameters.linearMode = combineHaParameters.rgbView.image.median() < 0.1;
         this.dialog.linkedRGBChannels.enabled = combineHaParameters.linearMode;

         if (combineHaParameters.linearMode)
            previewImage = doAutoStretchImage(previewImage, combineHaParameters.rgbView.id+"_preview");

         combineHaParameters.previewImage = previewImage;

         var metadata = {
            width: previewImage.width,
            height: previewImage.height
         }

         this.dialog.previewControl.SetPreview(previewImage, combineHaParameters.rgbView, metadata);
         this.dialog.previewControl.zoomToFit();
         this.dialog.previewControl.forceRedraw();

         this.dialog.previewTimer.previewKey = "..";
         this.dialog.previewTimer.start();
      }
   };


   this.checkboxFrame = new Frame;
   this.checkboxFrame.scaledMinWidth = LEFTWIDTH2;
   this.checkboxFrame.scaledMaxWidth = LEFTWIDTH2;
   this.checkboxFrame.sizer = new HorizontalSizer;
   this.checkboxFrame.sizer.margin = 6;
   this.checkboxFrame.sizer.spacing = 6;
   this.checkboxFrame.backgroundColor = 0xFFD8D7D3;
   this.checkboxFrame.sizer.addStretch();

   this.linearModeImage = new CheckBox(this);
   this.linearModeImage.text = "Linear Image";
   this.linearModeImage.checked = combineHaParameters.linearMode;

   this.checkboxFrame.sizer.add(this.linearModeImage);
   this.checkboxFrame.sizer.addSpacing(8);

   this.linearModeImage.onCheck = function(checked) {
      combineHaParameters.linearMode = checked;
      this.dialog.linkedRGBChannels.enabled = combineHaParameters.linearMode;
   };

   this.linkedRGBChannels = new CheckBox(this);
   this.linkedRGBChannels.text = "Link RGB Channels";
   this.linkedRGBChannels.checked = combineHaParameters.rgbLinked;

   this.checkboxFrame.sizer.add(this.linkedRGBChannels);

   this.linkedRGBChannels.onCheck = function(checked) {
      combineHaParameters.rgbLinked = checked;

      Console.writeln("changed RGB Link channels");
   };

   // Select the RGB image to be procressed
   this.hAlphaViewSelectorFrame = new Frame(this);
   this.hAlphaViewSelectorFrame.scaledMinWidth = LEFTWIDTH2;
   this.hAlphaViewSelectorFrame.scaledMaxWidth = LEFTWIDTH2;
   this.hAlphaViewSelectorFrame.backgroundColor = 0xFFD8D7D3;
   this.hAlphaViewSelectLabel = new Label(this);
   this.hAlphaViewSelectLabel.text = "H Alpha View:";
   this.hAlphaViewSelectLabel.toolTip = "<p>Select the H Alpha image to be combined.</p>";
   this.hAlphaViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.hAlphaViewSelector = new ViewList(this);
   this.hAlphaViewSelector.scaledMaxWidth = 250;
   this.hAlphaViewSelector.getMainViews();

   with(this.hAlphaViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         add(this.hAlphaViewSelectLabel);
         addSpacing(8);
         add(this.hAlphaViewSelector);
      }
   }

   this.hAlphaViewSelector.onViewSelected = function(view) {

      this.dialog.previewTimer.stop();

      combineHaParameters.alphaView = view;
      if (combineHaParameters.alphaView) {

         if (combineHaParameters.rgbView && combineHaParameters.rgbView.id) {

            var previewImage = new Image(combineHaParameters.rgbView.image);

            if (combineHaParameters.linearMode)
               previewImage = doAutoStretchImage(previewImage, combineHaParameters.rgbView.id+"_preview");
            combineHaParameters.previewImage = previewImage;

            var metadata = {
               width: previewImage.width,
               height: previewImage.height
            }

            this.dialog.previewControl.SetPreview(previewImage, combineHaParameters.rgbView, metadata);
            this.dialog.previewControl.zoomToFit();
            this.dialog.previewControl.forceRedraw();
         }

         this.dialog.previewTimer.previewKey = "..";
         this.dialog.previewTimer.start();
      }
   };

   // Select the RGB image to be procressed
   this.maskViewSelectorFrame = new Frame(this);
   this.maskViewSelectorFrame.scaledMinWidth = LEFTWIDTH2;
   this.maskViewSelectorFrame.scaledMaxWidth = LEFTWIDTH2;
   this.maskViewSelectorFrame.backgroundColor = 0xFFD8D7D3;
   this.maskViewSelectLabel = new Label(this);
   this.maskViewSelectLabel.text = "Mask:";
   this.maskViewSelectLabel.toolTip = "<p>Select a mask to protect parts of the image, i.e. a mask for stars (optional).</p>";
   this.maskViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.maskViewSelector = new ViewList(this);
   this.maskViewSelector.scaledMaxWidth = 250;
   this.maskViewSelector.getMainViews();

   this.maskViewSelector.onViewSelected = function(view) {
      this.dialog.previewTimer.stop();
      combineHaParameters.mask = view;

      this.dialog.previewTimer.previewKey = "..";
      this.dialog.previewTimer.start();
   }

   with(this.maskViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         add(this.maskViewSelectLabel);
         addSpacing(8);
         add(this.maskViewSelector);
      }
   }

   this.maskViewInvertFrame = new Frame(this);
   this.maskViewInvertFrame.scaledMinWidth = LEFTWIDTH2;
   this.maskViewInvertFrame.scaledMaxWidth = LEFTWIDTH2;
   this.maskViewInvertFrame.backgroundColor = 0xFFD8D7D3;
   this.maskViewInvertFrame.sizer = new HorizontalSizer();

   this.maskInvertCheckbox = new CheckBox(this);
   this.maskInvertCheckbox.text = "Invert Mask";
   this.maskInvertCheckbox.checked = combineHaParameters.invertMask;

   this.maskViewInvertFrame.sizer.addStretch();
   this.maskViewInvertFrame.sizer.add(this.maskInvertCheckbox);

   this.maskInvertCheckbox.onCheck = function(checked) {
      combineHaParameters.invertMask = checked;
   };


   this.parametersBar = new SectionBar(this, "Parameters");
   this.parametersBar.scaledMinWidth = LEFTWIDTH;
   this.parametersBar.scaledMaxWidth = LEFTWIDTH;
   this.parametersControl = new Control(this);
   this.parametersControl.scaledMinWidth = LEFTWIDTH;
   this.parametersControl.scaledMaxWidth = LEFTWIDTH;

   this.parametersBar.onToggleSection =  function( section, toggleBegin ) {
      if ( !toggleBegin ) {
         section.dialog.adjustToContents();
         section.dialog.setVariableSize();
      }
   }

   this.parametersBar.setSection( this.parametersControl );

   this.qToolFrame = new Frame(this);
   this.qToolFrame.scaledMinWidth = LEFTWIDTH;
   this.qToolFrame.scaledMaxWidth = LEFTWIDTH;
   this.qSlider = new NumericControl(this);
   this.qSlider.setRange(0.0, 10.0);
   this.qSlider.slider.setRange(0.0, 10000.0);
   this.qSlider.setPrecision(4);
   this.qSlider.setReal(true);
   this.qSlider.enableFixedPrecision(true);
   this.qSlider.setValue(combineHaParameters.amount);
   this.qSlider.label.text = "Amount: ";
   this.qSlider.label.minWidth = labelMinWidth;
   this.qSlider.tooltip = "<p>Increase or decrease to control the amount of H alpha to be added to the image.</p>";

   with(this.qToolFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         add(this.qSlider);
         adjustToContents();
      }
   }

   this.qSlider.onValueUpdated = function(q) {
      this.dialog.previewTimer.stop();
      combineHaParameters.amount = q;
      this.dialog.previewTimer.start();
   }

   this.betaFrame = new Frame(this);
   this.betaFrame.scaledMinWidth = LEFTWIDTH;
   this.betaFrame.scaledMaxWidth = LEFTWIDTH;
   this.betaSlider = new NumericControl(this);
   this.betaSlider.setRange(0.0, 0.5);
   this.betaSlider.slider.setRange(0.0, 100.0);
   this.betaSlider.setPrecision(4);
   this.betaSlider.setReal(true);
   this.betaSlider.enableFixedPrecision(true);
   this.betaSlider.setValue(combineHaParameters.beta);
   this.betaSlider.label.text = "Beta: ";
   this.betaSlider.label.minWidth = labelMinWidth;
   this.betaSlider.tooltip = "<p>Increase or decrease the amount of H beta (blue) to be added to the image.</p>";

   with(this.betaFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         add(this.betaSlider);
         adjustToContents();
      }
   }

   this.betaSlider.onValueUpdated = function(b) {
      this.dialog.previewTimer.stop();
      combineHaParameters.beta = b;
      this.dialog.previewTimer.start();
   }

   with(this.parametersControl) {
      sizer = new VerticalSizer();
      sizer.textAlignment = TextAlign_Left|TextAlign_VertCenter;
      backgroundColor = 0xFFD8D7D3;

      with(sizer) {
         addSpacing(12);
         add(this.qToolFrame);
         addSpacing(8);
         add(this.betaFrame);

         adjustToContents();
      }
   }

   this.bgNoiseBar = new SectionBar(this, "Remove H-Alpha Background Noise");
   this.bgNoiseBar.scaledMinWidth = LEFTWIDTH;
   this.bgNoiseBar.scaledMaxWidth = LEFTWIDTH;
   this.bgNoiseControl = new Control(this);
   this.bgNoiseControl.scaledMinWidth = LEFTWIDTH;
   this.bgNoiseControl.scaledMaxWidth = LEFTWIDTH;

   this.bgNoiseBar.onToggleSection =  function( section, toggleBegin ) {
      if ( !toggleBegin ) {
         section.dialog.adjustToContents();
         section.dialog.setVariableSize();
      }
   }

   this.bgNoiseBar.setSection( this.bgNoiseControl );

   this.bgNoiseSlider = new NumericControl(this);
   this.bgNoiseSlider.setRange(MINBGNOISE, MAXBGNOISE);
   this.bgNoiseSlider.slider.setRange(0.0, 1000000.0);
   this.bgNoiseSlider.setPrecision(6);
   this.bgNoiseSlider.enableFixedPrecision(true);
   this.bgNoiseSlider.setReal(true);

   this.bgNoiseSlider.setValue(combineHaParameters.bg);
   this.bgNoiseSlider.label.text = "Background: ";
   this.bgNoiseSlider.label.minWidth = labelMinWidth;
   this.bgNoiseSlider.tooltip = "<p>Increase or decrease the H Alpha background threshold to be added to the image.</p>";

   this.bgNoiseSliderSmall = new Slider(this);
   this.bgNoiseSliderSmall.setRange(0, 10000.0);
   this.bgNoiseSliderSmall.value = 5000.0;
   this.bgNoiseSlider.tooltip = "<p>Fine control for the H Alpha background threshold.</p>";

   this.bgNoiseFrame = new Frame(this);
   this.bgNoiseFrame.scaledMinWidth = LEFTWIDTH;
   this.bgNoiseFrame.scaledMaxWidth = LEFTWIDTH;
   this.bgNoiseFrame.sizer = new VerticalSizer();

   with(this.bgNoiseFrame.sizer) {
      addSpacing(4);

      add(this.bgNoiseSlider);
      addSpacing(8);
      add(this.bgNoiseSliderSmall);
   }

   this.bgNoiseSliderSmall.onValueUpdated = function(q) {
      this.dialog.previewTimer.stop();
      let k = (q-5000.0)/10000000.0;

      combineHaParameters.bg += k;
      if (combineHaParameters.bg<MINBGNOISE)
         combineHaParameters.bg = MINBGNOISE;
      else if (combineHaParameters.bg > MAXBGNOISE)
         combineHaParameters.bg = MAXBGNOISE;

      this.dialog.bgNoiseSlider.setValue(combineHaParameters.bg);
   }

   this.bgNoiseSliderSmall.onMouseRelease = function(q) {
      this.dialog.previewTimer.stop();
      this.dialog.bgNoiseSliderSmall.value = 5000.0;
      this.dialog.previewTimer.start();
   }

   this.bgNoiseSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      combineHaParameters.bg = t;
      this.dialog.previewTimer.start();
   };

   with(this.bgNoiseControl) {
      sizer = new VerticalSizer();
      sizer.textAlignment = TextAlign_Left|TextAlign_VertCenter;
      backgroundColor = 0xFFD8D7D3;
      with(sizer) {
         addSpacing(12);
         add(this.bgNoiseFrame);

         adjustToContents();
      }
   }

   this.convolutionBar = new SectionBar(this, "Convolution for H-Alpha");
   this.convolutionBar.scaledMinWidth = LEFTWIDTH;
   this.convolutionBar.scaledMaxWidth = LEFTWIDTH;
   this.convolutionControl = new Control(this);
   this.convolutionControl.scaledMinWidth = LEFTWIDTH;
   this.convolutionControl.scaledMaxWidth = LEFTWIDTH;

   this.convolutionBar.onToggleSection =  function( section, toggleBegin ) {
      if ( !toggleBegin ) {
         section.dialog.adjustToContents();
         section.dialog.setVariableSize();
      }
   }

   this.convolutionBar.setSection( this.convolutionControl );

   this.gconvSlider = new NumericControl(this);
   this.gconvSlider.setRange(0.0, 4.0);
   this.gconvSlider.slider.setRange(0.0, 1000.0);
   this.gconvSlider.setPrecision(4);
   this.gconvSlider.enableFixedPrecision(true);
   this.gconvSlider.setReal(true);

   this.gconvSlider.setValue(combineHaParameters.sigma);
   this.gconvSlider.label.text = "Sigma: ";
   this.gconvSlider.label.minWidth = labelMinWidth;
   this.gconvSlider.tooltip = "<p>Change the sigma parameter for the gaussian convolution to be applied to the H-Alpha image. It is highly recommended to use this with care and to consider other tools (such as NoiseXTerminator) for noise cancelation before combining the images using this script.</p>";

   this.gconvSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      combineHaParameters.sigma = t;
      this.dialog.previewTimer.start();
   }

   with(this.convolutionControl) {
      sizer = new VerticalSizer();
      sizer.textAlignment = TextAlign_Left|TextAlign_VertCenter;
      backgroundColor = 0xFFD8D7D3;

      with(sizer) {
         addSpacing(12);
         add(this.gconvSlider);

         adjustToContents();
      }
   }

   this.buttonFrame = new Frame;
   this.buttonFrame.scaledMinWidth = LEFTWIDTH;
   this.buttonFrame.scaledMaxWidth = LEFTWIDTH;
   this.buttonFrame.sizer = new HorizontalSizer;
   this.buttonFrame.sizer.margin = 6;
   this.buttonFrame.sizer.spacing = 6;


   this.ok_Button = new ToolButton( this );
   this.ok_Button.icon = this.scaledResource( ":/process-interface/execute.png" );
   this.ok_Button.setScaledFixedSize( 24, 24 );
   this.ok_Button.onClick = () => {
      this.dialog.previewTimer.stop();
      if (this.dialog.busy) return;
      this.ok();
      combineHaParameters.save();
      process();

   };

   this.cancel_Button = new ToolButton( this );

   this.cancel_Button.icon = this.scaledResource( ":/process-interface/cancel.png" );
   this.cancel_Button.setScaledFixedSize( 24, 24 );
   this.cancel_Button.onClick = () => {
      this.dialog.previewTimer.stop();
      if (this.dialog.busy) return;
      this.cancel();
   };

   this.help_Button = new ToolButton( this );
   this.help_Button.icon = this.scaledResource(":/process-interface/browse-documentation.png");
   this.help_Button.setScaledFixedSize( 24, 24 );
   this.help_Button.toolTip = "<p>Shows the script documentation.</p>";
   this.help_Button.onClick = () => {
      Dialog.browseScriptDocumentation("CombineHaWithRGB");
   };

   this.newInstanceButton = new ToolButton( this );
   this.newInstanceButton.icon = this.scaledResource( ":/process-interface/new-instance.png" );
   this.newInstanceButton.setScaledFixedSize( 24, 24 );
   this.newInstanceButton.onMousePress = () => {
      combineHaParameters.save();
      this.newInstance();
   }

   this.buttonFrame.sizer.add( this.newInstanceButton );
   this.buttonFrame.sizer.addSpacing(32);
   this.buttonFrame.sizer.add( this.ok_Button );
   this.buttonFrame.sizer.addStretch();
   this.buttonFrame.sizer.add( this.help_Button );
   this.buttonFrame.sizer.addSpacing(32);
   this.buttonFrame.sizer.add( this.cancel_Button );


   this.previewControl = new PreviewControl(this);
   this.previewControl.scaledMinWidth = 600;

   // Layout the dialog
   this.sizer = new HorizontalSizer;
   this.sizer.margin = 6;
   this.sizer.spacing = 6;

   this.leftSizer = new VerticalSizer;
   this.leftSizer.margin = 8;

   this.leftSizer.add(this.helpLabel);
   this.leftSizer.addSpacing(8);

   this.viewsFrame.sizer = new VerticalSizer();
   this.viewsFrame.sizer.margin = 8;
   this.viewsFrame.sizer.addSpacing(8);
   this.viewsFrame.sizer.add(this.rgbViewSelectorFrame);
   this.viewsFrame.sizer.add(this.checkboxFrame);
   this.viewsFrame.sizer.addSpacing(8);
   this.viewsFrame.sizer.add(this.hAlphaViewSelectorFrame);
   this.viewsFrame.sizer.addSpacing(12);
   this.viewsFrame.sizer.add(this.maskViewSelectorFrame);
   this.viewsFrame.sizer.addSpacing(12);
   this.viewsFrame.sizer.add(this.maskViewInvertFrame);
   this.viewsFrame.sizer.addSpacing(8);

   this.leftSizer.add(this.viewsFrame);
   this.leftSizer.addSpacing(8);

   this.leftSizer.add(this.parametersBar);
   this.leftSizer.add(this.parametersControl);
   this.leftSizer.addSpacing(8);
   this.leftSizer.add(this.bgNoiseBar);
   this.leftSizer.add(this.bgNoiseControl);
   this.leftSizer.addSpacing(8);
   this.leftSizer.add(this.convolutionBar);
   this.leftSizer.add(this.convolutionControl);

   this.leftSizer.addStretch();
   this.leftSizer.add(this.buttonFrame);

   this.sizer.add(this.leftSizer);
   this.sizer.add(this.previewControl);


   this.previewTimer = new Timer();
   this.previewTimer.interval = 0.50;
   this.previewTimer.periodic = true;
   this.previewTimer.dialog = this;
   this.previewTimer.busy = false;
   this.previewTimer.previewKey = "";
   this.previewTimer.updateCount = 0;

   this.previewTimer.onTimeout = function()  {
      let currentPreviewKey = getPreviewKey();
      let needsUpdate = (this.previewKey != currentPreviewKey);

      if (needsUpdate)
      {
         if (this.busy) return;

         this.stop();
         this.busy = true;
         this.dialog.ok_Button.enabled = false;
         this.dialog.cancel_Button.enabled = false;
         this.dialog.cursor = new Cursor( StdCursor_Hourglass );

         try {

            processPreview();

            if (combineHaParameters.previewImage) {
               let previewImage = combineHaParameters.previewImage;
               var metadata = {
                  width: previewImage.width,
                  height: previewImage.height
               }

               this.dialog.previewControl.SetPreview(previewImage, combineHaParameters.rgbView, metadata);
               this.dialog.previewControl.forceRedraw();
            }

         }
         finally {
            this.busy = false;
            this.start();
            this.dialog.ok_Button.enabled = true;
            this.dialog.cancel_Button.enabled = true;
            this.dialog.cursor = new Cursor( StdCursor_Arrow );
         }
      }

      this.previewKey = currentPreviewKey
   }

   this.onHide = function() {
      this.dialog.previewTimer.stop();
   }
}

CombineHAlphaDialog.prototype = new Dialog


function main() {

   // is script started from an instance in global or view target context?
   if (Parameters.isViewTarget) {
      combineHaParameters.load();
      combineHaParameters.rgbView = Parameters.targetView;
      process();
      return;
   } else if (Parameters.isGlobalTarget) {
      // then load the parameters from the instance and continue

      combineHaParameters.load();
   }

   let dialog = new CombineHAlphaDialog();
   dialog.execute();

}

main();
