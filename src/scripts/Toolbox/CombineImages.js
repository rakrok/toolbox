#include <pjsr/StdIcon.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/TextAlign.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/ColorSpace.jsh>
#include <pjsr/Interpolation.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/CheckState.jsh>
#include <pjsr/SampleType.jsh>
#include <pjsr/SectionBar.jsh>
#include "PixInsightToolsPreviewControl.jsh"

#include <pjsr/DataType.jsh>
#include <../src/scripts/AdP/WCSmetadata.jsh>

#feature-id CombineImages :  Toolbox > CombineImages

#feature-info  A script to combine two images.<br/>\
   <br/>\
   A script to combine two images or masks using different methods. \
   <br/> \
   Copyright &copy; 2024 Juergen Terpe


#define VERSION "1.7"
#define TITLE  "Combine Images"
#define TEXT   "Combines two images (or masks) using different methods."

#define LEFTWIDTH 400


var combineImagesParameters = {
   imageA: undefined,
   imageB: undefined,
   previewImage: undefined,
   replaceImageA: false,
   previewMode: 0,
   method: "Screen",
   imageAAmount: 1.0,
   imageBAmount: 1.0,
   blendAmount: 0.5,
   imageASat: 0.0,
   imageBSat: 0.0,
   linearImages: false,
   mask: undefined,
   maskAmount: 1.0,
   maskEnabled: false
}

function processMask(viewB, mask, amount) {

   if (mask != undefined) {
      var P = new PixelMath;
      if (viewB.image.isColor) {
         P.expression =  "$T[0]*"+mask.id+"*"+amount;
         P.expression1 = "$T[1]*"+mask.id+"*"+amount;
         P.expression2 = "$T[2]*"+mask.id+"*"+amount;
         P.useSingleExpression = false;
      }
      else {
         P.expression =  "$T[0]*"+mask.id+"*"+amount;
         P.useSingleExpression = true;
      }

      P.clearImageCacheAndExit = false;
      P.cacheGeneratedImages = false;
      P.generateOutput = true;
      P.singleThreaded = false;
      P.optimization = true;
      P.use64BitWorkingImage = false;
      P.rescale = false;
      P.rescaleLower = 0;
      P.rescaleUpper = 1;
      P.truncate = true;
      P.truncateLower = 0;
      P.truncateUpper = 1;
      P.createNewImage = false;
      P.executeOn(viewB);
   }

}

function combineExpression(viewA, viewB, method) {
   if (method == "Screen") {
      return "combine("+viewA.id+", "+viewB.id+", op_screen())";
   } else if (method == "Add") {
      return "combine("+viewA.id+", "+viewB.id+", op_add())";
   } else if (method == "Subtract") {
      return "combine("+viewA.id+", "+viewB.id+", op_sub())";
   } else if (method == "Divide") {
      return "combine("+viewA.id+", "+viewB.id+", op_div())";
   } else if (method == "Multiply") {
      return "combine("+viewA.id+", "+viewB.id+", op_mul())";
   } else if (method == "Difference") {
      return "combine("+viewA.id+", "+viewB.id+", op_dif())";
   } else if (method == "Maximum") {
      return "combine("+viewA.id+", "+viewB.id+", op_max())";
   } else if (method == "Minimum") {
      return "combine("+viewA.id+", "+viewB.id+", op_min())";
   } else if (method == "Exclusion") {
      return "combine("+viewA.id+", "+viewB.id+", op_exclusion())";
   } else if (method == "Hard Light") {
      return "combine("+viewA.id+", "+viewB.id+", op_hard_light())";
   } else if (method == "Linear Light") {
      return "combine("+viewA.id+", "+viewB.id+", op_linear_light())";
   } else if (method == "Color Burn") {
      return "combine("+viewA.id+", "+viewB.id+", op_color_burn())";
   } else if (method == "Color Dodge") {
      return "combine("+viewA.id+", "+viewB.id+", op_color_dodge())";
   } else if (method == "Pin Light") {
      return "combine("+viewA.id+", "+viewB.id+", op_pin_light())";
   } else if (method == "Pin Light") {
      return "combine("+viewA.id+", "+viewB.id+", op_pin_light())";
   } else if (method == "Linear Burn") {
      return "combine("+viewA.id+", "+viewB.id+", op_linear_burn())";
   } else if (method == "Overlay") {
      return "combine("+viewA.id+", "+viewB.id+", op_overlay())";
   } else if (method == "Soft Light") {
      return "combine("+viewA.id+", "+viewB.id+", op_soft_light())";
   } else if (method == "Vivid Light") {
      return "combine("+viewA.id+", "+viewB.id+", op_vivid_light())";
   } else if (method == "Exponential") {
      return "combine("+viewA.id+", "+viewB.id+", op_pow())";
   }
   return viewA.id+" + "+viewB.id;
}

function blendViews(viewA, viewB, amountA, postfix, showImage = false) {

   var P = new PixelMath;
   P.expression = amountA+"*"+viewA.id+" + "+(1.0 - amountA)+"*"+viewB.id;
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;

   if (postfix !== undefined) {
      P.newImageId = "blend_"+viewA.id+"_"+viewB.id+"_"+postfix;
      P.showNewImage = showImage;
   } else {
      P.newImageId = "blend_"+viewA.id+"_"+viewB.id;
      P.showNewImage = showImage;
   }
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(viewA);

   return View.viewById(P.newImageId);
}

function blendViewsInPlace(viewA, viewB, amountA) {

   var P = new PixelMath;
   P.expression = amountA+"*"+viewA.id+" + "+(1.0 - amountA)+"*"+viewB.id;
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;

   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(viewA);
}

function combineViews(viewA, viewB, method, postfix, showImage = false) {

   var P = new PixelMath;
   P.expression = combineExpression(viewA, viewB, method);
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;

   if (postfix !== undefined) {
      P.newImageId = "combined_"+viewA.id+"_"+viewB.id+"_"+postfix;
      P.showNewImage = showImage;
   } else {
      P.newImageId = "combined_"+viewA.id+"_"+viewB.id;
      P.showNewImage = showImage;
   }
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(viewA);

   return View.viewById(P.newImageId);
}

function combineReplaceViews(viewA, viewB, method) {

   var P = new PixelMath;
   P.expression = combineExpression(viewA, viewB, method);
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(viewA);
}



function changeAmount(view, amount) {
   var P = new PixelMath;
   P.expression = "$T*"+amount;
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;

   P.newImageId = "changed_"+view.id;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(view);

   return View.viewById(P.newImageId);
}

function changeAmountInPlace(view, amount) {
   var P = new PixelMath;
   P.expression = "$T*"+amount;
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(view);
}

function doAutoStretch(view) {
   let shadowsClipping = -2.80;
   let targetBackground = 0.2;

   var P = new HistogramTransformation;

   var median = view.image.median();//.computeOrFetchProperty( "Median" );
   var mad = view.image.MAD(median);//.computeOrFetchProperty( "MAD" );

   mad = mad * 1.4826; // coherent with a normal distribution

   var c0 = median + shadowsClipping * mad;
   var m = median;
   c0 = Math.range( c0, 0.0, 1.0 );
   m = Math.mtf( targetBackground, m - c0 );

   P.H = [ // c0, m, c1, r0, r1
      [0.00000000, 0.5, 1.00000000, 0.00000000, 1.00000000],
      [0.00000000, 0.5, 1.00000000, 0.00000000, 1.00000000],
      [0.00000000, 0.5, 1.00000000, 0.00000000, 1.00000000],
      [c0,   m, 1.00000000, 0.00000000, 1.00000000],
      [0.00000000, 0.5, 1.00000000, 0.00000000, 1.00000000]
   ];

   P.executeOn(view);
}


function improveVibrance(view, vibrance) {
   var P = new CurvesTransformation;

   P.c = [ // x, y
      [0.00000, 0.00000],
      [0.50000, 0.50000 + 0.25*vibrance],
      [1.00000, 1.00000]
   ];
   P.ct = CurvesTransformation.prototype.AkimaSubsplines;

   P.executeOn(view, false);
}



function processPreview() {
   if (combineImagesParameters.imageA !== undefined
       && combineImagesParameters.imageB !== undefined) {

      if (combineImagesParameters.method == "Blend") {
         var imageA = combineImagesParameters.imageA;
         var imageB = combineImagesParameters.imageB;

         processMask(imageB, combineImagesParameters.mask, combineImagesParameters.maskAmount);

         improveVibrance(imageA, combineImagesParameters.imageASat);
         improveVibrance(imageB, combineImagesParameters.imageBSat);

         var combinedView = blendViews(imageA, imageB, combineImagesParameters.blendAmount, "preview_temp_blend");

         if (combineImagesParameters.linearImages) {
            doAutoStretch(combinedView);
         }

         var previewImage = new Image(imageA.image);
         previewImage.assign(combinedView.image);
         if (combineImagesParameters.previewImage !== undefined) {
            combineImagesParameters.previewImage.free();
         }
         combineImagesParameters.previewImage = previewImage;

         combinedView.window.forceClose();

      } else {
         var imageA = changeAmount(combineImagesParameters.imageA, combineImagesParameters.imageAAmount);
         var imageB = changeAmount(combineImagesParameters.imageB, combineImagesParameters.imageBAmount);

         processMask(imageB, combineImagesParameters.mask, combineImagesParameters.maskAmount);

         improveVibrance(imageA, combineImagesParameters.imageASat);
         improveVibrance(imageB, combineImagesParameters.imageBSat);

         var combinedView = combineViews(imageA, imageB, combineImagesParameters.method, "preview_temp");

         if (combineImagesParameters.linearImages) {
            doAutoStretch(combinedView);
         }

         var previewImage = new Image(imageA.image);
         previewImage.assign(combinedView.image);
         if (combineImagesParameters.previewImage !== undefined) {
            combineImagesParameters.previewImage.free();
         }

         combineImagesParameters.previewImage = previewImage;

         combinedView.window.forceClose();
         imageA.window.forceClose();
         imageB.window.forceClose();
      }
   }

}

function process() {
   if (combineImagesParameters.imageA && combineImagesParameters.imageB) {

      if (combineImagesParameters.method == "Blend") {
         var imageA = combineImagesParameters.imageA;
         var imageB = combineImagesParameters.imageB;

         if (combineImagesParameters.maskEnabled)
            processMask(imageB, combineImagesParameters.mask, combineImagesParameters.maskAmount);

         improveVibrance(imageA, combineImagesParameters.imageASat);
         improveVibrance(imageB, combineImagesParameters.imageBSat);

         if (combineImagesParameters.replaceImageA) {
            blendViewsInPlace(imageA, imageB, combineImagesParameters.blendAmount);

         }
         else {
            let metadata = new ImageMetadata("CombineImages");
		      metadata.ExtractMetadata(imageA.window);

            var combinedView = blendViews(imageA, imageB, combineImagesParameters.blendAmount,
                                        undefined, true);

            combinedView.window.keywords = combineImagesParameters.imageA.window.keywords;

            if (!metadata.projection || !metadata.ref_I_G) {
               Console.writeln("The image " + combineImagesParameters.imageA.id + " has no astrometric solution");
            }
            else {
               metadata.SaveKeywords( combinedView.window, false);
			      metadata.SaveProperties( combinedView.window, TITLE + " " + VERSION);
            }
         }

      } else {
         var imageA = combineImagesParameters.imageA;
         var imageB = combineImagesParameters.imageB;

         if (combineImagesParameters.replaceImageA) {
            if (combineImagesParameters.maskEnabled)
               processMask(imageB, combineImagesParameters.mask, combineImagesParameters.maskAmount);

            changeAmountInPlace(imageA, combineImagesParameters.imageAAmount);
            changeAmountInPlace(imageB, combineImagesParameters.imageBAmount);

            improveVibrance(imageA, combineImagesParameters.imageASat);
            improveVibrance(imageB, combineImagesParameters.imageBSat);

            combineReplaceViews(imageA, imageB, combineImagesParameters.method);
         }
         else {
            let metadata = new ImageMetadata("CombineImages");
		      metadata.ExtractMetadata(combineImagesParameters.imageA.window);


            if (combineImagesParameters.maskEnabled)
               processMask(imageB, combineImagesParameters.mask, combineImagesParameters.maskAmount);

            var imageA = changeAmount(combineImagesParameters.imageA, combineImagesParameters.imageAAmount);
            var imageB = changeAmount(combineImagesParameters.imageB, combineImagesParameters.imageBAmount);

            improveVibrance(imageA, combineImagesParameters.imageASat);
            improveVibrance(imageB, combineImagesParameters.imageBSat);

            var combinedView = combineViews(imageA, imageB, combineImagesParameters.method, undefined, true);
            combinedView.window.keywords = combineImagesParameters.imageA.window.keywords;

            if (!metadata.projection || !metadata.ref_I_G) {
               Console.writeln("The image " + combineImagesParameters.imageA.id + " has no astrometric solution");
            }
            else {
               metadata.SaveKeywords( combinedView.window, false);
			      metadata.SaveProperties( combinedView.window, TITLE + " " + VERSION);
            }

            imageA.window.forceClose();
            imageB.window.forceClose();
         }
      }
   }

}

function getPreviewKey() {

   try {

      if (combineImagesParameters.imageA && combineImagesParameters.imageB) {
         let key = "ImageA: "+ combineImagesParameters.imageA
            + ", ImageB: "+ combineImagesParameters.imageB
            + ", Method: "+ combineImagesParameters.method
            + ", A-Amount: " + combineImagesParameters.imageAAmount
            + ", B-Amount: " + combineImagesParameters.imageBAmount
            + ", Mode: "+ combineImagesParameters.previewMode
            + ", BlendAmount: " + combineImagesParameters.blendAmount
            + ", A-Vibrance: " + combineImagesParameters.imageASat
            + ", B-Vibrance: " + combineImagesParameters.imageBSat
            + ", linear: " + combineImagesParameters.linearImages
            + ", mask: " + combineImagesParameters.mask
            + ", maskAmount: " + combineImagesParameters.maskAmount
            + ", maskEnabled: " + combineImagesParameters.maskEnabled;

         return key;
      } else {
         if (combineImagesParameters.imageA)
            return "No image B";

         return "No image A";
      }

   }
   catch(error) {
      Console.writeln(error);
      return "..."
   }
}

function closeView(view) {
   if (view && view.window) {
      try {
         Console.writeln("closing view: "+view.id);
         view.window.forceClose();
      }
      catch(error) {
         Console.writeln("could not close view..." + error);
      }
   }
}

function CombineImagesDialog() {
   this.__base__ = Dialog
   this.__base__();
   this.scaledMinWidth = 1280;
   this.scaledMinHeight = 800;


   let labelMinWidth = Math.round(this.font.width("Vibrance Image #2:M"));
   let sliderMinWidth = 250;


   this.helpLabel = new Label( this );
   this.helpLabel.scaledMinWidth = LEFTWIDTH;
   this.helpLabel.scaledMaxWidth = LEFTWIDTH;
   this.helpLabel.frameStyle = FrameStyle_Box;
   this.helpLabel.margin = 4;
   this.helpLabel.wordWrapping = true;
   this.helpLabel.useRichText = true;
   this.helpLabel.text = "<b>" + TITLE + " v"
                        + VERSION + "</b> &mdash; "
                        + TEXT;

   this.imageGroup = new GroupBox(this);
   this.imageGroup.title = "Images";
   this.imageGroup.scaledMinWidth = LEFTWIDTH;
   this.imageGroup.scaledMaxWidth = LEFTWIDTH;
   this.imageGroup.sizer = new VerticalSizer();

   this.imageAViewSelectorFrame = new Frame(this);
   this.imageAViewSelectorFrame.scaledMaxWidth = LEFTWIDTH;
   this.imageAViewSelectLabel = new Label(this);
   this.imageAViewSelectLabel.text = "Image #1:";
   this.imageAViewSelectLabel.toolTip = "<p>Select the first image to be combined.</p>";
   this.imageAViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.imageAViewSelector = new ViewList(this);
   this.imageAViewSelector.scaledMaxWidth = LEFTWIDTH-60;
   this.imageAViewSelector.getMainViews();

   with(this.imageAViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;
         add(this.imageAViewSelectLabel);
         addSpacing(8);
         add(this.imageAViewSelector);
         adjustToContents();
      }
   }

   this.imageAViewSelector.onViewSelected = function(view) {
      this.dialog.previewTimer.stop();
      Console.writeln("Image1 changed: "+ view.id);

      combineImagesParameters.imageA = view;

      if (combineImagesParameters.imageA) {
         if (combineImagesParameters.previewImage) {
            combineImagesParameters.previewImage.free();
         }

         combineImagesParameters.previewImage = new Image(combineImagesParameters.imageA.image);
         var previewImage = combineImagesParameters.previewImage;

         var metadata = {
            width: previewImage.width,
            height: previewImage.height
         }

         this.dialog.previewControl.SetPreview(previewImage, combineImagesParameters.imageA, metadata);
         this.dialog.previewControl.forceRedraw();
      }

      if (view && combineImagesParameters.imageB ) {
         if (view.image.width != combineImagesParameters.imageB.image.width ||
             view.image.height != combineImagesParameters.imageB.image.height) {
            combineImagesParameters.imageB = undefined;
            this.dialog.reverse_Button.enabled = false;

         } else {
            this.dialog.reverse_Button.enabled = true;
         }
      }

      this.dialog.previewTimer.previewKey = "..";
      this.dialog.previewTimer.start();
   };

   // Select the combine method....
   this.combineMethodFrame = new Frame(this);
   this.combineMethodFrame.scaledMinWidth = LEFTWIDTH;
   this.combineMethodFrame.scaledMaxWidth = LEFTWIDTH;
   this.combineMethodLabel = new Label(this);
   this.combineMethodLabel.text = "Combine Method:";
   this.combineMethodLabel.tooltip = "<p>Choose the method how to combine both images.</p>";
   this.combineMethodLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.combineMethodList = new ComboBox(this);
   this.combineMethodList.scaledMaxWidth = LEFTWIDTH-60;

   this.combineMethodList.addItem("Screen");
   this.combineMethodList.addItem("Blend");
   this.combineMethodList.addItem("Add");
   this.combineMethodList.addItem("Subtract");
   this.combineMethodList.addItem("Divide");
   this.combineMethodList.addItem("Multiply");
   this.combineMethodList.addItem("Difference");
   this.combineMethodList.addItem("Maximum");
   this.combineMethodList.addItem("Minimum");
   this.combineMethodList.addItem("Hard Light");
   this.combineMethodList.addItem("Linear Light");
   this.combineMethodList.addItem("Pin Light");
   this.combineMethodList.addItem("Color Burn");
   this.combineMethodList.addItem("Color Dodge");
   this.combineMethodList.addItem("Linear Burn");
   this.combineMethodList.addItem("Overlay");
   this.combineMethodList.addItem("Soft Light");
   this.combineMethodList.addItem("Vivid Light");
   this.combineMethodList.addItem("Exponential");

   with(this.combineMethodFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;

         add(this.combineMethodLabel);
         addSpacing(8);
         add(this.combineMethodList);
         adjustToContents();
      }
   }

   this.combineMethodList.onItemSelected = function(index) {
      this.dialog.previewTimer.stop();

      combineImagesParameters.method = this.itemText(index);
      if (combineImagesParameters.method == "Blend") {
         this.dialog.imageBLumSlider.hide();
         this.dialog.resetImageBLumButton.hide();
         this.dialog.imageALumSlider.setRange(0.0, 1.0);
         this.dialog.imageALumSlider.setValue(combineImagesParameters.blendAmount);

      } else {
         this.dialog.imageBLumSlider.show();
         this.dialog.resetImageBLumButton.show();
         this.dialog.imageALumSlider.setRange(0.0, 2.0);
         this.dialog.imageALumSlider.setValue(combineImagesParameters.imageAAmount);
      }

      this.dialog.previewTimer.previewKey = "..";
      this.dialog.previewTimer.start();
   }

   this.imageBViewSelectorFrame = new Frame(this);
   this.imageBViewSelectorFrame.scaledMinWidth = LEFTWIDTH;
   this.imageBViewSelectorFrame.scaledMaxWidth = LEFTWIDTH;
   this.imageBViewSelectLabel = new Label(this);
   this.imageBViewSelectLabel.text = "Image #2:";
   this.imageBViewSelectLabel.toolTip = "<p>Select the second image to be combined.</p>";
   this.imageBViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.imageBViewSelector = new ViewList(this);
   this.imageBViewSelector.scaledMaxWidth = LEFTWIDTH-60;
   this.imageBViewSelector.getMainViews();

   with(this.imageBViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;
         add(this.imageBViewSelectLabel);
         addSpacing(8);
         add(this.imageBViewSelector);
         adjustToContents();
      }
   }

   this.imageBViewSelector.onViewSelected = function(view) {
      this.dialog.previewTimer.stop();

      combineImagesParameters.imageB = view;

      if (combineImagesParameters.imageA) {
         if (combineImagesParameters.previewImage) {
            combineImagesParameters.previewImage.free();
         }

         combineImagesParameters.previewImage = new Image(combineImagesParameters.imageA.image);
         var previewImage = combineImagesParameters.previewImage;

         var metadata = {
            width: previewImage.width,
            height: previewImage.height
         }

         this.dialog.previewControl.SetPreview(previewImage, combineImagesParameters.imageA, metadata);
         this.dialog.previewControl.forceRedraw();
      }

      if (view && combineImagesParameters.imageA) {
         if (view.image.width != combineImagesParameters.imageA.image.width ||
             view.image.height != combineImagesParameters.imageA.image.height) {
            combineImagesParameters.imageA = undefined;
            this.dialog.reverse_Button.enabled = false;

            var mb = new MessageBox("Images of different sizes");
            mb.execute();

         } else {

            this.dialog.reverse_Button.enabled = true;
         }
      }
      this.dialog.previewTimer.previewKey = "..";
      this.dialog.previewTimer.start();
   };

   this.reverseFrame = new Frame;
   this.reverseFrame.scaledMinWidth = LEFTWIDTH;
   this.reverseFrame.scaledMaxWidth = LEFTWIDTH;

   this.reverseFrame.sizer = new HorizontalSizer();
   this.reverseFrame.sizer.margin = 6;
   this.reverseFrame.sizer.addStretch();

   this.reverse_Button = new PushButton( this );
   this.reverse_Button.text = "    Swap images    ";
   this.reverse_Button.enabled = false;

   this.reverseFrame.sizer.add(this.reverse_Button);

   this.reverse_Button.onClick = () => {
      if (combineImagesParameters.imageA !== undefined
         && combineImagesParameters.imageB !== undefined) {
         Console.writeln("Swap images");

         this.dialog.previewTimer.stop();

         var temp = combineImagesParameters.imageA;
         combineImagesParameters.imageA = combineImagesParameters.imageB;
         combineImagesParameters.imageB = temp;

         this.dialog.imageAViewSelector.currentView = combineImagesParameters.imageA;
         this.dialog.imageBViewSelector.currentView = combineImagesParameters.imageB;
         this.dialog.imageAViewSelector.update();
         this.dialog.imageBViewSelector.update();

         this.dialog.previewTimer.previewKey = "..";
         this.dialog.previewTimer.start();
      }
   };

   this.imageGroup.sizer.add(this.imageAViewSelectorFrame);
   this.imageGroup.sizer.add(this.imageBViewSelectorFrame);
   this.imageGroup.sizer.add(this.reverseFrame);


   this.balanceBar = new SectionBar(this, "Balance");
   this.balanceBar.scaledMinWidth = LEFTWIDTH;
   this.balanceBar.scaledMaxWidth = LEFTWIDTH;
   this.balanceControl = new Control(this);
   this.balanceControl.scaledMinWidth = LEFTWIDTH;
   this.balanceControl.scaledMaxWidth = LEFTWIDTH;

   this.balanceBar.onToggleSection =  function( section, toggleBegin ) {
      if ( !toggleBegin ) {
         section.dialog.adjustToContents();
         section.dialog.setVariableSize();
      }
   }

   this.imageALumSlider = new NumericControl(this);
   this.imageALumSlider.label.text = "Amount Image #1: ";
   this.imageALumSlider.label.minWidth = labelMinWidth;
   this.imageALumSlider.toolTip = "<p>Increase or decrease the amount of the image taken into account while combining the two images.</p>";
   this.imageALumSlider.setRange(0.0, 2.0);
   this.imageALumSlider.slider.setRange(0.0, 1000.0);
   this.imageALumSlider.setPrecision(3);
   this.imageALumSlider.setReal(true);
   this.imageALumSlider.setValue(combineImagesParameters.imageAAmount);

   this.resetImageALumButton = new ToolButton( this );
   this.resetImageALumButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetImageALumButton.setScaledFixedSize( 24, 24 );
   this.resetImageALumButton.toolTip = "<p>Reset the a amount of image #1 to its default.</p>";
   this.resetImageALumButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.imageALumSlider.setValue(1.0);
      combineImagesParameters.imageAAmount = 1.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.imageALumHorizontal = new HorizontalSizer();
   this.imageALumHorizontal.scaledMinWidth = LEFTWIDTH;
   this.imageALumHorizontal.scaledMaxWidth = LEFTWIDTH;
   this.imageALumHorizontal.add(this.imageALumSlider);
   this.imageALumHorizontal.add(this.resetImageALumButton);

   this.imageBLumSlider = new NumericControl(this);
   this.imageBLumSlider.label.text = "Amount Image #2: ";
   this.imageBLumSlider.label.minWidth = labelMinWidth;
   this.imageBLumSlider.toolTip = "<p>Increase or decrease the amount of the image taken into account while combining the two images.</p>";
   this.imageBLumSlider.setRange(0.0, 2.0);
   this.imageBLumSlider.slider.setRange(0.0, 1000.0);
   this.imageBLumSlider.setPrecision(3);
   this.imageBLumSlider.setReal(true);
   this.imageBLumSlider.setValue(combineImagesParameters.imageBAmount);

   this.resetImageBLumButton = new ToolButton( this );
   this.resetImageBLumButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetImageBLumButton.setScaledFixedSize( 24, 24 );
   this.resetImageBLumButton.toolTip = "<p>Reset the a amount of image #2 to its default.</p>";
   this.resetImageBLumButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.imageBLumSlider.setValue(1.0);
      combineImagesParameters.imageBAmount = 1.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.imageBLumHorizontal = new HorizontalSizer();
   this.imageBLumHorizontal.scaledMinWidth = LEFTWIDTH;
   this.imageBLumHorizontal.scaledMaxWidth = LEFTWIDTH;
   this.imageBLumHorizontal.add(this.imageBLumSlider);
   this.imageBLumHorizontal.add(this.resetImageBLumButton);

   this.imageASatSlider = new NumericControl(this);
   this.imageASatSlider.label.text = "Vibrance Image #1: ";
   this.imageASatSlider.label.minWidth = labelMinWidth;
   this.imageASatSlider.toolTip = "<p>Increase or decrease the vibrance of the image taken into account while combining the two images.</p>";
   this.imageASatSlider.setRange(-1.0, 1.0);
   this.imageASatSlider.slider.setRange(-1000.0, 1000.0);
   this.imageASatSlider.setPrecision(3);
   this.imageASatSlider.setReal(true);
   this.imageASatSlider.setValue(combineImagesParameters.imageASat);

   this.resetImageASatButton = new ToolButton( this );
   this.resetImageASatButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetImageASatButton.setScaledFixedSize( 24, 24 );
   this.resetImageASatButton.toolTip = "<p>Reset the a vibrance of image #1 to its default.</p>";
   this.resetImageASatButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.imageASatSlider.setValue(0.0);
      combineImagesParameters.imageASat = 0.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.imageASatHorizontal = new HorizontalSizer();
   this.imageASatHorizontal.scaledMinWidth = LEFTWIDTH;
   this.imageASatHorizontal.scaledMaxWidth = LEFTWIDTH;
   this.imageASatHorizontal.add(this.imageASatSlider);
   this.imageASatHorizontal.add(this.resetImageASatButton);


   this.imageBSatSlider = new NumericControl(this);
   this.imageBSatSlider.label.text = "Vibrance Image #2: ";
   this.imageBSatSlider.label.minWidth = labelMinWidth;
   this.imageBSatSlider.toolTip = "<p>Increase or decrease the vibrance of the image taken into account while combining the two images.</p>";
   this.imageBSatSlider.setRange(-1.0, 1.0);
   this.imageBSatSlider.slider.setRange(-1000.0, 1000.0);
   this.imageBSatSlider.setPrecision(3);
   this.imageBSatSlider.setReal(true);
   this.imageBSatSlider.setValue(combineImagesParameters.imageBSat);

   this.resetImageBSatButton = new ToolButton( this );
   this.resetImageBSatButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetImageBSatButton.setScaledFixedSize( 24, 24 );
   this.resetImageBSatButton.toolTip = "<p>Reset the a vibrance of image #2 to its default.</p>";
   this.resetImageBSatButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.imageBSatSlider.setValue(0.0);
      combineImagesParameters.imageBSat = 0.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.imageBSatHorizontal = new HorizontalSizer();
   this.imageBSatHorizontal.scaledMinWidth = LEFTWIDTH;
   this.imageBSatHorizontal.scaledMaxWidth = LEFTWIDTH;
   this.imageBSatHorizontal.add(this.imageBSatSlider);
   this.imageBSatHorizontal.add(this.resetImageBSatButton);

   with(this.balanceControl) {
      sizer = new VerticalSizer();
      backgroundColor = 0xFFD8D7D3;

      with(sizer) {
         addSpacing(8);
         add(this.imageALumHorizontal);
         addSpacing(2);
         add(this.imageBLumHorizontal);
         addSpacing(8);
         add(this.imageASatHorizontal);
         addSpacing(2);
         add(this.imageBSatHorizontal);
         addSpacing(8);
      }
   }

   this.imageALumSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      if (combineImagesParameters.method == "Blend") {
         combineImagesParameters.blendAmount = t;
      } else {
         combineImagesParameters.imageAAmount = t;
      }
      this.dialog.previewTimer.start();
   };

   this.imageBLumSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      combineImagesParameters.imageBAmount = t;
      this.dialog.previewTimer.start();
   };

   this.imageASatSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      combineImagesParameters.imageASat = t;
      this.dialog.previewTimer.start();
   };

   this.imageBSatSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      combineImagesParameters.imageBSat = t;
      this.dialog.previewTimer.start();
   };


   this.balanceBar.setSection( this.balanceControl );

   this.maskBar = new SectionBar(this, "Intensity Mask");
   this.maskBar.scaledMinWidth = LEFTWIDTH;
   this.maskBar.scaledMaxWidth = LEFTWIDTH;
   this.maskControl = new Control(this);
   this.maskControl.scaledMaxWidth = LEFTWIDTH;

   this.maskBar.onToggleSection =  function( section, toggleBegin ) {
      if ( !toggleBegin ) {
         section.dialog.adjustToContents();
         section.dialog.setVariableSize();
      }
   }

   this.maskSelectorFrame = new Frame(this);
   this.maskSelectorFrame.scaledMinWidth = LEFTWIDTH;
   this.maskSelectorFrame.scaledMaxWidth = LEFTWIDTH;
   this.maskSelectLabel = new Label(this);
   this.maskSelectLabel.text = "Mask (optional):";
   this.maskSelectLabel.toolTip = "<p>Select a mask to be applied for combination of images.</p>";
   this.maskSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.maskSelector = new ViewList(this);
   this.maskSelector.scaledMaxWidth = LEFTWIDTH-60;
   this.maskSelector.getMainViews();

   with(this.maskSelectorFrame) {
      sizer = new HorizontalSizer();
      sizer.scaledMinWidth = LEFTWIDTH;
      sizer.scaledMaxWidth = LEFTWIDTH;

      backgroundColor = 0xFFD8D7D3;
      with(sizer) {
         add(this.maskSelectLabel);
         addSpacing(8);
         add(this.maskSelector);
         addSpacing(12);
      }
   }

   this.maskSelector.onViewSelected = function(view) {
      this.dialog.previewTimer.stop();
      combineImagesParameters.mask = undefined;

      if (view && combineImagesParameters.imageA && view.image.isGrayscale && view.id != "") {

         if (view.image.width != combineImagesParameters.imageA.image.width ||
             view.image.height != combineImagesParameters.imageA.image.height) {

            Console.writeln("Mask not changed: "+ view.id);
            var mb = new MessageBox("Mask has a different size!");
            mb.execute();

            this.dialog.maskSelector.reload();
            this.dialog.previewTimer.start();
            return;
         }
         Console.writeln("Mask changed: "+ view.id);
         combineImagesParameters.mask = view;
      }
      else this.dialog.maskSelector.reload();
      this.dialog.previewTimer.start();
   }

   this.maskAmountSlider = new NumericControl(this);
   this.maskAmountSlider.label.text = "Mask amount:  ";
   this.maskAmountSlider.toolTip = "<p>Reduce or increase the influence of the mask.</p>";
   this.maskAmountSlider.setRange(0.0, 1.0);
   this.maskAmountSlider.slider.setRange(0.0, 1000.0);
   this.maskAmountSlider.setPrecision(3);
   this.maskAmountSlider.setReal(true);
   this.maskAmountSlider.setValue(combineImagesParameters.maskAmount);

   this.maskAmountSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      combineImagesParameters.maskAmount = t;
      this.dialog.previewTimer.start();
   };

   this.resetMaskAmountButton = new ToolButton( this );
   this.resetMaskAmountButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetMaskAmountButton.setScaledFixedSize( 24, 24 );
   this.resetMaskAmountButton.toolTip = "<p>Reset the a influence of the mask to its default.</p>";
   this.resetMaskAmountButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.maskAmountSlider.setValue(1.0);
      combineImagesParameters.maskAmount = 1.0;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.maskAmountHorizontal = new HorizontalSizer();
   this.maskAmountHorizontal.scaledMinWidth = LEFTWIDTH;
   this.maskAmountHorizontal.scaledMaxWidth = LEFTWIDTH;
   this.maskAmountHorizontal.add(this.maskAmountSlider);
   this.maskAmountHorizontal.add(this.resetMaskAmountButton);

   with(this.maskControl) {
      sizer = new VerticalSizer();
      backgroundColor = 0xFFD8D7D3;

      with(sizer) {
         margin = 6;
         addSpacing(8);
         add(this.maskSelectorFrame);
         addSpacing(8);
         add(this.maskAmountHorizontal);
      }
   }

   this.maskBar.setSection( this.maskControl );
   this.maskBar.enableCheckBox();
   this.maskBar.checkBox.checked = false;
   this.maskAmountSlider.enabled = false;
   this.resetMaskAmountButton.enabled = false;
   this.maskSelector.enabled = false;

   this.maskBar.onCheckSection = function( sectionbar )
   {
      combineImagesParameters.maskEnabled = sectionbar.checkBox.checked;
      this.dialog.maskAmountSlider.enabled = sectionbar.checkBox.checked;
      this.dialog.resetMaskAmountButton.enabled = sectionbar.checkBox.checked;
      this.dialog.maskSelector.enabled = sectionbar.checkBox.checked;
   };


   this.checkboxFrame = new Frame;
   this.checkboxFrame.scaledMinWidth = LEFTWIDTH;
   this.checkboxFrame.scaledMaxWidth = LEFTWIDTH;
   this.checkboxFrame.sizer = new HorizontalSizer;
   this.checkboxFrame.sizer.margin = 6;
   this.checkboxFrame.sizer.spacing = 6;

   this.replaceImage = new CheckBox(this);
   this.replaceImage.text = "Replace image #1";
   this.replaceImage.checked = false;

   this.linearImage = new CheckBox(this);
   this.linearImage.text = "Auto-STF preview";
   this.linearImage.checked = false;
   this.checkboxFrame.sizer.add(this.linearImage);
   this.checkboxFrame.sizer.addStretch();
   this.checkboxFrame.sizer.add(this.replaceImage);

   this.linearImage.onCheck = function(t) {
      this.dialog.previewTimer.stop();
      combineImagesParameters.linearImages = t;
      this.dialog.previewTimer.start();
   }


   this.buttonFrame = new Frame;
   this.buttonFrame.scaledMinWidth = LEFTWIDTH;
   this.buttonFrame.scaledMaxWidth = LEFTWIDTH;
   this.buttonFrame.sizer = new HorizontalSizer;
   this.buttonFrame.sizer.margin = 6;
   this.buttonFrame.sizer.spacing = 6;
   this.buttonFrame.sizer.addSpacing(8);

   this.ok_Button = new ToolButton( this );
   this.ok_Button.icon = this.scaledResource( ":/process-interface/execute.png" );
   this.ok_Button.setScaledFixedSize( 24, 24 );
   this.ok_Button.toolTip = "<p>Change the target view image using the specified parameters.</p>";
   this.ok_Button.onClick = () => {
      this.dialog.previewTimer.stop();
      if (this.dialog.busy) return;
      this.ok();
      combineImagesParameters.replaceImageA = this.replaceImage.checked;

      process();
   };

   this.cancel_Button = new ToolButton( this );
   this.cancel_Button.icon = this.scaledResource( ":/process-interface/cancel.png" );
   this.cancel_Button.setScaledFixedSize( 24, 24 );
   this.cancel_Button.toolTip = "<p>Close this dialog with no changes.</p>";
   this.cancel_Button.onClick = () => {
      this.dialog.previewTimer.stop();
      if (this.dialog.busy) return;
      this.cancel();
   };

   this.help_Button = new ToolButton( this );
   this.help_Button.icon = this.scaledResource(":/process-interface/browse-documentation.png");
   this.help_Button.setScaledFixedSize( 24, 24 );
   this.help_Button.toolTip = "<p>Shows the script documentation.</p>";
   this.help_Button.onClick = () => {
      Dialog.browseScriptDocumentation("CombineImages");
   };


   this.buttonFrame.sizer.add( this.ok_Button );
   this.buttonFrame.sizer.addSpacing(16);
   this.buttonFrame.sizer.add( this.cancel_Button );
   this.buttonFrame.sizer.addStretch();
   this.buttonFrame.sizer.add( this.help_Button );
   this.buttonFrame.sizer.addSpacing(8);

   this.previewControl = new PreviewControl(this);
   this.previewControl.scaledMinWidth = 800;
   this.previewControl.minHeight = 600;

   // layout the dialog
   this.sizerLeft = new VerticalSizer;
   this.sizerLeft.scaledMaxWidth = LEFTWIDTH;
   this.sizerLeft.scaledMinWidth = LEFTWIDTH;
   this.sizerLeft.add(this.helpLabel);
   this.sizerLeft.addSpacing(8);

   this.sizerLeft.add(this.imageGroup);

   this.sizerLeft.addSpacing(16);

   this.sizerLeft.add(this.combineMethodFrame);
   this.sizerLeft.addSpacing(8);
   this.sizerLeft.add(this.balanceBar);
   this.sizerLeft.add(this.balanceControl);
   this.sizerLeft.addSpacing(16);
   this.sizerLeft.add(this.maskBar);
   this.sizerLeft.add(this.maskControl);
   this.sizerLeft.addSpacing(16);

   this.sizerLeft.add(this.checkboxFrame);
   this.sizerLeft.addSpacing(240);
   this.sizerLeft.addStretch();
   this.sizerLeft.add(this.buttonFrame);

   this.sizer = new HorizontalSizer;
   this.sizer.margin = 8;
   this.sizer.spacing = 6;
   this.sizer.add(this.sizerLeft);
   this.sizer.add(this.previewControl);

   this.previewTimer = new Timer();
   this.previewTimer.interval = 2.00;
   this.previewTimer.periodic = true;
   this.previewTimer.dialog = this;
   this.previewTimer.busy = false;
   this.previewTimer.previewKey = "";
   this.previewTimer.updateCount = 0;

   this.previewTimer.onTimeout = function()  {
      let currentPreviewKey = getPreviewKey();
      let needsUpdate = (this.previewKey != currentPreviewKey);

      if (needsUpdate)
      {
         if (this.busy) return;

         this.dialog.imageAViewSelector.enabled = false;
         this.dialog.imageBViewSelector.enabled = false;
         this.dialog.ok_Button.enabled = false;
         this.dialog.cancel_Button.enabled = false;

         this.stop();
         this.busy = true;

         try {
            processPreview();

            if (combineImagesParameters.previewImage != undefined) {
               let previewImage = combineImagesParameters.previewImage;
               var metadata = {
                  width: previewImage.width,
                  height: previewImage.height
               }

               this.dialog.previewControl.SetPreview(previewImage, combineImagesParameters.imageA, metadata);
               this.dialog.previewControl.forceRedraw();
            }

         }
         catch(error) {
            Console.critical(error);
         }

         this.previewKey = currentPreviewKey;

         this.busy = false;
         this.dialog.imageAViewSelector.enabled = true;
         this.dialog.imageBViewSelector.enabled = true;
         this.dialog.ok_Button.enabled = true;
         this.dialog.cancel_Button.enabled = true;

         this.start();
      }


   }

   this.onHide = function() {
      this.previewTimer.stop();
      if (combineImagesParameters.previewImage !== undefined) {
         combineImagesParameters.previewImage.free();
      }
   }

   if (combineImagesParameters.imageA !== undefined) {
      this.imageAViewSelector.currentView = combineImagesParameters.imageA;
      combineImagesParameters.imageA = combineImagesParameters.imageA;

      if (combineImagesParameters.previewImage !== undefined) {
         combineImagesParameters.previewImage.free();
      }

      combineImagesParameters.previewImage = new Image(combineImagesParameters.imageA.image);
      var previewImage = combineImagesParameters.previewImage;

      var metadata = {
         width: previewImage.width,
         height: previewImage.height
      }

      this.previewControl.SetPreview(previewImage, combineImagesParameters.imageA, metadata);
      this.previewControl.zoomToFit();
      this.previewControl.forceRedraw();
   }

}

CombineImagesDialog.prototype = new Dialog


function main() {

   var window = ImageWindow.activeWindow;
   if (!window.isNull) {
      combineImagesParameters.imageA = window.mainView;
   }

   let dialog = new CombineImagesDialog();
   dialog.execute();

}

main();
