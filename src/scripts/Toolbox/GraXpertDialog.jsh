#ifndef __GRAXPERTDIALOG_JSH
#define __GRAXPERTDIALOG_JSH

#include "GraXpertLib.jsh"

#define GRAXPERT_TEXT "Remove gradients from images using GraXpert. Minimum required GraXpert release: <b>Ariel (v2.2.0)!</b><br />Install from https://www.graxpert.com"
#define GRAXPERT_FORMWIDTH 360

function GraXpertDialog(lib, params)
{
   this.__base__ = Dialog
   this.__base__();

   this.userResizable = false;
   this.scaledMinWidth = GRAXPERT_FORMWIDTH;

   this.helpLabel = new Label( this );
   this.helpLabel.frameStyle = FrameStyle_Box;
   this.helpLabel.margin = 4;
   this.helpLabel.wordWrapping = true;
   this.helpLabel.useRichText = true;
   this.helpLabel.text = "<b>" + GRAXPERT_TITLE + " Script v"
                        + GRAXPERT_VERSION + "</b> &mdash; "
                        + GRAXPERT_TEXT;

   this.imageViewSelectorFrame = new Frame(this);
   this.imageViewSelectLabel = new Label(this);
   this.imageViewSelectLabel.text = "Image:";
   this.imageViewSelectLabel.toolTip = "<p>Select the image for gradient removal.</p>";
   this.imageViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.imageViewSelector = new ViewList(this);
   this.imageViewSelector.maxWidth = GRAXPERT_FORMWIDTH;
   this.imageViewSelector.getMainViews();

   with(this.imageViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;
         add(this.imageViewSelectLabel);
         addSpacing(8);
         add(this.imageViewSelector);
         adjustToContents();
      }
   }

   this.imageViewSelector.onViewSelected = function(view) {
      params.targetView = view;
   };

   this.correctionMethodFrame = new Frame(this);
   this.correctionMethodLabel = new Label(this);
   this.correctionMethodLabel.text = "Correction:";
   this.correctionMethodLabel.tooltip = "<p>Subtract the background model to correct for additive effects, such as gradients caused by light pollution. Stronger gradients caused by things like vignetting must be corrected by division.</p>";
   this.correctionMethodLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.correctionMethodList = new ComboBox(this);
   this.correctionMethodList.addItem("Subtraction");
   this.correctionMethodList.addItem("Division");

   with(this.correctionMethodFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;

         add(this.correctionMethodLabel);
         addSpacing(8);
         add(this.correctionMethodList);
         adjustToContents();
      }
   }

   // for security reasons we check this!
   if (params.correction == undefined)
      params.correction = 0;
   this.correctionMethodList.currentItem = params.correction;

   this.correctionMethodList.onItemSelected = function(index) {
      params.correction = index;
   }

   this.smoothingSlider = new NumericControl(this);
   this.smoothingSlider.label.text = "Smoothing:";
   this.smoothingSlider.toolTip = "<p>Increase or decrease the smoothing for the GraXpert gradient removal.</p>";
   this.smoothingSlider.setRange(0.0, 1.0);
   this.smoothingSlider.slider.setRange(0.0, 1000.0);
   this.smoothingSlider.setPrecision(3);
   this.smoothingSlider.setReal(true);
   this.smoothingSlider.setValue(params.smoothing);

   this.smoothingSlider.onValueUpdated = function(t) {
      params.smoothing = t;
   }

   this.resetSmoothingButton = new ToolButton( this );
   this.resetSmoothingButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetSmoothingButton.setScaledFixedSize( 24, 24 );
   this.resetSmoothingButton.toolTip = "<p>Reset Smoothing.</p>";
   this.resetSmoothingButton.onClick = () => {
      params.smoothing = 0.0;
      this.smoothingSlider.setValue(0.0);
   }

   this.smoothingControl = new HorizontalSizer();
   this.smoothingControl.maxWidth = GRAXPERT_FORMWIDTH;
   this.smoothingControl.margin = 6;
   this.smoothingControl.add(this.smoothingSlider);
   this.smoothingControl.add(this.resetSmoothingButton);

   this.replaceTargetFrame = new Frame;
   this.replaceTargetFrame.sizer = new HorizontalSizer;
   this.replaceTargetFrame.sizer.margin = 6;
   this.replaceTargetFrame.sizer.spacing = 6;

   this.showBackgroundCheckbox = new CheckBox(this);
   this.showBackgroundCheckbox.text = "Show background model";
   this.showBackgroundCheckbox.checked = params.showBackground;
   this.showBackgroundCheckbox.toolTip = "<p>Shows the background model created by GraXpert AI. <b>This requires GraXpert version 2.0.3 or higher!</b></p>";
   this.replaceTargetFrame.sizer.add(this.showBackgroundCheckbox);

   this.showBackgroundCheckbox.onCheck = function(checked) {
      params.showBackground = checked;
   }

   this.replaceTargetFrame.sizer.addStretch();

   this.replaceTargetCheckbox = new CheckBox(this);
   this.replaceTargetCheckbox.text = "Replace the target view";
   this.replaceTargetCheckbox.checked = params.replaceTarget;
   this.replaceTargetCheckbox.toolTip = "<p>Replaces the target view with the processed image, if checked. Otherwise, a new image will be created.</p>";
   this.replaceTargetFrame.sizer.add(this.replaceTargetCheckbox);

   this.replaceTargetCheckbox.onCheck = function(checked) {
      params.replaceTarget = checked;
   }

   this.buttonFrame = new Frame;

   this.buttonFrame.sizer = new HorizontalSizer;
   this.buttonFrame.sizer.margin = 6;
   this.buttonFrame.sizer.spacing = 6;

   this.newInstanceButton = new ToolButton( this );
   this.newInstanceButton.icon = this.scaledResource( ":/process-interface/new-instance.png" );
   this.newInstanceButton.setScaledFixedSize( 24, 24 );
   this.newInstanceButton.onMousePress = () => {
      if (lib.hasGraXpertPath()) {
         lib.saveInstanceParameters();
         Console.hide();
         this.newInstance();
      }
   }

   this.ok_Button = new ToolButton( this );
   this.ok_Button.icon = this.scaledResource( ":/process-interface/execute.png" );
   this.ok_Button.setScaledFixedSize( 24, 24 );
   this.ok_Button.toolTip = "<p>Execute.</p>";
   this.ok_Button.onClick = () => {

      if (lib.hasGraXpertPath()) {

         lib.storeGraXpertParameters();

         this.ok();
         lib.process();
      } else {
         let mb = new MessageBox(
            "<p>You must setup the path to GraXpert first using the wrench icon!</p>",
            TITLE,
            StdIcon_Error,
            StdButton_Ok
         );
         mb.execute();
      }
   };

   this.cancel_Button = new ToolButton( this );
   this.cancel_Button.icon = this.scaledResource( ":/process-interface/cancel.png" );
   this.cancel_Button.setScaledFixedSize( 24, 24 );
   this.cancel_Button.toolTip = "<p>Close this dialog with no changes.</p>";
   this.cancel_Button.onClick = () => {
      this.cancel();
   };

   this.help_Button = new ToolButton( this );
   this.help_Button.icon = this.scaledResource(":/process-interface/browse-documentation.png");
   this.help_Button.setScaledFixedSize( 24, 24 );
   this.help_Button.toolTip = "<p>Shows the script documentation.</p>";
   this.help_Button.onClick = () => {
      Dialog.browseScriptDocumentation("GraXpert");
   };

   this.reset_Button = new ToolButton( this );
   this.reset_Button.icon = this.scaledResource(":/process-interface/reset.png");
   this.reset_Button.setScaledFixedSize( 24, 24 );
   this.reset_Button.toolTip = "<p>Resets all settings to their defaults.</p>";
   this.reset_Button.onClick = () => {
      params = lib.setGraXpertDefaults();
      this.smoothingSlider.setValue(params.smoothing);
      this.correctionMethodList.currentItem = params.correction;
      this.showBackgroundCheckbox.checked = params.showBackground;
      this.replaceTargetCheckbox.checked = params.replaceTarget;
   }

   this.setup_Button = new ToolButton( this );
   this.setup_Button.icon = this.scaledResource( ":/icons/wrench.png" );
   this.setup_Button.setScaledFixedSize( 24, 24 );
   this.setup_Button.toolTip = "<p>Setup the path to GraXpert.</p>";
   this.setup_Button.onClick = () => {
     lib.selectGraXpertPath();
   };

   this.buttonFrame.sizer.add(this.newInstanceButton);
   this.buttonFrame.sizer.addSpacing(8);
   this.buttonFrame.sizer.add(this.ok_Button);
   this.buttonFrame.sizer.addSpacing(8);
   this.buttonFrame.sizer.add(this.cancel_Button);
   this.buttonFrame.sizer.addSpacing(32);
   this.buttonFrame.sizer.add(this.help_Button);
   this.buttonFrame.sizer.addSpacing(16);
   this.buttonFrame.sizer.add(this.reset_Button);

   this.buttonFrame.sizer.addStretch();
   this.buttonFrame.sizer.add(this.setup_Button);

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;

   this.sizer.add(this.helpLabel);
   this.sizer.addSpacing(8);

   this.sizer.add(this.imageViewSelectorFrame);
   this.sizer.addSpacing(4);
   this.sizer.add(this.correctionMethodFrame);
   this.sizer.add(this.smoothingControl);
   this.sizer.add(this.replaceTargetFrame);
   this.sizer.addSpacing(16);

   this.sizer.add(this.buttonFrame);

   if (params.targetView !== undefined) {
       this.imageViewSelector.currentView = params.targetView;
   }
}

GraXpertDialog.prototype = new Dialog;

#endif // __GRAXPERTDIALOG_JSH
