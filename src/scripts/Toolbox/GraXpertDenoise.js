#include <pjsr/StdIcon.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/TextAlign.jsh>
#include <pjsr/ColorSpace.jsh>
#include <pjsr/Interpolation.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/CheckState.jsh>
#include <pjsr/SampleType.jsh>
#include <pjsr/DataType.jsh>
#include <../src/scripts/AdP/WCSmetadata.jsh>

#feature-id GraXpertDenoise : Toolbox > GraXpertDenoise

#feature-info  A script to run GraXpert noise reduction from within PixInsight.<br/>\
   <br/>\
   A script to run GraXpert noise reduction from within PixInsight. \
   <br/> \
   Copyright &copy; 2024 The GraXpert Team


#define VERSION "1.1"
#define TITLE  "GraXpert Denoise"
#define TEXT   "Reduce noise from images using GraXpert. Minimum required GraXpert release: <b>(v3.0.0)!</b><br />Install from https://www.graxpert.com"

#define FORMWIDTH 420

#define DEFAULT_AUTOSTRETCH_SCLIP  -2.80
#define DEFAULT_AUTOSTRETCH_TBGND   0.25


#ifeq __PI_PLATFORM__ MACOSX
#define GRAXPERTSCRPT_DIR File.homeDirectory + "/Library/Application Support/GraXpertScript"
#endif
#ifeq __PI_PLATFORM__ MSWINDOWS
#define GRAXPERTSCRPT_DIR File.homeDirectory + "/AppData/Local/GraXpertScript"
#endif
#ifeq __PI_PLATFORM__ LINUX
#define GRAXPERTSCRPT_DIR File.homeDirectory + "/.local/share/GraXpertScript"
#endif



#define SCRIPT_CONFIG GRAXPERTSCRPT_DIR + "/GraXpertDenoiseScript.json"


var graxpertParameters = {
   targetView: undefined,
   replaceTarget: true,
   denoiseStrength: 0.5,
   denoiseBatchSizePow2: 4
}

// stores the current parameters values into the script instance
function saveInstanceParameters() {
   Parameters.set("denoiseStrength", graxpertParameters.denoiseStrength);
   Parameters.set("denoiseBatchSizePow2", graxpertParameters.denoiseBatchSizePow2);
   Parameters.set("replaceTarget", graxpertParameters.replaceTarget);
}

// loads the script instance parameters
function loadInstanceParameters() {
   if (Parameters.has("denoiseStrength"))
      graxpertParameters.denoiseStrength = Parameters.getReal("denoiseStrength");
   if (Parameters.has("denoiseBatchSizePow2"))
      graxpertParameters.denoiseBatchSizePow2 = Parameters.getInteger("denoiseBatchSizePow2");
   if (Parameters.has("replaceTarget"))
      graxpertParameters.replaceTarget = Parameters.getBoolean("replaceTarget");
}


function setGraXpertDefaults() {
   graxpertParameters.denoiseStrength = 0.5;
   graxpertParameters.denoiseBatchSizePow2 = 4;
	graxpertParameters.replaceTarget = true;
   return graxpertParameters;
}

function readGraXpertParameters() {
	var params = undefined

	if (File.exists(SCRIPT_CONFIG)) {
		try {
			params = JSON.parse(File.readTextFile(SCRIPT_CONFIG));

         if ((params.replaceTarget==true || params.replaceTarget==false)
             && (params.denoiseStrength >= 0.0 && params.denoiseStrength <=1.0)) {
            Console.writeln("Reading previously used settings from " + SCRIPT_CONFIG);
         } else {
            params = setGraXpertDefaults();
         }

		} catch (error) {
			Console.warningln("Loading GraXpert script settings failed...");
			Console.warningln(error);
         params = undefined;
         try {
            File.remove(SCRIPT_CONFIG);
         }
         catch (error2) {
            Console.criticalln('Could not delete potential corrupt file (please remove manualy): ' + SCRIPT_CONFIG);
         }
		}
	}

	// set default params
	if ( params == undefined ) {
		params = setGraXpertDefaults();
	}

   graxpertParameters.replaceTarget = params.replaceTarget;
   graxpertParameters.denoiseBatchSizePow2 = params.denoiseBatchSizePow2;
   if (params.denoiseStrength != undefined)
      graxpertParameters.denoiseStrength =  params.denoiseStrength;
}

function storeReplacer(key, value) {
    if (key=="targetView") return undefined;
    else return value;
}

function storeGraXpertParameters() {
   if (!File.directoryExists(GRAXPERTSCRPT_DIR)) {
      File.createDirectory(GRAXPERTSCRPT_DIR, true);
   }
   File.writeTextFile(SCRIPT_CONFIG, JSON.stringify(graxpertParameters, storeReplacer));
}



function selectGraXpertPath() {

   let pathFile = File.homeDirectory+"/GraXpertPath.txt";

#ifeq __PI_PLATFORM__ MACOSX

   var fd = new OpenFileDialog();
   fd.caption = "Select the GraXpert app from your applications folder...";
   fd.filters = [
         ["Apps", ".app"]
      ];
   if (fd.execute()) {
      path = fd.fileName;
      Console.writeln("macOS: " + path);
      File.writeTextFile(pathFile, path);
      return path;
   }
   return undefined;
#endif
#ifeq __PI_PLATFORM__ MSWINDOWS


   let mb = new MessageBox(
               "<p>Should I try to set the path to GraXpert automatically?</p>",
               TITLE,
               StdIcon_Question,
               StdButton_Yes,
               StdButton_No
      );
   let result = mb.execute();
   if (result == StdButton_Yes) {
      let path = "graxpert";
      Console.writeln("Windows: " + path);
      File.writeTextFile(pathFile, path);
      return path;
   }
   else {
      var fd = new OpenFileDialog();
      fd.caption = "Select the GraXpert app from your programs folder...";
      fd.filters = [
            ["Programs", ".exe"]
         ];
      if (fd.execute()) {
         let path = File.unixPathToWindows(fd.fileName);
         Console.writeln("Windows: " + path);
         File.writeTextFile(pathFile, path);
         return path;
      }
   }

   return undefined;
#else
   var fd = new OpenFileDialog();
   fd.caption = "Select the GraXpert app from your applications folder...";

   if (fd.execute()) {
      let path = fd.fileName;
      Console.writeln("Linux: " + path);
      File.writeTextFile(pathFile, path);
      return path;
   }
   return undefined;
#endif
}

function hasGraXpertPath() {
   let pathFile = File.homeDirectory+"/GraXpertPath.txt";
   return File.exists(pathFile);
}


function getGraXpertPath() {

   // read text file containing file path
   let pathFile = File.homeDirectory+"/GraXpertPath.txt";
   try {
      if (File.exists(pathFile)) {
         return File.readTextFile(pathFile);
      }

      let mb = new MessageBox(
               "<p>You must setup the path to GraxPert first using the wrench icon!</p>",
               TITLE,
               StdIcon_Error,
               StdButton_Ok
      );
      mb.execute();
   }
   catch(error) {
      Console.criticalln('Could not read GraXpert path: ' + error);
      try {
         File.remove(pathFile);
      }
      catch(error2) {
      }
   }

   return undefined;
}

function executeGraXpert(cmdLine) {

   Console.writeln("running "+ cmdLine);

   let wrongVersion = false;
   let noError = true;

   this.process = new ExternalProcess;

   this.process.onStarted = function() {
      Console.noteln( 'starting GraXpert...' );
   };

   this.process.onError = function( code ) {
      Console.criticalln(' ERROR: ' + code);
   };

   this.process.onFinished = function() {
      Console.noteln( 'GraXpert finished...' );
   }

   this.process.onStandardOutputDataAvailable = function() {

      var output = String(this.stdout);
      wrongVersion = output.contains('unrecognized argument');

      if (output.contains('INFO ')) {

         if (output.contains('Using user-supplied')
             || output.contains('Available inference providers')
             || output.contains('CoreMLExecutionProvider::GetCapability')) {
            return;
         }

         // the script does intentionally not support changing the AI version for now, so this is not relevant for the user
         output = output.replace("You can overwrite this by providing the argument '-ai_version'", "");
#ifeq __PI_PLATFORM__ MACOSX
         output = output.replace("\n", "");
#endif
#ifeq __PI_PLATFORM__ MSWINDOWS
         output = output.replace("\r\n", "");
#else
         output = output.replace("\n", "");
#endif

         Console.writeln(output);
      }
      else if (output.contains('ERROR ')) {
         Console.criticalln(output);
         noError = false;
      }
      else if (output.contains('WARNING ')) {
         Console.warningln(output);
      }
      else {
         Console.writeln(output);
      }

   };

   this.process.onStandardErrorDataAvailable  = function() {
      Console.criticalln( 'GraXpert Error: ' + this.stderr.toString() );
   };

   try {

      this.process.start( cmdLine );
      for ( ; this.process.isStarting; )
         processEvents();
      for ( ; this.process.isRunning; )
         processEvents();

      if (wrongVersion) {
         Console.criticalln('ERROR: Your GraXpert version is not compatible with this script version! Please update your GraXpert version to use the new features!');

         let mb = new MessageBox(
            "<p>Your GraXpert version is not compatible with this script version! Please update your GraXpert version to use the new features!</p>",
            TITLE,
            StdIcon_Error,
            StdButton_Ok
         );
         mb.execute();

         return false;
      }

      return noError;
   }
   catch(error) {
      Console.criticalln(error);
      return false;
   }
}

function buildGraxpertCmdLine(graXpertPath, imagePath) {

#ifeq __PI_PLATFORM__ MSWINDOWS
   imagePath = File.unixPathToWindows(imagePath);
#endif

   var cmdLine = '';

   cmdLine = '"' + graXpertPath + '" -cli ' +
            '-cmd denoising ' +
            '-strength ' + graxpertParameters.denoiseStrength +
            ' -batch_size ' + graxpertParameters.denoiseBatchSizePow2 +
            ' "' +
            imagePath + '"';

   return cmdLine;
}


function assign(view, toView) {
   var P = new PixelMath;
   P.expression = view.id;
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.createNewImage = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(toView);
}

function cloneHidden(view, postfix, swapfile=true) {
   var P = new PixelMath;
   P.expression = "$T";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.truncate = true;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + postfix;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view, swapfile);

   return View.viewById(P.newImageId);
}

function closeView(view) {
   if (view && view.window) {
      try {
         view.window.forceClose();
      }
      catch(error) {
         Console.writeln("could not close view..." + error);
      }
   }
}

function copyImage(imagePath, targetView) {
   if (targetView) {
      var processedPath = imagePath;

      if (imagePath.endsWith(".xisf")) {
         processedPath = imagePath.replace(".xisf", "_GraXpert.xisf");
         if (!File.exists(processedPath))
            processedPath = imagePath.replace(".xisf", "_GraXpert.fits");
      } else {
         processedPath = imagePath.replace(".fits", "_GraXpert.fits");
      }

#ifeq __PI_PLATFORM__ MSWINDOWS
      processedPath = File.unixPathToWindows(processedPath);
#endif

      if (File.exists(processedPath)) {

         var windows = ImageWindow.open(processedPath);

         if (windows != null && windows.length > 0) {
            let processedWindow = windows[0];
            assign(processedWindow.mainView, targetView);
            processedWindow.forceClose();

            try {
               File.remove(processedPath);
            }
            catch(error) {
               Console.warningln("Could not delete file " + processedPath);
               Console.warningln(error);
            }
         }
      }
   }
}

function getTempImage(imagePath) {

   if (imagePath.endsWith(".xisf")) {
      return imagePath.replace(".xisf", "_Temp.xisf");
   } else if (imagePath.endsWith(".fits")) {
      return imagePath.replace(".fits", "_Temp.xisf");
   } else if (imagePath.endsWith(".tiff")) {
      return imagePath.replace(".tiff", "_Temp.xisf");
   } else if (imagePath.endsWith(".tif")) {
      return imagePath.replace(".tif", "_Temp.xisf");
   }
   return imagePath + "_Temp.xisf";
}

function getFileSystemSeparator() {
   return corePlatform == "Windows" ? "\\" : "\/";
}

function process() {
   if (graxpertParameters.targetView) {
      let clonedView = cloneHidden(graxpertParameters.targetView, "_GraXpert");
      let targetView = graxpertParameters.targetView;

      let metadata = new ImageMetadata("GraXpert");
		metadata.ExtractMetadata(targetView.window);

      if (!graxpertParameters.replaceTarget) {
         Console.writeln("Creating new image window...");
         targetView = clonedView;
      }

      try {
         let window = clonedView.window;

         var imagePath = File.systemTempDirectory + getFileSystemSeparator() + targetView.id + "_GraXpertImage.xisf";
         Console.writeln('Temporary image file: ' + imagePath);

         let graxpertPath = getGraXpertPath();
         if (graxpertPath != undefined) {
            let imgPath = getTempImage(imagePath);

            try {

               if (window.saveAs(imgPath, false, false, true, false)) {

                  let cmdLine = buildGraxpertCmdLine(graxpertPath, imgPath);
                  if (executeGraXpert(cmdLine)) {
                     copyImage(imgPath, targetView);
                  }
                  else {
                     if (!graxpertParameters.replaceTarget) {
                        closeView(clonedView);
                     }
                  }
               }
               else
                   Console.warningln("Could not write file " + imgPath + " required to call GraXpert!");
            }
            finally {
               try {
                  File.remove(imgPath);
               }
               catch(error) {
                  Console.warningln("Could not delete file " + processedPath);
                  Console.warningln(error);
               }
            }
         }
         else
            Console.criticalln("GraXpert path is undefined!");
      }
      finally {
         if (!graxpertParameters.replaceTarget) {

            targetView.window.keywords = graxpertParameters.targetView.window.keywords;
            if (!metadata.projection || !metadata.ref_I_G) {
               Console.writeln("The image " + targetView.id + " has no astrometric solution");
            }
            else {
               metadata.SaveKeywords( targetView.window, false);
			      metadata.SaveProperties( targetView.window, TITLE + " " + VERSION);
            }

            targetView.window.show();
         } else {
            closeView(clonedView);
         }
      }
   }
}

function getComboBoxIndex(batchSize) {
   switch(batchSize) {
      case 1: return 0;
      case 2: return 1;
      case 4: return 2;
      case 8: return 3;
      case 16: return 4;
      case 32: return 5;
   }
   return 2;
}


function GraXpertDialog() {
   this.__base__ = Dialog
   this.__base__();

   this.userResizable = false;
   this.scaledMinWidth = FORMWIDTH;

   this.helpLabel = new Label( this );
   this.helpLabel.frameStyle = FrameStyle_Box;
   this.helpLabel.margin = 4;
   this.helpLabel.wordWrapping = true;
   this.helpLabel.useRichText = true;
   this.helpLabel.text = "<b>" + TITLE + " Script v"
                        + VERSION + "</b> &mdash; "
                        + TEXT;

   this.imageViewSelectorFrame = new Frame(this);
   this.imageViewSelectLabel = new Label(this);
   this.imageViewSelectLabel.text = "Image:";
   this.imageViewSelectLabel.toolTip = "<p>Select the image to process.</p>";
   this.imageViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.imageViewSelector = new ViewList(this);
   this.imageViewSelector.maxWidth = FORMWIDTH;
   this.imageViewSelector.getMainViews();

   with(this.imageViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;
         add(this.imageViewSelectLabel);
         addSpacing(8);
         add(this.imageViewSelector);
         adjustToContents();
      }
   }

   this.imageViewSelector.onViewSelected = function(view) {
      graxpertParameters.targetView = view;
   };

   this.denoiseStrengthSlider = new NumericControl(this);
   this.denoiseStrengthSlider.label.text = "Strength:";
   this.denoiseStrengthSlider.toolTip = "<p>Increase or decrease the strength of the noise removal.</p>";
   this.denoiseStrengthSlider.setRange(0.0, 1.0);
   this.denoiseStrengthSlider.slider.setRange(0.0, 1000.0);
   this.denoiseStrengthSlider.setPrecision(3);
   this.denoiseStrengthSlider.setReal(true);
   this.denoiseStrengthSlider.setValue(graxpertParameters.denoiseStrength);

   this.denoiseStrengthSlider.onValueUpdated = function(t) {
      graxpertParameters.denoiseStrength = t;
   }

   this.resetDenoiseStrengthButton = new ToolButton( this );
   this.resetDenoiseStrengthButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetDenoiseStrengthButton.setScaledFixedSize( 24, 24 );
   this.resetDenoiseStrengthButton.toolTip = "<p>Reset denoise strength.</p>";
   this.resetDenoiseStrengthButton.onClick = () => {
      graxpertParameters.denoiseStrength = 1.0;
      this.denoiseStrengthSlider.setValue(1.0);
   }

   this.denoiseStrengthControl = new HorizontalSizer();
   this.denoiseStrengthControl.maxWidth = FORMWIDTH;
   this.denoiseStrengthControl.margin = 6;
   this.denoiseStrengthControl.add(this.denoiseStrengthSlider);
   this.denoiseStrengthControl.add(this.resetDenoiseStrengthButton);

   this.denoiseBatchLabel = new Label(this);
   this.denoiseBatchLabel.text = "Batch Size:";
   this.denoiseBatchLabel.tooltip = "<p>The Batch size.</p>";
   this.denoiseBatchLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.denoiseBatchList = new ComboBox(this);
   this.denoiseBatchList.addItem("1"); // 0
   this.denoiseBatchList.addItem("2"); // 1
   this.denoiseBatchList.addItem("4"); // 2
   this.denoiseBatchList.addItem("8"); // 3
   this.denoiseBatchList.addItem("16"); // 4
   this.denoiseBatchList.addItem("32"); // 5

   // for security reasons we check this!
   if (graxpertParameters.denoiseBatchSizePow2 == undefined)
      graxpertParameters.denoiseBatchSizePow2 = 4;
   this.denoiseBatchList.currentItem = getComboBoxIndex(graxpertParameters.denoiseBatchSizePow2);
   this.denoiseBatchList.tooltip = "<p>Increase the batch size to use more parallelism for better performance (requires better graphics hardware which supports more parallelism).</p>";

   this.denoiseBatchControl = new HorizontalSizer();
   this.denoiseBatchControl.maxWidth = FORMWIDTH;
   this.denoiseBatchControl.margin = 6;
   this.denoiseBatchControl.add(this.denoiseBatchLabel);
   this.denoiseBatchControl.addSpacing(8);
   this.denoiseBatchControl.add(this.denoiseBatchList);
   this.denoiseBatchControl.addStretch();

   this.denoiseBatchList.onItemSelected = function(index) {
      graxpertParameters.denoiseBatchSizePow2 = Math.pow(2, index);
   }

   this.replaceTargetFrame = new Frame;
   this.replaceTargetFrame.sizer = new HorizontalSizer;
   this.replaceTargetFrame.sizer.margin = 6;
   this.replaceTargetFrame.sizer.spacing = 6;

   this.replaceTargetCheckbox = new CheckBox(this);
   this.replaceTargetCheckbox.text = "Replace the target view";
   this.replaceTargetCheckbox.checked = graxpertParameters.replaceTarget;
   this.replaceTargetCheckbox.toolTip = "<p>Replaces the target view with the processed image, if checked. Otherwise, a new image will be created.</p>";
   this.replaceTargetFrame.sizer.add(this.replaceTargetCheckbox);

   this.replaceTargetCheckbox.onCheck = function(checked) {
      graxpertParameters.replaceTarget = checked;
   }

   this.buttonFrame = new Frame;

   this.buttonFrame.sizer = new HorizontalSizer;
   this.buttonFrame.sizer.margin = 6;
   this.buttonFrame.sizer.spacing = 6;

   this.newInstanceButton = new ToolButton( this );
   this.newInstanceButton.icon = this.scaledResource( ":/process-interface/new-instance.png" );
   this.newInstanceButton.setScaledFixedSize( 24, 24 );
   this.newInstanceButton.onMousePress = () => {

      if (hasGraXpertPath()) {
         saveInstanceParameters();
         Console.hide();
         this.newInstance();
      }
   }

   this.ok_Button = new ToolButton( this );
   this.ok_Button.icon = this.scaledResource( ":/process-interface/execute.png" );
   this.ok_Button.setScaledFixedSize( 24, 24 );
   this.ok_Button.toolTip = "<p>Execute.</p>";
   this.ok_Button.onClick = () => {

      if (hasGraXpertPath()) {

         storeGraXpertParameters();

         this.ok();
         process();
      } else {
         let mb = new MessageBox(
            "<p>You must setup the path to GraXpert first using the wrench icon!</p>",
            TITLE,
            StdIcon_Error,
            StdButton_Ok
         );
         mb.execute();
      }
   };

   this.cancel_Button = new ToolButton( this );
   this.cancel_Button.icon = this.scaledResource( ":/process-interface/cancel.png" );
   this.cancel_Button.setScaledFixedSize( 24, 24 );
   this.cancel_Button.toolTip = "<p>Close this dialog with no changes.</p>";
   this.cancel_Button.onClick = () => {
      this.cancel();
   };

   this.help_Button = new ToolButton( this );
   this.help_Button.icon = this.scaledResource(":/process-interface/browse-documentation.png");
   this.help_Button.setScaledFixedSize( 24, 24 );
   this.help_Button.toolTip = "<p>Shows the script documentation.</p>";
   this.help_Button.onClick = () => {
      Dialog.browseScriptDocumentation("GraXpertDenoise");
   };


   this.reset_Button = new ToolButton( this );
   this.reset_Button.icon = this.scaledResource(":/process-interface/reset.png");
   this.reset_Button.setScaledFixedSize( 24, 24 );
   this.reset_Button.toolTip = "<p>Resets all settings to their defaults.</p>";
   this.reset_Button.onClick = () => {
      graxpertParameters = setGraXpertDefaults();
      this.denoiseBatchList.currentItem = graxpertParameters.denoiseBatchSize - 1;
      this.denoiseStrengthSlider.setValue(graxpertParameters.denoiseStrength);
   }

   this.setup_Button = new ToolButton( this );
   this.setup_Button.icon = this.scaledResource( ":/icons/wrench.png" );
   this.setup_Button.setScaledFixedSize( 24, 24 );
   this.setup_Button.toolTip = "<p>Setup the path to GraXpert (v3.0 or higher).</p>";
   this.setup_Button.onClick = () => {
     selectGraXpertPath();
   };

   this.buttonFrame.sizer.add(this.newInstanceButton);
   this.buttonFrame.sizer.addSpacing(8);
   this.buttonFrame.sizer.add(this.ok_Button);
   this.buttonFrame.sizer.addSpacing(8);
   this.buttonFrame.sizer.add(this.cancel_Button);
   this.buttonFrame.sizer.addSpacing(32);

   this.buttonFrame.sizer.add(this.help_Button);
   this.buttonFrame.sizer.addSpacing(16);
   this.buttonFrame.sizer.add(this.reset_Button);

   this.buttonFrame.sizer.addStretch();
   this.buttonFrame.sizer.add(this.setup_Button);

   this.sizer = new VerticalSizer;
   this.sizer.margin = 8;

   this.sizer.add(this.helpLabel);
   this.sizer.addSpacing(8);

   this.sizer.add(this.imageViewSelectorFrame);
   this.sizer.addSpacing(12);
   this.sizer.add(this.denoiseStrengthControl);
   this.sizer.addSpacing(4);
   this.sizer.add(this.denoiseBatchControl);

   this.sizer.addSpacing(16);

   this.sizer.add(this.buttonFrame);

   if (graxpertParameters.targetView !== undefined) {
       this.imageViewSelector.currentView = graxpertParameters.targetView;
   }
}

GraXpertDialog.prototype = new Dialog

function main() {

   if (Parameters.isViewTarget) {
      loadInstanceParameters();
      graxpertParameters.targetView = Parameters.targetView;
      process();
      return;
   } else if (Parameters.isGlobalTarget) {
      loadInstanceParameters();
   }

   readGraXpertParameters();

   var window = ImageWindow.activeWindow;
   if (!window.isNull) {
      graxpertParameters.targetView = window.mainView;
   } else {
      graxpertParameters.targetView = undefined;
   }


   let dialog = new GraXpertDialog();
   dialog.execute();

}

main();
