#include <pjsr/StdIcon.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/TextAlign.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdIcon.jsh>
#include <pjsr/ColorSpace.jsh>
#include <pjsr/Interpolation.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/CheckState.jsh>
#include <pjsr/SampleType.jsh>
#include <pjsr/SectionBar.jsh>
#include "PixInsightToolsPreviewControl.jsh"


#feature-id LabColorBoost :  Toolbox > LabColorBoost

#feature-info  Boost colors in Lab mode.<br/>\
   <br/>\
   Improve colors using Lab color space. \
   <br/> \
   Copyright &copy; 2024 Juergen Terpe


#define VERSION "1.6"
#define TITLE  "Boost Colors in Lab color space"
#define TEXT   "Converts the RGB image into CIE Lab color image and increases the colors, especially the red and blue tones. This script should be used as a final processing step."

#define LEFTWIDTH 420
#define LABELWIDTH 150

#define DELTA_A 0.05
#define DELTA_B 0.05


var boostLabColorParameters = {
   imageView: undefined,
   L: undefined,
   a: undefined,
   b: undefined,
   Lab: undefined,
   previewImage: undefined,
   blend: 0.25,
   shadows: 0.1,
   middletones: 0.5,
   protectHighlights: 0.75,
   amount_a: DELTA_A,
   amount_b: DELTA_B,
   abSynchron: true,

   // stores the current parameters values into the script instance
   save: function() {
     Parameters.set("blend", boostLabColorParameters.blend);
     Parameters.set("shadows", boostLabColorParameters.shadows);
     Parameters.set("middletones", boostLabColorParameters.middletones);
     Parameters.set("protectHighlights", boostLabColorParameters.protectHighlights);
     Parameters.set("a", boostLabColorParameters.amount_a);
     Parameters.set("b", boostLabColorParameters.amount_b);
     Parameters.set("abSynchron", boostLabColorParameters.abSynchron);
   },

   // loads the script instance parameters
   load: function() {

      Console.writeln("Loading previous parameters...");
      if (Parameters.has("blend"))
         boostLabColorParameters.blend = Parameters.getReal("blend");
      if (Parameters.has("shadows"))
         boostLabColorParameters.shadows = Parameters.getReal("shadows");
      if (Parameters.has("middletones"))
         boostLabColorParameters.middletones = Parameters.getReal("middletones");
      if (Parameters.has("protectHighlights"))
         boostLabColorParameters.protectHighlights = Parameters.getReal("protectHighlights");
      if (Parameters.has("a"))
         boostLabColorParameters.amount_a = Parameters.getReal("a");
      if (Parameters.has("b"))
         boostLabColorParameters.amount_b = Parameters.getReal("b");
      if (Parameters.has("abSynchron"))
         boostLabColorParameters.abSynchron = Parameters.getReal("abSynchron");
   }
}

function getPreviewKey() {

   try {

      if (boostLabColorParameters.imageView == undefined)
         return ""
      else {
         let key = "Image: "+ boostLabColorParameters.imageView
            + ", blend: " + boostLabColorParameters.blend
            + ", shadows: " + boostLabColorParameters.shadows
            + ", midtones: " + boostLabColorParameters.middletones
            + ", protectHighlights: " + boostLabColorParameters.protectHighlights
            + ", a: " + boostLabColorParameters.amount_a
            + ", b: " + boostLabColorParameters.amount_b;

         return key;
      }
   }
   catch(error) {
      Console.writeln(error);
      return "..."
   }
}

function extractCIEa(view) {
   var P = new PixelMath;
   P.expression = "CIEa("+view.id+")";
   P.expression1 = "";
   P.expression2 = "";
   P.expression3 = "";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + "_a";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return View.viewById(P.newImageId);
}

function extractCIEb(view) {
   var P = new PixelMath;
   P.expression = "CIEb("+view.id+")";
   P.expression1 = "";
   P.expression2 = "";
   P.expression3 = "";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + "_b";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return View.viewById(P.newImageId);
}

function extractCIEL(view) {
   var P = new PixelMath;
   P.expression = "CIEL("+view.id+")";
   P.expression1 = "";
   P.expression2 = "";
   P.expression3 = "";
   P.useSingleExpression = true;
   P.symbols = "";
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id + "_L";
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.Gray;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return View.viewById(P.newImageId);
}

function cleanupLAB() {
   closeView(boostLabColorParameters.L);
   closeView(boostLabColorParameters.a);
   closeView(boostLabColorParameters.b);
   closeView(boostLabColorParameters.Lab);
   boostLabColorParameters.L = undefined;
   boostLabColorParameters.a = undefined;
   boostLabColorParameters.b = undefined;
   boostLabColorParameters.Lab = undefined;
}

function clone(view, postfix) {
   var P = new PixelMath;
   P.expression = "$T";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.rescaleLower = 0;
   P.rescaleUpper = 1;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id+postfix;
   P.newImageWidth = 0;
   P.newImageHeight = 0;
   P.newImageAlpha = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
   P.executeOn(view);

   return View.viewById(P.newImageId);
}


function closeView(view) {
   if (view !== undefined && view.window != null && !view.window.isNull) {
      try {
         view.window.forceClose();
      }
      catch(error) {
         Console.writeln("could not close view...");
      }
   }
}

function combineLab(target, L, a, b) {
   var P = new ChannelCombination;
   P.colorSpace = ChannelCombination.prototype.CIELab;
   P.channels = [ // enabled, id
      [true, L.id],
      [true, a.id],
      [true, b.id]
   ];
   P.inheritAstrometricSolution = true;
   P.executeOn(target);
}

function compressAChannel(view) {
   var P = new CurvesTransformation;
   P.K = [ // x, y
      [0.00000, 0.00000],
      [boostLabColorParameters.amount_a/1000.0, 0.00000],
      [1-boostLabColorParameters.amount_a/1000.0, 1.00000],
      [1.00000, 1.00000]
   ];
   P.Kt = CurvesTransformation.prototype.AkimaSubsplines;

   P.executeOn(view);
}

function compressBChannel(view) {
   var P = new CurvesTransformation;
   P.K = [ // x, y
      [0.00000, 0.00000],
      [boostLabColorParameters.amount_b/1000.0, 0.00000],
      [1-boostLabColorParameters.amount_b/1000.0, 1.00000],
      [1.00000, 1.00000]
   ];
   P.Kt = CurvesTransformation.prototype.AkimaSubsplines;

   P.executeOn(view);

}

function SetRGBWorkingSpace(view) {
   var P = new RGBWorkingSpace;
   P.channels = [ // Y, x, y
      [1.000000, 0.648431, 0.330856],
      [1.000000, 0.321152, 0.597871],
      [1.000000, 0.155886, 0.066044]
   ];
   P.gamma = 2.20;
   P.sRGBGamma = true;
   P.applyGlobalRGBWS = false;
   P.executeOn(view);
}

function prepare(view) {

   cleanupLAB();
   SetRGBWorkingSpace(view);

   boostLabColorParameters.L = extractCIEL(view);
   boostLabColorParameters.b = extractCIEb(view);
   boostLabColorParameters.a = extractCIEa(view);
   boostLabColorParameters.Lab = clone(view, "_LAB");

   compressAChannel(boostLabColorParameters.a);
   compressBChannel(boostLabColorParameters.b);

   combineLab(boostLabColorParameters.Lab,
            boostLabColorParameters.L,
            boostLabColorParameters.a,
            boostLabColorParameters.b);

}

function blendViews(target, viewA, viewB, t) {

   if (t > 0.0) {

      var P = new PixelMath;
      P.expression = t+"*"+viewA.id+" + "+(1.0 - t)+"*"+viewB.id;
      P.useSingleExpression = true;
      P.symbols = "";
      P.clearImageCacheAndExit = false;
      P.cacheGeneratedImages = false;
      P.generateOutput = true;
      P.singleThreaded = false;
      P.optimization = true;
      P.use64BitWorkingImage = false;
      P.rescale = false;
      P.truncate = true;
      P.createNewImage = false;
      P.newImageWidth = 0;
      P.newImageHeight = 0;
      P.newImageAlpha = false;
      P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
      P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

      P.executeOn(target);
   } else {
      var P = new PixelMath;
      P.expression = viewB.id;
      P.useSingleExpression = true;
      P.symbols = "";
      P.clearImageCacheAndExit = false;
      P.cacheGeneratedImages = false;
      P.generateOutput = true;
      P.singleThreaded = false;
      P.optimization = true;
      P.use64BitWorkingImage = false;
      P.rescale = false;
      P.truncate = true;
      P.createNewImage = false;
      P.newImageWidth = 0;
      P.newImageHeight = 0;
      P.newImageAlpha = false;
      P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
      P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

      P.executeOn(target);
   }

}

function improveMask(view) {
   Console.writeln("Shadows: "+ boostLabColorParameters.shadows);
   Console.writeln("Midtones: "+ boostLabColorParameters.middletones);

   var P = new HistogramTransformation;
   P.H = [ // c0, m, c1, r0, r1
      [0.00000000, 0.50000000, 1.00000000, 0.00000000, 1.00000000],
      [0.00000000, 0.50000000, 1.00000000, 0.00000000, 1.00000000],
      [0.00000000, 0.50000000, 1.00000000, 0.00000000, 1.00000000],
      [boostLabColorParameters.shadows, 1.0-boostLabColorParameters.middletones, 1.00000000, 0.00000000, 1.00000000],
      [0.00000000, 0.50000000, 1.00000000, 0.00000000, 1.00000000]
   ];
   P.executeOn(view);
}

function createRangeMask(view) {
   var P = new PixelMath;
   P.expression = "iif($T > "+ boostLabColorParameters.protectHighlights+ ", $T, 0.0)";
   P.useSingleExpression = true;
   P.clearImageCacheAndExit = false;
   P.cacheGeneratedImages = false;
   P.optimization = true;
   P.use64BitWorkingImage = false;
   P.rescale = false;
   P.truncate = true;
   P.createNewImage = true;
   P.showNewImage = false;
   P.newImageId = view.id+"_PH";
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(view);
   let rm = View.viewById(P.newImageId);

   var C = new Convolution;
   C.mode = Convolution.prototype.Parametric;
   C.sigma = 5.00;
   C.shape = 3.50;
   C.aspectRatio = 1.00;
   C.rescaleHighPass = false;

   C.executeOn(rm);

   return rm;
}

function subtractMasks(mask, highlights) {
   var P = new PixelMath;
   P.expression = "$T - " + highlights.id;
   P.useSingleExpression = true;
   P.cacheGeneratedImages = false;
   P.generateOutput = true;
   P.singleThreaded = false;
   P.optimization = true;
   P.truncate = true;
   P.truncateLower = 0;
   P.truncateUpper = 1;
   P.createNewImage = false;
   P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
   P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

   P.executeOn(mask);
}


function blend(target, source, lab, t, L) {

   // manipulate the mask - make it darker/brighter
   var mask = clone(L, "_mask");
   improveMask(mask);

   let highlights = createRangeMask(L);
   subtractMasks(mask, highlights);
   closeView(highlights);

   target.window.maskVisible = false;
   target.window.setMask(mask.window);

   blendViews(target, lab, source, 0.25*t);
   target.window.removeMask();

   closeView(mask);
}


function processPreview() {

   if (boostLabColorParameters.imageView !== undefined) {

      if (boostLabColorParameters.Lab == undefined)
         prepare(boostLabColorParameters.imageView);

      var preview = boostLabColorParameters.previewImage;

      preview.beginProcess( UndoFlag_NoSwapFile );
      preview.image.assign( boostLabColorParameters.imageView.image );
      preview.endProcess();

      blend(preview, boostLabColorParameters.imageView, boostLabColorParameters.Lab,
             boostLabColorParameters.blend, boostLabColorParameters.L);

   }
}

function process() {
   if (boostLabColorParameters.imageView !== undefined) {

      if (boostLabColorParameters.Lab == undefined)
         prepare(boostLabColorParameters.imageView);

      var previewImage = boostLabColorParameters.imageView;

      blend(previewImage, boostLabColorParameters.imageView, boostLabColorParameters.Lab,
             boostLabColorParameters.blend, boostLabColorParameters.L);
   }
}


function BoostLabColorDialog() {
   this.__base__ = Dialog
   this.__base__();
   this.scaledMinWidth = 1280;
   this.scaledMinHeight = 800;

   let labelMinWidth = Math.round(this.font.width("Highlights:M"));

   this.helpLabel = new Label( this );
   this.helpLabel.scaledMinWidth = LEFTWIDTH;
   this.helpLabel.scaledMaxWidth = LEFTWIDTH;
   this.helpLabel.frameStyle = FrameStyle_Box;
   this.helpLabel.margin = 4;
   this.helpLabel.wordWrapping = true;
   this.helpLabel.useRichText = true;
   this.helpLabel.text = "<b>" + TITLE + " v"
                         + VERSION + "</b> &mdash; "
                         + TEXT;

   this.imageGroup = new GroupBox(this);
   this.imageGroup.title = "Image";
   this.imageGroup.scaledMinWidth = LEFTWIDTH;
   this.imageGroup.scaledMaxWidth = LEFTWIDTH;
   this.imageGroup.sizer = new VerticalSizer();
   this.imageGroup.sizer.spacing = 0;

   // Select the image to be procressed
   this.targetViewSelectorFrame = new Frame(this);
   this.targetViewSelectorFrame.scaledMinWidth = LEFTWIDTH;
   this.targetViewSelectorFrame.scaledMaxWidth = LEFTWIDTH;
   this.targetViewSelectLabel = new Label(this);
   this.targetViewSelectLabel.text = "Target View:";
   this.targetViewSelectLabel.toolTip = "<p>Select the non-linear image to be processed.</p>";
   this.targetViewSelectLabel.textAlignment = TextAlign_Left|TextAlign_VertCenter;

   this.targetViewSelector = new ViewList(this);
   this.targetViewSelector.scaledMaxWidth = 350;
   this.targetViewSelector.getMainViews();

   with(this.targetViewSelectorFrame) {
      sizer = new HorizontalSizer();

      with(sizer) {
         margin = 6;
         add(this.targetViewSelectLabel);
         addSpacing(8);
         add(this.targetViewSelector);
         adjustToContents();
      }
   }

   this.targetViewSelector.onViewSelected = function(view) {
      this.dialog.previewTimer.stop();
      if (boostLabColorParameters.previewImage) {
         closeView(boostLabColorParameters.previewImage);
      }
      if (boostLabColorParameters.Lab) {
         closeView(boostLabColorParameters.Lab);
         boostLabColorParameters.Lab = undefined;
      }

      boostLabColorParameters.imageView = view;
      var previewImage = clone(boostLabColorParameters.imageView, "_preview");
      var metadata = {
         width: previewImage.image.width,
         height: previewImage.image.height
      }

      this.dialog.previewControl.SetPreview(previewImage.image, boostLabColorParameters.imageView, metadata);
      this.dialog.previewControl.zoomToFit();
      boostLabColorParameters.previewImage = previewImage;


      this.dialog.previewTimer.previewKey = "..";
      this.dialog.previewTimer.start();
   }


   this.imageGroup.sizer.add(this.targetViewSelectorFrame);
   this.imageGroup.sizer.addSpacing(8);

   this.blendSlider = new NumericControl(this);
   this.blendSlider.scaledMaxWidth = LEFTWIDTH-32;
   this.blendSlider.label.text = "Blend: ";
   this.blendSlider.label.minWidth = labelMinWidth;
   this.blendSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.blendSlider.toolTip = "<p>Increase/Decrease the Lab color boost effect.</p>";
   this.blendSlider.setRange(0.0, 1.0);
   this.blendSlider.slider.setRange(0.0, 100000.0);
   this.blendSlider.slider.stepSize = 1;
   this.blendSlider.setPrecision(4);
   this.blendSlider.setReal(true);
   this.blendSlider.setValue(boostLabColorParameters.blend);

   this.blendSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      boostLabColorParameters.blend = t;
      this.dialog.previewTimer.start();
   };


   this.resetBlendButton = new ToolButton( this );
   this.resetBlendButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetBlendButton.setScaledFixedSize( 24, 24 );
   this.resetBlendButton.toolTip = "<p>Reset the Lab color blend to its default.</p>";
   this.resetBlendButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.blendSlider.setValue(0.1);
      boostLabColorParameters.blend = 0.1;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.shadowsSlider = new NumericControl(this);
   this.shadowsSlider.scaledMaxWidth = LEFTWIDTH-32;
   this.shadowsSlider.label.text = "Shadows: ";
   this.shadowsSlider.label.minWidth = labelMinWidth;
   this.shadowsSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.shadowsSlider.toolTip = "<p>Increase/Decrease the shadows to be excluded from color boost effect.</p>";
   this.shadowsSlider.setRange(0.0, 1.0);
   this.shadowsSlider.slider.setRange(0.0, 100000.0);
   this.shadowsSlider.setPrecision(4);
   this.shadowsSlider.setReal(true);
   this.shadowsSlider.setValue(boostLabColorParameters.shadows);

   this.shadowsSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      boostLabColorParameters.shadows = t;
      this.dialog.previewTimer.start();
   };

   this.resetShadowsButton = new ToolButton( this );
   this.resetShadowsButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetShadowsButton.setScaledFixedSize( 24, 24 );
   this.resetShadowsButton.toolTip = "<p>Reset the shadows to its default.</p>";
   this.resetShadowsButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.shadowsSlider.setValue(0.1);
      boostLabColorParameters.shadows = 0.1;
      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.midtonesSlider = new NumericControl(this);
   this.midtonesSlider.scaledMaxWidth = LEFTWIDTH-32;
   this.midtonesSlider.label.text = "Balance: ";
   this.midtonesSlider.label.minWidth = labelMinWidth;
   this.midtonesSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.midtonesSlider.toolTip = "<p>Increase/Decrease the balance for the midtones in luminance effected by the color boost.</p>";
   this.midtonesSlider.setRange(0.2, 0.9);
   this.midtonesSlider.slider.setRange(0.0, 100000.0);
   this.midtonesSlider.setPrecision(4);
   this.midtonesSlider.setReal(true);
   this.midtonesSlider.setValue(boostLabColorParameters.middletones);

   this.midtonesSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      boostLabColorParameters.middletones = t;

      this.dialog.previewTimer.start();
   };

   this.resetMidtonesButton = new ToolButton( this );
   this.resetMidtonesButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetMidtonesButton.setScaledFixedSize( 24, 24 );
   this.resetMidtonesButton.toolTip = "<p>Reset the luminance midtones to its default.</p>";
   this.resetMidtonesButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.midtonesSlider.setValue(0.5);
      boostLabColorParameters.middletones = 0.5;

      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.protectHighlightsSlider = new NumericControl(this);
   this.protectHighlightsSlider.scaledMaxWidth = LEFTWIDTH-32;
   this.protectHighlightsSlider.label.text = "Highlights: ";
   this.protectHighlightsSlider.label.minWidth = labelMinWidth;
   this.protectHighlightsSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.protectHighlightsSlider.toolTip = "<p>Increase/Decrease to protect more or less of the highlights.</p>";
   this.protectHighlightsSlider.setRange(0.2, 0.9);
   this.protectHighlightsSlider.slider.setRange(0.0, 100000.0);
   this.protectHighlightsSlider.setPrecision(4);
   this.protectHighlightsSlider.setReal(true);
   this.protectHighlightsSlider.setValue(boostLabColorParameters.protectHighlights);

   this.protectHighlightsSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      boostLabColorParameters.protectHighlights = t;

      this.dialog.previewTimer.start();
   };

   this.resetHighlightsButton = new ToolButton( this );
   this.resetHighlightsButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetHighlightsButton.setScaledFixedSize( 24, 24 );
   this.resetHighlightsButton.toolTip = "<p>Reset the Highlights protection to its default.</p>";
   this.resetHighlightsButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.protectHighlightsSlider.setValue(0.75);
      boostLabColorParameters.protectHighlights = 0.75;

      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.rangeBar = new SectionBar(this, "a/b Range");
   this.rangeBar.scaledMinWidth = LEFTWIDTH;
   this.rangeBar.scaledMaxWidth = LEFTWIDTH;
   this.rangeControl = new Control(this);
   this.rangeControl.scaledMinWidth = LEFTWIDTH;
   this.rangeControl.scaledMaxWidth = LEFTWIDTH;
   this.rangeControl.backgroundColor = 0xFFD8D7D3;

   this.rangeBar.onToggleSection =  function( section, toggleBegin ) {
      if ( !toggleBegin ) {
         section.dialog.adjustToContents();
         section.dialog.setVariableSize();
      }
   }

   this.aSlider = new NumericControl(this);
   this.aSlider.scaledMaxWidth = LEFTWIDTH-32;
   this.aSlider.label.text = "a: ";
   this.aSlider.label.minWidth = labelMinWidth;
   this.aSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.aSlider.toolTip = "<p>Increase/Decrease a to change the amount of green/red tones.</p>";
   this.aSlider.setRange(0.000, 0.25);
   this.aSlider.slider.setRange(0.0, 100000.0);
   this.aSlider.setPrecision(4);
   this.aSlider.setReal(true);
   this.aSlider.setValue(boostLabColorParameters.amount_a);

   this.aSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      boostLabColorParameters.amount_a = t;
      if (boostLabColorParameters.Lab) {
         closeView(boostLabColorParameters.Lab);
         boostLabColorParameters.Lab = undefined;
      }
      if (boostLabColorParameters.abSynchron) {
         boostLabColorParameters.amount_b = t;
         this.dialog.bSlider.setValue(boostLabColorParameters.amount_b);
      }

      this.dialog.previewTimer.start();
   };

   this.resetAButton = new ToolButton( this );
   this.resetAButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetAButton.setScaledFixedSize( 24, 24 );
   this.resetAButton.toolTip = "<p>Reset the a amount to its default.</p>";
   this.resetAButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.aSlider.setValue(DELTA_A);
      boostLabColorParameters.amount_a = DELTA_A;
      if (boostLabColorParameters.Lab) {
         closeView(boostLabColorParameters.Lab);
         boostLabColorParameters.Lab = undefined;
      }

      if (boostLabColorParameters.abSynchron) {
         boostLabColorParameters.amount_b = boostLabColorParameters.amount_a;
         this.dialog.bSlider.setValue(boostLabColorParameters.amount_b);
      }

      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.bSlider = new NumericControl(this);
   this.bSlider.scaledMaxWidth = LEFTWIDTH-32;
   this.bSlider.label.text = "b: ";
   this.bSlider.label.minWidth = labelMinWidth;
   this.bSlider.label.textAlignment = TextAlign_Right|TextAlign_VertCenter;
   this.bSlider.toolTip = "<p>Increase/Decrease b to change the amount of yellow/blue tones.</p>";
   this.bSlider.setRange(0.000, 0.25);
   this.bSlider.slider.setRange(0.0, 100000.0);
   this.bSlider.setPrecision(4);
   this.bSlider.setReal(true);
   this.bSlider.setValue(boostLabColorParameters.amount_b);

   this.bSlider.onValueUpdated = function(t) {
      this.dialog.previewTimer.stop();
      boostLabColorParameters.amount_b = t;
      if (boostLabColorParameters.Lab) {
         closeView(boostLabColorParameters.Lab);
         boostLabColorParameters.Lab = undefined;
      }
      if (boostLabColorParameters.abSynchron) {
         boostLabColorParameters.amount_a = t;
         this.dialog.aSlider.setValue(boostLabColorParameters.amount_a);
      }

      this.dialog.previewTimer.start();
   };

   this.resetBButton = new ToolButton( this );
   this.resetBButton.icon = this.scaledResource( ":/icons/clear-inverted.png" );
   this.resetBButton.setScaledFixedSize( 24, 24 );
   this.resetBButton.toolTip = "<p>Reset the b to its default.</p>";
   this.resetBButton.onClick = () => {
      let timerWasRunning = this.dialog.previewTimer.isRunning;
      this.dialog.previewTimer.stop();
      this.dialog.bSlider.setValue(DELTA_B);
      boostLabColorParameters.amount_b = DELTA_B;
      if (boostLabColorParameters.Lab) {
         closeView(boostLabColorParameters.Lab);
         boostLabColorParameters.Lab = undefined;
      }

      if (boostLabColorParameters.abSynchron) {
         boostLabColorParameters.amount_a = boostLabColorParameters.amount_b;
         this.dialog.aSlider.setValue(boostLabColorParameters.amount_a);
      }

      if (timerWasRunning) this.dialog.previewTimer.start();
   }

   this.abSynchronCheckBox = new CheckBox(this);
   this.abSynchronCheckBox.text = "synchronous";
   this.abSynchronCheckBox.checked = boostLabColorParameters.abSynchron;
   this.abSynchronCheckBox.toolTip = "<p>Channels a and b normally should be changed by the same values!</b></p>";


   this.abSynchronCheckBox.onCheck = function(checked) {
      boostLabColorParameters.abSynchron = checked;
   }

   this.aHorizontal = new HorizontalSizer();
   this.aHorizontal.scaledMinWidth = LEFTWIDTH-12;
   this.aHorizontal.scaledMaxWidth = LEFTWIDTH-12;
   this.aHorizontal.add(this.aSlider);
   this.aHorizontal.addSpacing(8);
   this.aHorizontal.add(this.resetAButton);

   this.bHorizontal = new HorizontalSizer();
   this.bHorizontal.scaledMinWidth = LEFTWIDTH-12;
   this.bHorizontal.scaledMaxWidth = LEFTWIDTH-12;
   this.bHorizontal.add(this.bSlider);
   this.bHorizontal.addSpacing(8);
   this.bHorizontal.add(this.resetBButton);

   with(this.rangeControl) {
      sizer = new VerticalSizer();

      backgroundColor = 0xFFD8D7D3;
      with(sizer) {
         margin = 6;
         addSpacing(8);
         add(this.aHorizontal);
         addSpacing(8);
         add(this.bHorizontal);
         addSpacing(8);
         add(this.abSynchronCheckBox);
         addSpacing(8);
      }
   }

   this.rangeBar.setSection( this.rangeControl );

   this.buttonFrame = new Frame;
   this.buttonFrame.scaledMinWidth = LEFTWIDTH;
   this.buttonFrame.scaledMaxWidth = LEFTWIDTH;
   this.buttonFrame.sizer = new HorizontalSizer;
   this.buttonFrame.sizer.margin = 6;
   this.buttonFrame.sizer.spacing = 6;

   this.newInstanceButton = new ToolButton( this );
   this.newInstanceButton.icon = this.scaledResource( ":/process-interface/new-instance.png" );
   this.newInstanceButton.setScaledFixedSize( 24, 24 );
   this.newInstanceButton.onMousePress = () => {
      boostLabColorParameters.save();
      this.newInstance();
   }

   this.ok_Button = new ToolButton( this );
   this.ok_Button.icon = this.scaledResource( ":/process-interface/execute.png" );
   this.ok_Button.setScaledFixedSize( 24, 24 );
   this.ok_Button.toolTip = "<p>Change the target view image using the specified parameters.</p>";
   this.ok_Button.onClick = () => {
      this.dialog.previewTimer.stop();
      if (this.dialog.busy) return;
      process();

      boostLabColorParameters.save();

      this.ok();
   };


   this.cancel_Button = new ToolButton( this );
   this.cancel_Button.icon = this.scaledResource( ":/process-interface/cancel.png" );
   this.cancel_Button.setScaledFixedSize( 24, 24 );
   this.cancel_Button.toolTip = "<p>Close this dialog with no changes.</p>";
   this.cancel_Button.onClick = () => {
      this.dialog.previewTimer.stop();
      if (this.dialog.busy) return;
      this.cancel();
   };

   this.help_Button = new ToolButton( this );
   this.help_Button.icon = this.scaledResource(":/process-interface/browse-documentation.png");
   this.help_Button.setScaledFixedSize( 24, 24 );
   this.help_Button.toolTip = "<p>Shows the script documentation.</p>";
   this.help_Button.onClick = () => {
      Dialog.browseScriptDocumentation("LabColorBoost");
   };

   this.buttonFrame.sizer.add( this.newInstanceButton );
   this.buttonFrame.sizer.addStretch();
   this.buttonFrame.sizer.add( this.ok_Button );
   this.buttonFrame.sizer.addStretch();
   this.buttonFrame.sizer.add( this.help_Button );

   this.buttonFrame.sizer.addStretch();
   this.buttonFrame.sizer.add( this.cancel_Button );


   this.previewControl = new PreviewControl(this);
   this.previewControl.scaledMinWidth = 800;
   this.previewControl.scaledMinHeight = 600;
   this.previewControl.onCustomScope = this;

   this.sizer = new HorizontalSizer;
   this.sizer.margin = 6;
   this.sizer.spacing = 6;

   this.leftSizer = new VerticalSizer;
   this.leftSizer.add(this.helpLabel);
   this.leftSizer.addSpacing(16);
   this.leftSizer.add(this.imageGroup);
   this.leftSizer.addSpacing(16);

   this.parametersFrame = new Frame;
   this.parametersFrame.scaledMinWidth = LEFTWIDTH;
   this.parametersFrame.scaledMaxWidth = LEFTWIDTH;
   this.parametersFrame.backgroundColor = 0xFFD8D7D3;
   this.parametersFrame.sizer = new VerticalSizer;
   this.parametersFrame.sizer.addSpacing(8);

   this.leftSizer.add(this.parametersFrame);
   this.leftSizer.addSpacing(8);

   this.blendhorizontal = new HorizontalSizer();
   this.blendhorizontal.scaledMaxWidth = LEFTWIDTH;
   this.blendhorizontal.add(this.blendSlider);
   this.blendhorizontal.addSpacing(8);
   this.blendhorizontal.add(this.resetBlendButton);

   this.parametersFrame.sizer.add(this.blendhorizontal);
   this.parametersFrame.sizer.addSpacing(4);

   this.shadowshorizontal = new HorizontalSizer();
   this.shadowshorizontal.scaledMaxWidth = LEFTWIDTH;
   this.shadowshorizontal.add(this.shadowsSlider);
   this.shadowshorizontal.addSpacing(8);
   this.shadowshorizontal.add(this.resetShadowsButton);

   this.parametersFrame.sizer.add(this.shadowshorizontal);
   this.parametersFrame.sizer.addSpacing(4);

   this.midtoneshorizontal = new HorizontalSizer();
   this.midtoneshorizontal.scaledMaxWidth = LEFTWIDTH;
   this.midtoneshorizontal.add(this.midtonesSlider);
   this.midtoneshorizontal.addSpacing(8);
   this.midtoneshorizontal.add(this.resetMidtonesButton);

   this.parametersFrame.sizer.add(this.midtoneshorizontal);
   this.parametersFrame.sizer.addSpacing(4);

   this.hightlightsHorizontal = new HorizontalSizer();
   this.hightlightsHorizontal.scaledMaxWidth = LEFTWIDTH;
   this.hightlightsHorizontal.add(this.protectHighlightsSlider);
   this.hightlightsHorizontal.addSpacing(8);
   this.hightlightsHorizontal.add(this.resetHighlightsButton);

   this.parametersFrame.sizer.add(this.hightlightsHorizontal);
   this.parametersFrame.sizer.addSpacing(8);

   this.leftSizer.add(this.rangeBar);
   this.leftSizer.add(this.rangeControl);
   this.leftSizer.addSpacing(8);

   this.hintLabel = new Label( this );
   this.hintLabel.scaledMinWidth = LEFTWIDTH;
   this.hintLabel.scaledMaxWidth = LEFTWIDTH;
   this.hintLabel.margin = 4;
   this.hintLabel.wordWrapping = true;
   this.hintLabel.useRichText = true;
   this.hintLabel.text = "<b>Extracting L*a*b colors, please wait!</b>";
   this.hintLabel.textColor = 0xFF3030;
   this.hintLabel.font = new Font(this.font.family, 14);
   this.hintLabel.visible = false;

   this.leftSizer.addSpacing(32);
   this.leftSizer.add(this.hintLabel);
   this.leftSizer.addSpacing(120);

   this.leftSizer.addStretch();
   this.leftSizer.add(this.buttonFrame);

   this.sizer.add(this.leftSizer);
   this.sizer.add(this.previewControl);

   this.previewTimer = new Timer();
   this.previewTimer.interval = 0.50;
   this.previewTimer.periodic = true;
   this.previewTimer.dialog = this;
   this.previewTimer.busy = false;
   this.previewTimer.previewKey = "";
   this.previewTimer.updateCount = 0;

   this.previewTimer.onTimeout = function()  {
      let currentPreviewKey = getPreviewKey();
      let needsUpdate = (this.dialog.previewTimer.previewKey != currentPreviewKey);

      if (needsUpdate) {
         if (this.busy) return;

         this.dialog.targetViewSelector.enabled = false;
         this.dialog.ok_Button.enabled = false;
         this.dialog.cancel_Button.enabled = false;
         this.dialog.aSlider.enabled = false;
         this.dialog.bSlider.enabled = false;
         this.dialog.resetAButton.enabled = false;
         this.dialog.resetBButton.enabled = false;

         this.dialog.previewTimer.stop();
         this.dialog.previewTimer.busy = true;

         try {

            if (boostLabColorParameters.Lab == undefined)
               this.dialog.hintLabel.visible = true;

            processPreview();

            if (boostLabColorParameters.previewImage) {
               let previewImage = boostLabColorParameters.previewImage;
               var metadata = {
                  width: previewImage.image.width,
                  height: previewImage.image.height
               }

               this.dialog.previewControl.SetPreview(previewImage.image, boostLabColorParameters.imageView, metadata);
               this.dialog.previewControl.forceRedraw();
            }
         }
         catch(error) {
            Console.critical(error);
         }
         finally {
            this.dialog.previewTimer.previewKey = currentPreviewKey;

            if (boostLabColorParameters.Lab !== undefined)
               this.dialog.hintLabel.visible = false;

            this.dialog.previewTimer.busy = false;
            this.dialog.targetViewSelector.enabled = true;
            this.dialog.aSlider.enabled = true;
            this.dialog.bSlider.enabled = true;
            this.dialog.resetAButton.enabled = true;
            this.dialog.resetBButton.enabled = true;
            this.dialog.ok_Button.enabled = true;
            this.dialog.cancel_Button.enabled = true;
            this.dialog.previewTimer.start();

         }
      }

   }

   this.onHide = function() {
      this.previewTimer.stop();
      cleanupLAB();

      if (boostLabColorParameters.previewImage) {
         closeView(boostLabColorParameters.previewImage);
      }
   }

   if (boostLabColorParameters.imageView) {
      this.targetViewSelector.currentView = boostLabColorParameters.imageView;
      if (boostLabColorParameters.previewImage) {
           closeView(boostLabColorParameters.previewImage);
      }

      var previewImage = clone(boostLabColorParameters.imageView, "_preview");
      var metadata = {
         width: previewImage.image.width,
         height: previewImage.image.height
      }

      this.dialog.previewControl.SetPreview(previewImage.image, boostLabColorParameters.imageView, metadata);
      this.dialog.previewControl.zoomToFit();
      boostLabColorParameters.previewImage = previewImage;

      this.previewTimer.previewKey = "..";
      this.previewTimer.start();
   }


}

BoostLabColorDialog.prototype = new Dialog


function main() {


   // is script started from an instance in global or view target context?
   if (Parameters.isViewTarget) {
      boostLabColorParameters.load();
      boostLabColorParameters.imageView = Parameters.targetView;
      process();
      return;
   } else if (Parameters.isGlobalTarget) {
      // then load the parameters from the instance and continue

      boostLabColorParameters.load();
   }

   var window = ImageWindow.activeWindow;
   if (!window.isNull) {
      boostLabColorParameters.imageView = window.mainView;
   }

   let dialog = new BoostLabColorDialog();
   dialog.execute();

}

main();

