/*
 * Preview Control
 *
 * This file was taken from the AnnotateImage script
 *
 * Copyright (C) 2013-2020, Andres del Pozo
 * Contributions (C) 2019-2020, Juan Conejero (PTeam)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

// slightly modified to fit the needs of the PixInsight Tools

#feature-info Preview Control for several scripts.<br/>\
   <br/>\
   Preview Control. \
   <br/> \
   Copyright &copy; 2023 Juergen Terpe

#include <pjsr/ButtonCodes.jsh>
#include <pjsr/StdCursor.jsh>
#include <pjsr/CompositionOp.jsh>

function PreviewControl(parent) {
   this.__base__ = Frame;
   this.__base__(parent);

   this.referenceView = undefined;
   this.NoImage = true;
   this.previewImage = undefined;
   this.bitmap = undefined;
   this.metadata = undefined;
   this.dragging = false;
   this.zooming = false;
   this.mousePressed = false;
   this.dragFrom = new Point();
   this.dragTo = new Point();

   this.onCustomPaintScope = undefined;
   this.onCustomPaint = undefined;

   this.SetPreview = function(image, view, metadata) {
      this.previewImage = image;
      let bmp = image.render();
      this.referenceView = view;
      view.window.applyColorTransformation(bmp);
      this.bitmap = bmp;
      this.metadata = metadata;
      this.NoImage = false;
      this.scaledImage = null;
      this.SetZoomOutLimit();
      this.UpdateZoom(this.zoom);

   };

   this.scaleImage = function(scale) {
      if (this.previewImage !== undefined) {
         let imageScaleable = new Image(this.previewImage);
         imageScaleable.resample(scale);
         return imageScaleable.render(1, false, false);
      }
      return null;
   }

   this.UpdateZoom = function (newZoom, refPoint) {
        newZoom = Math.max(this.zoomOutLimit, Math.min(4, newZoom));
        if (newZoom == this.zoom && this.scaledImage)
            return;

        if (refPoint == null)
            refPoint = new Point(this.scrollbox.viewport.width / 2, this.scrollbox.viewport.height / 2);

        let imgx = null;
        if (this.scrollbox.maxHorizontalScrollPosition > 0)
            imgx = (refPoint.x + this.scrollbox.horizontalScrollPosition) / this.scale;

        let imgy = null;
        if (this.scrollbox.maxVerticalScrollPosition > 0)
            imgy = (refPoint.y + this.scrollbox.verticalScrollPosition) / this.scale;

        this.zoom = newZoom;
        this.scaledImage = null;
        gc(true);

        if (this.zoom > 0) {
            this.scale = this.zoom;
            this.zoomVal_Label.text = format("%d:1", this.zoom);
        } else {
            this.scale = 1 / (-this.zoom + 2);
            this.zoomVal_Label.text = format("1:%d", -this.zoom + 2);
        }

        if (this.bitmap) {
            this.scaledImage = this.scaleImage(this.scale);
            if (this.referenceView)
               this.referenceView.window.applyColorTransformation(this.scaledImage);
        }
        else
            this.scaledImage = {
                width: this.metadata.width * this.scale,
                height: this.metadata.height * this.scale
            };

        this.scrollbox.maxHorizontalScrollPosition = Math.max(0, this.scaledImage.width - this.scrollbox.viewport.width);
        this.scrollbox.maxVerticalScrollPosition = Math.max(0, this.scaledImage.height - this.scrollbox.viewport.height);

        if (this.scrollbox.maxHorizontalScrollPosition > 0 && imgx != null)
            this.scrollbox.horizontalScrollPosition = imgx * this.scale - refPoint.x;
        if (this.scrollbox.maxVerticalScrollPosition > 0 && imgy != null)
            this.scrollbox.verticalScrollPosition = imgy * this.scale - refPoint.y;

        this.scrollbox.viewport.update();
    };

    this.zoomIn_Button = new ToolButton(this);
    this.zoomIn_Button.icon = this.scaledResource(":/icons/zoom-in.png");
    this.zoomIn_Button.setScaledFixedSize(24, 24);
    this.zoomIn_Button.toolTip = "Zoom In";
    this.zoomIn_Button.onMousePress = function () {
        this.parent.UpdateZoom(this.parent.zoom + 1);
    };

    this.zoomOut_Button = new ToolButton(this);
    this.zoomOut_Button.icon = this.scaledResource(":/icons/zoom-out.png");
    this.zoomOut_Button.setScaledFixedSize(24, 24);
    this.zoomOut_Button.toolTip = "Zoom Out";
    this.zoomOut_Button.onMousePress = function () {
        this.parent.UpdateZoom(this.parent.zoom - 1);
    };

    this.zoom11_Button = new ToolButton(this);
    this.zoom11_Button.icon = this.scaledResource(":/icons/zoom-1-1.png");
    this.zoom11_Button.setScaledFixedSize(24, 24);
    this.zoom11_Button.toolTip = "Zoom 1:1";
    this.zoom11_Button.onMousePress = function () {
        this.parent.UpdateZoom(1);
    };

    this.buttons_Sizer = new HorizontalSizer;
    this.buttons_Sizer.margin = 4;
    this.buttons_Sizer.spacing = 4;
    this.buttons_Sizer.add(this.zoomIn_Button);
    this.buttons_Sizer.add(this.zoomOut_Button);
    this.buttons_Sizer.add(this.zoom11_Button);
    this.buttons_Sizer.addStretch();

    this.setScaledMinSize(640, 480);
    this.zoom = 1;
    this.scale = 1;
    this.zoomOutLimit = -5;
    this.scrollbox = new ScrollBox(this);
    this.scrollbox.autoScroll = true;
    this.scrollbox.tracking = true;
    this.scrollbox.cursor = new Cursor(StdCursor_Cross);

    this.scroll_Sizer = new HorizontalSizer;
    this.scroll_Sizer.add(this.scrollbox);

    this.SetZoomOutLimit = function () {
       if (this.metadata != null) {

           let scaleX = Math.ceil(this.metadata.width / this.scrollbox.viewport.width);
           let scaleY = Math.ceil(this.metadata.height / this.scrollbox.viewport.height);
           let scale = Math.max(scaleX, scaleY);
           this.zoomOutLimit = -scale + 2;
       }
    };



    this.SetZoomRect = function(rect) {
       let scaleX = Math.ceil(rect.width / this.scrollbox.viewport.width);
       let scaleY = Math.ceil(rect.height / this.scrollbox.viewport.height);
       let scale = Math.max(scaleX, scaleY);
       let c = rect.center;

       let ox = (this.scrollbox.maxHorizontalScrollPosition > 0) ?
            -this.scrollbox.horizontalScrollPosition : (this.width - this.scaledImage.width) / 2;
       let oy = (this.scrollbox.maxVerticalScrollPosition > 0) ?
            -this.scrollbox.verticalScrollPosition : (this.height - this.scaledImage.height) / 2;
       let center = new Point((c.x - ox) / this.scale, (c.y - oy) / this.scale);


       let newZoom = Math.max(this.zoomOutLimit, Math.min(4, scale));

       this.zoom = newZoom;
       this.scaledImage = null;
       gc(true);

       if (this.zoom > 0) {
            this.scale = this.zoom;
            this.zoomVal_Label.text = format("%d:1", this.zoom);
        } else {
            this.scale = 1 / (-this.zoom + 2);
            this.zoomVal_Label.text = format("1:%d", -this.zoom + 2);
        }

        if (this.bitmap) {
            this.scaledImage = this.scaleImage(this.scale);
            if (this.referenceView)
               this.referenceView.window.applyColorTransformation(this.scaledImage);
        }
        else
            this.scaledImage = {
                width: this.metadata.width * this.scale,
                height: this.metadata.height * this.scale
            };


        this.scrollbox.maxHorizontalScrollPosition = Math.max(0, this.scaledImage.width - this.scrollbox.viewport.width);
        this.scrollbox.maxVerticalScrollPosition = Math.max(0, this.scaledImage.height - this.scrollbox.viewport.height);

        let offsetX = (this.scrollbox.maxHorizontalScrollPosition > 0) ?
                  -this.scrollbox.horizontalScrollPosition : (this.scrollbox.viewport.width - this.scaledImage.width) / 2;
        let offsetY = (this.scrollbox.maxVerticalScrollPosition > 0) ?
                  -this.scrollbox.verticalScrollPosition : (this.scrollbox.viewport.height - this.scaledImage.height) / 2;

        this.scrollbox.horizontalScrollPosition = offsetX + center.x * this.scale - this.scrollbox.viewport.width/2;
        this.scrollbox.verticalScrollPosition = offsetY + center.y*this.scale - this.scrollbox.viewport.height/2;

        this.scrollbox.viewport.update();
    };

    this.scrollbox.onHorizontalScrollPosUpdated = function (newPos) {
        this.viewport.update();
    };

    this.scrollbox.onVerticalScrollPosUpdated = function (newPos) {
        this.viewport.update();
    };

    this.forceRedraw = function () {
        this.scrollbox.viewport.update();
    };

    this.zoomToFit = function() {
        this.UpdateZoom(this.zoomOutLimit);
    };

    this.getDragRect = function() {
         let dX0 = Math.min(this.dragFrom.x, this.dragTo.x);
         let dY0 = Math.min(this.dragFrom.y, this.dragTo.y);
         let dX1 = Math.max(this.dragFrom.x, this.dragTo.x);
         let dY1 = Math.max(this.dragFrom.y, this.dragTo.y);

         let x0 = dX0;
         let x1 = dX1;
         let y0 = dY0;
         let y1 = dY1;

         return new Rect(x0, y0, x1, y1);
    };

    this.scrollbox.viewport.onMousePress = function (x, y, button, buttonState, modifiers) {
        let preview = this.parent.parent;

        if (buttonState == MouseButton_Right) {
            preview.zoomToFit();
            preview.mousePressed = false;
            preview.scrolling  = null;
            preview.scrollbox.cursor = new Cursor(StdCursor_Cross);
            return;
        }

        if (modifiers == KeyModifier_Shift) {
            if (preview.zoom <= 0) {
               preview.mousePressed = true;
               preview.dragFrom = new Point(x, y);
               preview.dragTo = new Point(x, y);
               preview.scrollbox.cursor = new Cursor(StdCursor_Cross);
            }

        }
        else {
            preview.scrollbox.cursor = new Cursor(StdCursor_ClosedHand);

            preview.scrolling = {
               orgCursor: new Point(x, y),
               orgScroll: new Point(preview.scrollbox.horizontalScrollPosition, preview.scrollbox.verticalScrollPosition)
            };
        }

    };

    this.scrollbox.viewport.onMouseMove = function (x, y, buttonState, modifiers) {

       let preview = this.parent.parent;

       if (preview.mousePressed) {
           preview.dragTo = new Point(x, y);
           preview.zooming = true;

           this.update();
           return;
       }

       if (preview.scrolling) {
            preview.scrollbox.horizontalScrollPosition = preview.scrolling.orgScroll.x - (x - preview.scrolling.orgCursor.x);
            preview.scrollbox.verticalScrollPosition = preview.scrolling.orgScroll.y - (y - preview.scrolling.orgCursor.y);

            preview.scrollbox.cursor = new Cursor(StdCursor_ClosedHand);
       }
    };

    this.scrollbox.viewport.onMouseRelease = function (x, y, button, buttonState, modifiers) {
        let preview = this.parent.parent;

        if (preview.mousePressed) {

            if ((preview.getDragRect().area > 0)) {
               preview.supressRepaint = true;
               preview.SetZoomRect(preview.getDragRect());
            }

            preview.dragFrom = new Point();
            preview.dragTo = new Point();
        }
        else if (preview.scrolling && buttonState == MouseButton_Left) {
            preview.scrollbox.horizontalScrollPosition = preview.scrolling.orgScroll.x - (x - preview.scrolling.orgCursor.x);
            preview.scrollbox.verticalScrollPosition = preview.scrolling.orgScroll.y - (y - preview.scrolling.orgCursor.y);
        }
        preview.scrolling = null;
        preview.scrollbox.cursor = new Cursor(StdCursor_Cross);
        preview.mousePressed = false;
        preview.dragging = false;
        preview.zooming = false;
        this.update();
    };

    this.scrollbox.viewport.onResize = function (wNew, hNew, wOld, hOld) {
        let preview = this.parent.parent;
        if (preview.metadata && preview.scaledImage) {
            this.parent.maxHorizontalScrollPosition = Math.max(0, preview.scaledImage.width - wNew);
            this.parent.maxVerticalScrollPosition = Math.max(0, preview.scaledImage.height - hNew);
            preview.SetZoomOutLimit();
            preview.UpdateZoom(preview.zoom);
        }
        preview.mousePressed = false;
        preview.scrolling = null;
        preview.scrollbox.cursor = new Cursor(StdCursor_Cross);
        this.update();
    };

    this.scrollbox.viewport.onPaint = function (x0, y0, x1, y1) {
        let preview = this.parent.parent;
        let graphics = new Graphics(this);

        graphics.fillRect(x0, y0, x1, y1, new Brush(0xff303030));

        if (!this.NoImage) {
           if (preview.scaledImage != null) {
               let offsetX = (this.parent.maxHorizontalScrollPosition > 0) ?
                  -this.parent.horizontalScrollPosition : (this.width - preview.scaledImage.width) / 2;
               let offsetY = (this.parent.maxVerticalScrollPosition > 0) ?
                  -this.parent.verticalScrollPosition : (this.height - preview.scaledImage.height) / 2;
               graphics.translateTransformation(offsetX, offsetY);
           }

           if (preview.bitmap)
               graphics.drawBitmap(0, 0, preview.scaledImage);
           else if (preview.scaledImage)
               graphics.fillRect(0, 0, preview.scaledImage.width, preview.scaledImage.height, new Brush(0xff000000));

           graphics.pen = new Pen(0xffffffff, 0);
           if (preview.scaledImage)
               graphics.drawRect(-1, -1, preview.scaledImage.width + 1, preview.scaledImage.height + 1);

           if (preview.onCustomPaint) {
               graphics.antialiasing = true;
               graphics.scaleTransformation(preview.scale, preview.scale);
               preview.onCustomPaint.call(preview.onCustomPaintScope, graphics, x0, y0, x1, y1);
           }
        }

        if (preview.mousePressed) {
            graphics.resetTransformation();
            graphics.fillRect(preview.getDragRect(), new Brush(0x20ffffff));
        }

        graphics.end();
    };

    this.backgroundColor = 0xFFD8D7D3;
    this.zoomLabel_Label = new Label(this);
    this.zoomLabel_Label.text = "Zoom:";
    this.zoomVal_Label = new Label(this);
    this.zoomVal_Label.text = "1:1";

    this.hintsLabel = new Label(this);
    this.hintsLabel.text = "Shift+Left Mousebutton for zoom rectangle, Right Mousebotton zoom to fit";
    this.hintsLabel.styleSheet = this.scaledStyleSheet("QLabel { font-family: Hack; color: #7F7F7F}");

    this.hintsFrame = new Frame(this);
    this.hintsFrame.styleSheet = this.scaledStyleSheet("QLabel { font-family: Hack; }");

    this.hintsFrame.sizer = new HorizontalSizer;
    this.hintsFrame.sizer.margin = 4;
    this.hintsFrame.sizer.spacing = 4;
    this.hintsFrame.sizer.add(this.zoomLabel_Label);
    this.hintsFrame.sizer.add(this.zoomVal_Label);
    this.hintsFrame.sizer.addSpacing(8);
    this.hintsFrame.sizer.add(this.hintsLabel);

    this.hintsFrame.sizer.addStretch();

    this.sizer = new VerticalSizer;
    this.sizer.add(this.buttons_Sizer);
    this.sizer.add(this.scroll_Sizer);
    this.sizer.add(this.hintsFrame);
}

PreviewControl.prototype = new Frame;
