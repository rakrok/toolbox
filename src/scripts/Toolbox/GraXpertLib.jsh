#ifndef __GRAXPERTLIB_jsh
#define __GRAXPERTLIB_jsh

#include <pjsr/StdIcon.jsh>
#include <pjsr/StdButton.jsh>
#include <pjsr/StdDialogCode.jsh>
#include <pjsr/Sizer.jsh>
#include <pjsr/FrameStyle.jsh>
#include <pjsr/TextAlign.jsh>
#include <pjsr/ColorSpace.jsh>
#include <pjsr/Interpolation.jsh>
#include <pjsr/UndoFlag.jsh>
#include <pjsr/NumericControl.jsh>
#include <pjsr/CheckState.jsh>
#include <pjsr/SampleType.jsh>

#include <pjsr/DataType.jsh>
#include <../src/scripts/AdP/WCSmetadata.jsh>

#define GRAXPERT_VERSION "1.4"
#define GRAXPERT_TITLE "GraXpert"

#define GRAXPERT_DEFAULT_AUTOSTRETCH_SCLIP -2.80
#define GRAXPERT_DEFAULT_AUTOSTRETCH_TBGND 0.25

#ifeq __PI_PLATFORM__ MACOSX
#define GRAXPERT_SCRPT_DIR File.homeDirectory + "/Library/Application Support/GraXpertScript"
#endif
#ifeq __PI_PLATFORM__ MSWINDOWS
#define GRAXPERT_SCRPT_DIR File.homeDirectory + "/AppData/Local/GraXpertScript"
#endif
#ifeq __PI_PLATFORM__ LINUX
#define GRAXPERT_SCRPT_DIR File.homeDirectory + "/.local/share/GraXpertScript"
#endif

#define GRAXPERT_SCRIPT_CONFIG GRAXPERT_SCRPT_DIR + "/GraXpertScript.json"

function GraXpertLib()
{
   this.__base__ = Object;
   this.__base__();

   this.graxpertParameters = {
      targetView: undefined,
      correction: 0,
      smoothing: 0.0,
      replaceTarget: true,
      showBackground: false
   };

   // stores the current parameters values into the script instance
   this.saveInstanceParameters = function()
   {
      Parameters.set("correction", this.graxpertParameters.correction);
      Parameters.set("smoothing", this.graxpertParameters.smoothing);
      Parameters.set("replaceTarget", this.graxpertParameters.replaceTarget);
      Parameters.set("showBackground", this.graxpertParameters.showBackground);
   }

   // loads the script instance parameters
   this.loadInstanceParameters = function()
   {
      if (Parameters.has("correction"))
         this.graxpertParameters.correction = Parameters.getInteger("correction");
      if (Parameters.has("smoothing"))
         this.graxpertParameters.smoothing = Parameters.getReal("smoothing");
      if (Parameters.has("replaceTarget"))
         this.graxpertParameters.replaceTarget = Parameters.getBoolean("replaceTarget");
      if (Parameters.has("showBackground"))
         this.graxpertParameters.showBackground = Parameters.getBoolean("showBackground");
   }

   this.setGraXpertDefaults = function()
   {
      this.graxpertParameters.correction = 0;
      this.graxpertParameters.smoothing = 0.0;
      this.graxpertParameters.replaceTarget = true;
      this.graxpertParameters.showBackground = false;
      return graxpertParameters;
   }

   this.readGraXpertParameters = function()
   {
      var params = undefined

      if (File.exists(GRAXPERT_SCRIPT_CONFIG)) {
         try {
            params = JSON.parse(File.readTextFile(GRAXPERT_SCRIPT_CONFIG));

            if ((params.replaceTarget==true || params.replaceTarget==false)
                && (params.showBackground==true || params.showBackground==false)
                && (params.smoothing >= 0.0 && params.smoothing <=1.0)) {
               Console.writeln("Reading previously used settings from " + GRAXPERT_SCRIPT_CONFIG);
            } else {
               params = setGraXpertDefaults();
            }

         } catch (error) {
            Console.warningln("Loading GraXpert script settings failed...");
            Console.warningln(error);
            params = undefined;
            try {
               File.remove(GRAXPERT_SCRIPT_CONFIG);
            }
            catch (error2) {
               Console.criticalln('Could not delete potential corrupt file (please remove manualy): ' + GRAXPERT_SCRIPT_CONFIG);
            }
         }
      }

      // set default params
      if ( params == undefined ) {
         params = setGraXpertDefaults();
      }

      this.graxpertParameters.replaceTarget = params.replaceTarget;
      this.graxpertParameters.showBackground = params.showBackground;
      this.graxpertParameters.smoothing = params.smoothing;
      if (params.correction != undefined)
         this.graxpertParameters.correction =  params.correction;
   }

   function storeReplacer(key, value) {
       if (key=="targetView") return undefined;
       else return value;
   }

   this.storeGraXpertParameters = function()
   {
      if (!File.directoryExists(GRAXPERT_SCRPT_DIR)) {
         File.createDirectory(GRAXPERT_SCRPT_DIR, true);
      }
      File.writeTextFile(GRAXPERT_SCRIPT_CONFIG, JSON.stringify(
         this.graxpertParameters, storeReplacer));
   }


   this.selectGraXpertPath = function() {

      let pathFile = File.homeDirectory+"/GraXpertPath.txt";

#ifeq __PI_PLATFORM__ MACOSX
      var fd = new OpenFileDialog();
      fd.caption = "Select the GraXpert app from your applications folder...";
      fd.filters = [
            ["Apps", ".app"]
         ];
      if (fd.execute()) {
         let path = fd.fileName;
         Console.writeln("macOS: " + path);
         File.writeTextFile(pathFile, path);
         return path;
      }
      return undefined;
#endif
#ifeq __PI_PLATFORM__ MSWINDOWS
      let mb = new MessageBox(
                  "<p>Should I try to set the path to GraXpert automatically?</p>",
                  GRAXPERT_TITLE,
                  StdIcon_Question,
                  StdButton_Yes,
                  StdButton_No
         );
      let result = mb.execute();
      if (result == StdButton_Yes) {
         let path = "graxpert";
         Console.writeln("Windows: " + path);
         File.writeTextFile(pathFile, path);
         return path;
      }
      else {
         var fd = new OpenFileDialog();
         fd.caption = "Select the GraXpert app from your programs folder...";
         fd.filters = [
               ["Programs", ".exe"]
            ];
         if (fd.execute()) {
            let path = File.unixPathToWindows(fd.fileName);
            Console.writeln("Windows: " + path);
            File.writeTextFile(pathFile, path);
            return path;
         }
      }

      return undefined;
#else
      var fd = new OpenFileDialog();
      fd.caption = "Select the GraXpert app from your applications folder...";

      if (fd.execute()) {
         let path = fd.fileName;
         Console.writeln("Linux: " + path);
         File.writeTextFile(pathFile, path);
         return path;
      }
      return undefined;
#endif
   }

   this.hasGraXpertPath = function() {
      let pathFile = File.homeDirectory+"/GraXpertPath.txt";
      return File.exists(pathFile);
   }

   function getGraXpertPath() {

      // read text file containing file path
      let pathFile = File.homeDirectory+"/GraXpertPath.txt";
      try {
         if (File.exists(pathFile)) {
            return File.readTextFile(pathFile);
         }

         let mb = new MessageBox(
                  "<p>You must setup the path to GraxPert first using the wrench icon!</p>",
                  GRAXPERT_TITLE,
                  StdIcon_Error,
                  StdButton_Ok
         );
         mb.execute();
      }
      catch(error) {
         Console.criticalln('Could not read GraXpert path: ' + error);
         try {
            File.remove(pathFile);
         }
         catch(error2) {
         }
      }

      return undefined;
   }

   function executeGraXpert(cmdLine) {

      Console.writeln("running "+ cmdLine);

      let wrongVersion = false;
      let noError = true;

      let process = new ExternalProcess;

      process.onStarted = function() {
         Console.noteln( 'starting GraXpert...' );
      };

      process.onError = function( code ) {
         Console.criticalln(' ERROR: ' + code);
      };

      process.onFinished = function() {
         Console.noteln( 'GraXpert finished...' );
      }

      process.onStandardOutputDataAvailable = function()
      {
         var output = String(this.stdout);
         wrongVersion = output.contains('unrecognized argument');

         if (output.contains('AutoGraph is not available in this environment')) {
            return;
         }
         if (output.contains('TF_ENABLE_ONEDNN_OPTS=0')) {
            return;
         }

         if (output.contains('INFO ')) {

            if (output.contains('Using user-supplied')) {
               return;
            }

            // the script does intentionally not support changing the AI version for now, so this is not relevant for the user
            output = output.replace("You can overwrite this by providing the argument '-ai_version'", "");
            Console.writeln(output);
         }
         else if (output.contains('ERROR ')) {
            Console.criticalln(output);
            noError = false;
         }
         else if (output.contains('WARNING ')) {
            Console.warningln(output);
         }
         else {
            Console.writeln(output);
         }
      };

      process.onStandardErrorDataAvailable  = function() {
         Console.criticalln( 'GraXpert Error: ' + this.stderr.toString() );
      };

      try {
         process.start( cmdLine );
         for ( ; process.isStarting; )
            processEvents();
         for ( ; process.isRunning; )
            processEvents();

         if (wrongVersion) {
            Console.criticalln('ERROR: Your GraXpert version is not compatible with this script version! Please update your GraXpert version to use the new features!');

            let mb = new MessageBox(
               "<p>Your GraXpert version is not compatible with this script version! Please update your GraXpert version to use the new features!</p>",
               GRAXPERT_TITLE,
               StdIcon_Error,
               StdButton_Ok
            );
            mb.execute();

            return false;
         }

         return noError;
      }
      catch(error) {
         Console.criticalln(error);
         return false;
      }
   }

   function getCorrectionType(corr) {
      if (corr == 1) {
         return "Division";
      }
      return "Subtraction";
   }

   this.buildGraxpertCmdLine = function(graXpertPath, imagePath)
   {
#ifeq __PI_PLATFORM__ MSWINDOWS
      imagePath = File.unixPathToWindows(imagePath);
#endif

      var cmdLine = '"' + graXpertPath + '" -cli ' +
               '"' +
               imagePath +
               '" -correction ' + getCorrectionType(this.graxpertParameters.correction) +
               ' -smoothing ' + this.graxpertParameters.smoothing;

      if (this.graxpertParameters.showBackground) {
         cmdLine += ' -bg';
      }

      return cmdLine;
   }

   function assign(view, toView) {
      var P = new PixelMath;
      P.expression = view.id;
      P.useSingleExpression = true;
      P.clearImageCacheAndExit = false;
      P.cacheGeneratedImages = false;
      P.generateOutput = true;
      P.singleThreaded = false;
      P.optimization = true;
      P.use64BitWorkingImage = false;
      P.createNewImage = false;
      P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
      P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;

      P.executeOn(toView);
   }

   function cloneHidden(view, postfix, swapfile=true) {
      var P = new PixelMath;
      P.expression = "$T";
      P.useSingleExpression = true;
      P.clearImageCacheAndExit = false;
      P.cacheGeneratedImages = false;
      P.generateOutput = true;
      P.singleThreaded = false;
      P.optimization = true;
      P.use64BitWorkingImage = false;
      P.rescale = false;
      P.truncate = true;
      P.createNewImage = true;
      P.showNewImage = false;
      P.newImageId = view.id + postfix;
      P.newImageWidth = 0;
      P.newImageHeight = 0;
      P.newImageAlpha = false;
      P.newImageColorSpace = PixelMath.prototype.SameAsTarget;
      P.newImageSampleFormat = PixelMath.prototype.SameAsTarget;
      P.executeOn(view, swapfile);

      return View.viewById(P.newImageId);
   }

   function closeView(view) {
      if (view && view.window) {
         try {
            view.window.forceClose();
         }
         catch(error) {
            Console.writeln("could not close view..." + error);
         }
      }
   }

   function copyImage(imagePath, targetView) {
      if (targetView) {
         var processedPath = imagePath;

         if (imagePath.endsWith(".xisf")) {
            processedPath = imagePath.replace(".xisf", "_GraXpert.xisf");
            if (!File.exists(processedPath))
               processedPath = imagePath.replace(".xisf", "_GraXpert.fits");
         } else {
            processedPath = imagePath.replace(".fits", "_GraXpert.fits");
         }

#ifeq __PI_PLATFORM__ MSWINDOWS
         processedPath = File.unixPathToWindows(processedPath);
#endif

         if (File.exists(processedPath)) {

            var windows = ImageWindow.open(processedPath);

            if (windows != null && windows.length > 0) {
               let processedWindow = windows[0];
               assign(processedWindow.mainView, targetView);
               processedWindow.forceClose();

               try {
                  File.remove(processedPath);
               }
               catch(error) {
                  Console.warningln("Could not delete file " + processedPath);
                  Console.warningln(error);
               }
            }
         }
      }
   }

   function showBackgroundImage(imagePath) {
      var backgroundPath = imagePath;

      if (imagePath.endsWith(".xisf")) {
         backgroundPath = imagePath.replace(".xisf", "_GraXpert_background.xisf");
         if (!File.exists(backgroundPath))
            backgroundPath = imagePath.replace(".xisf", "_GraXpert_background.fits");
      } else {
         backgroundPath = imagePath.replace(".fits", "_GraXpert_background.fits");
      }

      if (File.exists(backgroundPath)) {
         var windows = ImageWindow.open(backgroundPath, 'Background_Model', '', true);
         if (windows != null && windows.length > 0) {
            let backgroundWindow = windows[0];

            GraXpertLib.STFAutoStretch(backgroundWindow.mainView, false);
            backgroundWindow.show();

            try {
               File.remove(backgroundPath);
            }
            catch(error) {
               Console.warningln("Could not delete file " + backgroundPath);
               Console.warningln(error);
            }
         }
      }
   }

   function getTempImage(imagePath) {

      if (imagePath.endsWith(".xisf")) {
         return imagePath.replace(".xisf", "_Temp.xisf");
      } else if (imagePath.endsWith(".fits")) {
         return imagePath.replace(".fits", "_Temp.xisf");
      } else if (imagePath.endsWith(".tiff")) {
         return imagePath.replace(".tiff", "_Temp.xisf");
      } else if (imagePath.endsWith(".tif")) {
         return imagePath.replace(".tif", "_Temp.xisf");
      }
      return imagePath + "_Temp.xisf";
   }

   function getFileSystemSeparator() {
      return corePlatform == "Windows" ? "\\" : "\/";
   }

   this.process = function()
   {
      if (this.graxpertParameters.targetView) {
         let clonedView = cloneHidden(this.graxpertParameters.targetView, "_GraXpert");
         let targetView = this.graxpertParameters.targetView;

         let metadata = new ImageMetadata("GraXpert");
         metadata.ExtractMetadata(targetView.window);

         if (!this.graxpertParameters.replaceTarget) {
            Console.writeln("Creating new image window...");
            targetView = clonedView;
         }

         let autoSTF = true;

         try {
            let window = clonedView.window;

            var imagePath = File.systemTempDirectory + getFileSystemSeparator() + targetView.id + "_GraXpertImage.xisf";
            Console.writeln('Temporary image file: ' + imagePath);

            let graxpertPath = getGraXpertPath();
            if (graxpertPath != undefined) {
               let imgPath = getTempImage(imagePath);

               try {
                  if (window.saveAs(imgPath, false, false, true, false)) {

                     let cmdLine = this.buildGraxpertCmdLine(graxpertPath, imgPath);
                     if (executeGraXpert(cmdLine)) {
                        copyImage(imgPath, targetView);

                        if (this.graxpertParameters.showBackground) {
                           showBackgroundImage(imgPath);
                        }
                     }
                     else {
                        autoSTF = false;
                        if (!this.graxpertParameters.replaceTarget) {
                           closeView(clonedView);
                        }
                     }
                  }
                  else
                      Console.warningln("Could not write file " + imgPath + " required to call GraXpert!");
               }
               finally {
                  try {
                     File.remove(imgPath);
                  }
                  catch(error) {
                     Console.warningln("Could not delete file " + processedPath);
                     Console.warningln(error);
                  }
               }
            }
            else
               Console.criticalln("GraXpert path is undefined!");
         }
         finally {
            if (!this.graxpertParameters.replaceTarget) {

               targetView.window.keywords = this.graxpertParameters.targetView.window.keywords;
               if (!metadata.projection || !metadata.ref_I_G) {
                  Console.writeln("The image " + targetView.id + " has no astrometric solution");
               }
               else {
                  metadata.SaveKeywords( targetView.window, false);
                  metadata.SaveProperties( targetView.window, GRAXPERT_TITLE + " " + GRAXPERT_VERSION);
               }

               targetView.window.show();
            } else {
               closeView(clonedView);
            }

            if (autoSTF)
               GraXpertLib.STFAutoStretch(targetView, false);
         }
      }
   }

   GraXpertLib.STFAutoStretch = function(view, rgbLinked)
   {
      let shadowsClipping = GRAXPERT_DEFAULT_AUTOSTRETCH_SCLIP;
      let targetBackground = GRAXPERT_DEFAULT_AUTOSTRETCH_TBGND;

      var stf = new ScreenTransferFunction;
      var n = view.image.isColor ? 3 : 1;
      var median = view.computeOrFetchProperty( "Median" );

      var mad = view.computeOrFetchProperty( "MAD" );
      mad.mul( 1.4826 ); // coherent with a normal distribution

      if ( rgbLinked )
      {
         /*
          * Try to find how many channels look as channels of an inverted image.
          * We know a channel has been inverted because the main histogram peak is
          * located over the right-hand half of the histogram. Seems simplistic
          * but this is consistent with astronomical images.
          */
         var invertedChannels = 0;
         for ( var c = 0; c < n; ++c )
            if ( median.at( c ) > 0.5 )
               ++invertedChannels;

         if ( invertedChannels < n )
         {
            /*
             * Noninverted image
             */
            var c0 = 0, m = 0;
            for ( var c = 0; c < n; ++c )
            {
               if ( 1 + mad.at( c ) != 1 )
                  c0 += median.at( c ) + shadowsClipping * mad.at( c );
               m  += median.at( c );
            }
            c0 = Math.range( c0/n, 0.0, 1.0 );
            m = Math.mtf( targetBackground, m/n - c0 );

            stf.STF = [ // c0, c1, m, r0, r1
                        [c0, 1, m, 0, 1],
                        [c0, 1, m, 0, 1],
                        [c0, 1, m, 0, 1],
                        [0, 1, 0.5, 0, 1] ];
         }
         else
         {
            /*
             * Inverted image
             */
            var c1 = 0, m = 0;
            for ( var c = 0; c < n; ++c )
            {
               m  += median.at( c );
               if ( 1 + mad.at( c ) != 1 )
                  c1 += median.at( c ) - shadowsClipping * mad.at( c );
               else
                  c1 += 1;
            }
            c1 = Math.range( c1/n, 0.0, 1.0 );
            m = Math.mtf( c1 - m/n, targetBackground );

            stf.STF = [ // c0, c1, m, r0, r1
                        [0, c1, m, 0, 1],
                        [0, c1, m, 0, 1],
                        [0, c1, m, 0, 1],
                        [0, 1, 0.5, 0, 1] ];
         }
      }
      else
      {
         /*
          * Unlinked RGB channnels: Compute automatic stretch functions for
          * individual RGB channels separately.
          */
         var A = [ // c0, c1, m, r0, r1
                  [0, 1, 0.5, 0, 1],
                  [0, 1, 0.5, 0, 1],
                  [0, 1, 0.5, 0, 1],
                  [0, 1, 0.5, 0, 1] ];

         for ( var c = 0; c < n; ++c )
         {
            if ( median.at( c ) < 0.5 )
            {
               /*
                * Noninverted channel
                */
               var c0 = (1 + mad.at( c ) != 1) ? Math.range( median.at( c ) + shadowsClipping * mad.at( c ), 0.0, 1.0 ) : 0.0;
               var m  = Math.mtf( targetBackground, median.at( c ) - c0 );
               A[c] = [c0, 1, m, 0, 1];
            }
            else
            {
               /*
                * Inverted channel
                */
               var c1 = (1 + mad.at( c ) != 1) ? Math.range( median.at( c ) - shadowsClipping * mad.at( c ), 0.0, 1.0 ) : 1.0;
               var m  = Math.mtf( c1 - median.at( c ), targetBackground );
               A[c] = [0, c1, m, 0, 1];
            }
         }

         stf.STF = A;
      }

      stf.executeOn( view );
   }
};

#endif // __GRAXPERTLIB_JSH
