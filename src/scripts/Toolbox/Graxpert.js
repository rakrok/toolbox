#include "GraXpertLib.jsh"
#include "GraXpertDialog.jsh"

#feature-id GraXpert : Toolbox > GraXpert

#feature-info  A script to run GraXpert from within PixInsight.<br/>\
   <br/>\
   A script to run GraXpert from within PixInsight. \
   <br/> \
   Copyright &copy; 2024 The GraXpert Team

function main()
{
   let gxp = new GraXpertLib;

   if (Parameters.isViewTarget) {
      gxp.loadInstanceParameters();
      gxp.graxpertParameters.targetView = Parameters.targetView;
      gxp.process();
      return;
   } else if (Parameters.isGlobalTarget) {
      gxp.loadInstanceParameters();
   }

   gxp.readGraXpertParameters();

   var window = ImageWindow.activeWindow;
   if (!window.isNull) {
      gxp.graxpertParameters.targetView = window.mainView;
   } else {
      gxp.graxpertParameters.targetView = undefined;
   }

   let dialog = new GraXpertDialog(gxp, gxp.graxpertParameters);
   dialog.execute();
}

main();
