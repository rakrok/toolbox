# PixInsight Utilities

This is a collection of utlities scripts for image processing in **PixInsight**. 

See also [www.ideviceapps.de/pixinsight-toolbox.html](https://www.ideviceapps.de/pixinsight-toolbox.html) for a more detailed description.


# Installation
   
The Utilities Scripts can be found under: 

https://www.ideviceapps.de/PixInsight/Utilities/

To install these scripts open PixInsight, then navigate to "Resources" => "Updates" => "Manage Repositories". 


Press the button "Add" and insert the URL of the script package: 

https://www.ideviceapps.de/PixInsight/Utilities/

Now close the form with "OK" and navigate to "Resources" => "Updates" => "Check for Updates" and start updating your repositories. After restarting PixInsight the script will be available.  